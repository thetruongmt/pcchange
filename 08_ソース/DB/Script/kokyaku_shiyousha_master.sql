CREATE TABLE `kokyaku_shiyousha_master` (
  `kokyaku_shiyousha_no` smallint(5) unsigned zerofill NOT NULL COMMENT '顧客使用者番号',
  `kokyaku_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客番号',
  `kokyaku_bumon_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客部門番号',
  `shiyousha_name` varchar(128) DEFAULT NULL COMMENT '使用者名前',
  `naisen_no` varchar(128) DEFAULT NULL COMMENT '内線番号',
  `mail_address` varchar(128) DEFAULT NULL COMMENT 'メールアドレス',
  PRIMARY KEY (`kokyaku_shiyousha_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='顧客使用者マスタ'
