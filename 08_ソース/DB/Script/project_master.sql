CREATE TABLE `project_master` (
  `sagyou_project_no` smallint(5) unsigned zerofill NOT NULL COMMENT '作業プロジェクト番号',
  `sagyou_project_name` varchar(128) DEFAULT NULL COMMENT '作業プロジェクト名',
  `kokyaku_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客番号',
  `sagyou_kikan_from` date DEFAULT NULL COMMENT '作業期間FROM',
  `sagyou_kikan_to` date DEFAULT NULL COMMENT '作業期間TO',
  PRIMARY KEY (`sagyou_project_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='プロジェクトマスタ'
