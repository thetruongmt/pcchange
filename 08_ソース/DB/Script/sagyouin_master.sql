CREATE TABLE `sagyouin_master` (
  `sagyouin_no` smallint(5) unsigned zerofill NOT NULL COMMENT '作業員番号',
  `sagyouin_name` varchar(128) DEFAULT NULL COMMENT '作業員名',
  `password` varchar(128) DEFAULT NULL COMMENT 'パスワード',
  PRIMARY KEY (`sagyouin_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作業員マスタ'
