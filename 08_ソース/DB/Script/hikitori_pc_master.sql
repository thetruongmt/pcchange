CREATE TABLE `hikitori_pc_master` (
  `hikitori_pc_no` smallint(5) unsigned zerofill NOT NULL COMMENT '引取PC番号',
  `sagyou_project_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '作業プロジェクト番号',
  `hikitori_kanryou_date` date DEFAULT NULL COMMENT '引取完了日',
  `lease_keiyaku_no` varchar(128) DEFAULT NULL COMMENT 'リース契約番号',
  `pc_katashiki` varchar(128) DEFAULT NULL COMMENT 'PC型式',
  `pc_meishou` varchar(128) DEFAULT NULL COMMENT 'PC名称',
  `serial_no` varchar(128) DEFAULT NULL COMMENT 'シリアル番号',
  `kokyaku_shiyousha_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客使用者番号',
  `pc_name` varchar(128) DEFAULT NULL COMMENT 'コンピュータ名',
  `sagyou_hikiate_pc_kobetsu_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '作業(引当)PC個別番号',
  `settei_koumoku1` text COMMENT '設定項目１',
  `settei_koumoku2` text COMMENT '設定項目２',
  `settei_koumoku3` text COMMENT '設定項目３',
  `settei_koumoku4` text COMMENT '設定項目４',
  PRIMARY KEY (`hikitori_pc_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='引取PCマスタ'
