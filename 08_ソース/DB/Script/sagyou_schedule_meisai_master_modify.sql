ALTER TABLE `pcchange_db`.`sagyou_schedule_meisai_master` 
ADD COLUMN `sagyou_month` DATE NOT NULL AFTER `sagyou_project_no`,
ADD COLUMN upd_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
CHANGE COLUMN `sagyou_project_no` `sagyou_project_no` SMALLINT(5) UNSIGNED ZEROFILL NOT NULL COMMENT '作業プロジェクト番号' ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`sagyou_schedule_meisai_no`, `sagyou_project_no`, `sagyou_month`);
;