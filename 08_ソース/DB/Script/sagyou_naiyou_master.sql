CREATE TABLE `sagyou_naiyou_master` (
  `sagyou_naiyou_no` smallint(5) unsigned zerofill NOT NULL COMMENT '作業内容(パターン)番号',
  `sagyou_name` varchar(128) DEFAULT NULL COMMENT '作業名',
  `sagyou_project_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '作業プロジェクト番号',
  `kokyaku_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客番号',
  `kokyaku_bumon_no` smallint(5) unsigned zerofill DEFAULT NULL COMMENT '顧客部門番号',
  `sagyou_settei_koumoku1` text COMMENT '作業設定項目１',
  `sagyou_settei_koumoku2` text COMMENT '作業設定項目２',
  `sagyou_settei_koumoku3` text COMMENT '作業設定項目３',
  `sagyou_settei_koumoku4` text COMMENT '作業設定項目４',
  `sagyou_settei_koumoku5` text COMMENT '作業設定項目５',
  `sagyou_settei_koumoku6` text COMMENT '作業設定項目６',
  `sagyou_settei_koumoku7` text COMMENT '作業設定項目７',
  `sagyou_settei_koumoku8` text COMMENT '作業設定項目８',
  `sagyou_settei_koumoku9` text COMMENT '作業設定項目９',
  `sagyou_settei_koumoku10` text COMMENT '作業設定項目１０',
  PRIMARY KEY (`sagyou_naiyou_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作業内容(パターン)マスタ'
