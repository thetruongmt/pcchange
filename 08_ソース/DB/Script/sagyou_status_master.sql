CREATE TABLE `sagyou_status_master` (
  `sagyou_status_no` smallint(5) unsigned zerofill NOT NULL COMMENT '作業ステータス番号',
  `status` varchar(128) NOT NULL COMMENT 'ステータス',
  PRIMARY KEY (`sagyou_status_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作業ステータスマスタ'
