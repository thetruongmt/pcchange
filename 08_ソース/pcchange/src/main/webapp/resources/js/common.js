
$(document).ready(function() {
	 $(".js-month-picker").datetimepicker({
	    	locale: 'ja',
	    	collapse: true,
	    	format: "YYYY/MM",
	    	icons: {
			  time: 'fa fa-clock-o',
			  date: 'fa fa-calendar',
			  up: 'fa fa-chevron-up',
			  down: 'fa fa-chevron-down',
			  previous: 'fa fa-chevron-left',
			  next: 'fa fa-chevron-right',
			  today: 'fa fa-crosshairs',
			  clear: 'fa fa-trash-o',
			  close: 'fa fa-times'
			},
	    	viewMode: 'months',
	    	viewDate: false,
	    	disabledDates: false,

	    });
	 $(".js-date-picker").datetimepicker({
	    	locale: 'ja',
	    	collapse: true,
	    	format: "YYYY/MM/DD",
	    	icons: {
			  time: 'fa fa-clock-o',
			  date: 'fa fa-calendar',
			  up: 'fa fa-chevron-up',
			  down: 'fa fa-chevron-down',
			  previous: 'fa fa-chevron-left',
			  next: 'fa fa-chevron-right',
			  today: 'fa fa-crosshairs',
			  clear: 'fa fa-trash-o',
			  close: 'fa fa-times'
			},
	    	viewMode: 'days'

	    });

	 $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
});