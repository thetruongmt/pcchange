$(document).ready(function() {
	/************** EVENT ***************/
	$('.js-kokyaku-shiyousha-search').click(function(event) {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url:BASE_URL +  "/search/shiyousha?projectNo=" + $("select[id$='sagyouProjectNo']").val(),
            dataType: 'html',
            success: function(data) {
                // ページにモダルを追加する
                $('#modalContainer').append(data);
                // モダルを表示する
                $('#myModal').modal('toggle');
                $('#myModal').on('hidden.bs.modal', function(event) {
                    $('#modalContainer').empty();
                });

                // テーブルをクリックした
                $('#tblKokyaku tbody tr').click(function(event) {
                    $('#tblKokyaku tbody tr.bg-success').first().removeClass('bg-success');
                    $(event.target).closest('tr').addClass('bg-success')
                });

                // 選択ボタンを押した
                var $colDataNo = $(event.target).closest('.row').find("[id$='kokyakuShiyousha.kokyakuShiyoushaNo']")
                var $colDataName = $(event.target).closest('.row').find("[id$='kokyakuShiyousha.shiyoushaName']")
                var $modified = $(event.target).closest('tr').find("[id$='modified']");
                $('#btnSelect').click(function(event) {
                    $('#myModal').modal('toggle');
                    $colDataNo.val($('#tblKokyaku tbody tr.bg-success').children('td').eq(1).data("id"));
                    $colDataName.val($('#tblKokyaku tbody tr.bg-success').children('td').eq(4).text());
        			if($modified.length > 0){
        				$modified.val(1);
        			}
                });
            },
            error: function(e) {
                console.log("ERROR: ", e);
            },
            done: function(e) {
                console.log("DONE");
            }
        });

    });

	$('.js-hikitori-pc-search').click(function(event) {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url:BASE_URL +  "/search/hikitoripc?projectNo=" + $("select[id$='sagyouProjectNo']").val(),
            dataType: 'html',
            success: function(data) {
                // ページにモダルを追加する
                $('#modalContainer').append(data);
                // モダルを表示する
                $('#myModal').modal('toggle');
                $('#myModal').on('hidden.bs.modal', function(event) {
                    $('#modalContainer').empty();
                });

                // テーブルをクリックした
                $('#tblResult tbody tr').click(function(event) {
                    $('#tblResult tbody tr.bg-success').first().removeClass('bg-success');
                    $(event.target).closest('tr').addClass('bg-success')
                });

                // 選択ボタンを押した
                var $colDataNo = $(event.target).closest('.row').find("[id$='hikitoriPCNo']")
                var $modified = $(event.target).closest('tr').find("[id$='modified']");
                $('#btnSelect').click(function(event) {
                    $('#myModal').modal('toggle');
                    $colDataNo.val($('#tblResult tbody tr.bg-success').children('td').eq(1).data("id"));
        			if($modified.length > 0){
        				$modified.val(1);
        			}
                });
            },
            error: function(e) {
                console.log("ERROR: ", e);
            },
            done: function(e) {
                console.log("DONE");
            }
        });

    });

    /*ページング*/
    $("#shinpcPage").on("click", ".page-link", function(){
    	//disable check
    	var $link = $(this);
    	var $li = $link.closest("li");
    	if($li.hasClass("disabled")){
    		return ;
    	}
    	var page = $link.data("page");
    	if(!page){
    		return ;
    	}

    	var result = window.confirm("入力したデータを保存せずに移動してもよろしいですか？");
    	if(!result){
    		return ;
    	}

    	$("#currentPage").val(page);
    	$("#btnSearch").click();

    });

    $("#shinpcPage").on("click", "tr", function(){
		var $tr = $(this);
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});
	$("#shinpcPage").on("focus", "input, select, tr button", function(){
		var $tr = $(this).closest("tr");
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});

    /*行を新規作成*/
    $("#shinpcPage").on("change", "input, select", function(){
    	var $input = $(this);
    	var $tr = $input.closest("tr");
		var $tbody = $($tr).closest("tbody");

		if($tr.hasClass("js-new-row")){
			var rowCnt = $("tr", $tbody).length;
			if(rowCnt > 0){
				rowCnt--;
				var $newRow = $tr.clone(true);
				$tr.removeClass("js-new-row");
				$(".js-row-title", $tr).text("").removeClass("js-row-title");
				$("[id$='sagyouinNo']", $tr)
				.attr("id","detail"+rowCnt+".sagyouin.sagyouinNo")
				.attr("name","detail["+rowCnt+"].sagyouin.sagyouinNo");

				$("[id$='hikitoriPCNo']", $tr)
				.attr("id","detail"+rowCnt+".hikitoriPCNo")
				.attr("name","detail["+rowCnt+"].hikitoriPCNo");

				$("[id$='sagyouNaiyouNo']", $tr)
				.attr("id","detail"+rowCnt+".sagyouNaiyou.sagyouNaiyouNo")
				.attr("name","detail["+rowCnt+"].sagyouNaiyou.sagyouNaiyouNo");

				$("[id$='serialNo']", $tr)
				.attr("id","detail"+rowCnt+".serialNo")
				.attr("name","detail["+rowCnt+"].serialNo");

				$("[id$='shiyoushaName']", $tr)
				.attr("id","detail"+rowCnt+".kokyakuShiyousha.shiyoushaName")
				.attr("name","detail["+rowCnt+"].kokyakuShiyousha.shiyoushaName");

				$("[id$='kokyakuShiyoushaNo']", $tr)
				.attr("id","detail"+rowCnt+".kokyakuShiyousha.kokyakuShiyoushaNo")
				.attr("name","detail["+rowCnt+"].kokyakuShiyousha.kokyakuShiyoushaNo");

				$("[id$='pcName']", $tr)
				.attr("id","detail"+rowCnt+".pcName")
				.attr("name","detail["+rowCnt+"].pcName");

				$("[id$='network1']", $tr)
				.attr("id","detail"+rowCnt+".network1")
				.attr("name","detail["+rowCnt+"].network1");

				$("[id$='network2']", $tr)
				.attr("id","detail"+rowCnt+".network2")
				.attr("name","detail["+rowCnt+"].network2");

				$("[id$='setteiKoumoku3']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku3")
				.attr("name","detail["+rowCnt+"].setteiKoumoku3");

				$("[id$='setteiKoumoku4']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku4")
				.attr("name","detail["+rowCnt+"].setteiKoumoku4");

				$("[id$='setteiKoumoku5']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku5")
				.attr("name","detail["+rowCnt+"].setteiKoumoku5");

				$("[id$='setteiKoumoku6']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku6")
				.attr("name","detail["+rowCnt+"].setteiKoumoku6");

				$("[id$='setteiKoumoku7']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku7")
				.attr("name","detail["+rowCnt+"].setteiKoumoku7");

				$("[id$='setteiKoumoku8']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku8")
				.attr("name","detail["+rowCnt+"].setteiKoumoku8");

				$("[id$='setteiKoumoku9']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku9")
				.attr("name","detail["+rowCnt+"].setteiKoumoku9");

				$("[id$='setteiKoumoku10']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku10")
				.attr("name","detail["+rowCnt+"].setteiKoumoku10");

				$("[id$='sagyouStatusNo']", $tr)
				.attr("id","detail"+rowCnt+".sagyouStatus.sagyouStatusNo")
				.attr("name","detail["+rowCnt+"].sagyouStatus.sagyouStatusNo");

				$("[id$='modified']", $tr)
				.attr("id","detail"+rowCnt+".modified")
				.attr("name","detail["+rowCnt+"].modified");

				$newRow.removeClass("table-active");
				$("input,select", $newRow).val("");
				if(dt){
					dt.row.add($newRow).draw();
				}

			}
		}else{
			var $modified = $("[id$=modified]",$tr);
			if($modified.length > 0){
				$modified.val(1);
			}
		}
    });

    /*行を削除する*/
    $("#shinpcPage").on("click", "#btnDelete", function(){
    	var $selectTr = $("tr.table-active");
		if($selectTr.length <= 0){
			alert("削除する行を選択してください。");
			return false;
		}
		if($selectTr.hasClass("js-new-row")){
			alert("新行を削除できません。");
			return false;
		}

		var $shiyouNo = $("[id$='sagyouHikiatePcKobetsuNo']", $selectTr);
		if($shiyouNo.length <= 0){
			if(confirm("選択した行を削除してもよろしいですか")){
				alert("選択した行を削除しました。");
				fncRefreshIdForGrid($selectTr);
				$selectTr.remove();
			}
			return;
		}else{
			if(confirm("選択した行を削除してもよろしいですか")){
				var data = {
						sagyouHikiatePcKobetsuNo: $("[id$=sagyouHikiatePcKobetsuNo]", $selectTr).val(),
						updDate: $("[id$=updDate]", $selectTr).val(),
				};
				$.ajax({
					url: BASE_URL + "/shinpc/delete",
					dataType: "json",
					cache: false,
					data: data,
					method: "POST",
					success: function(ret){
						if(ret.status == 0){
							alert(ret.message);
							return;
						}else{
							alert(ret.message);
							fncRefreshIdForGrid($selectTr);
							$selectTr.remove();
						}
					},
					error: function(){
						alert("削除が失敗しました。");
					}
				});
			}
		}
    });

    /************** INIT ***************/
    var dt = null;
    var fncInit = function(){

	dt = $('.table-input').DataTable( {
	    scrollX:        true,
	    scrollCollapse: true,
	    searching:		false,
	    ordering:		false,
	    info:			false,
	    paging:         false,
	    fixedColumns:   {
	        leftColumns: 1
	    }
	} );
	var $errors = $(".is-invalid");
		if($errors.length > 0){
			$errors.first().focus().select();
    	}
	}

    var fncRefreshIdForGrid = function($trFrom){
		var $allTrs = $("tr", $trFrom.closest("tbody"));
		var idx = $allTrs.index($trFrom);
		if(idx >= 0){
			idx = idx+1;
			for(idx ; idx < $allTrs.length ; idx++){
				fncRefreshIdForRow($($allTrs[idx]), idx-1);
			}
		}

	}

	var fncRefreshIdForRow = function($tr, idx){
		var $input = $("input, select", $tr);
		$input.each(function(){
			var $this = $(this);
			var id = $this.attr("id");
			var name=$this.attr("name");
			if(name && name.match(/(\[{1}[0-9]+\]{1})/)){
				var newName = name.replace(/(\[{1}[0-9]+\]{1})/g, "["+idx+"]");
				$this.attr("name", newName);
			}
			if(name && id.match(/([0-9]+\.{1})/)){
				var newId = id.replace(/([0-9]+\.{1})/g, ""+idx+".");
				$this.attr("id", newId);
			}
		});
	}

   //画面の初期表示
   fncInit();

});