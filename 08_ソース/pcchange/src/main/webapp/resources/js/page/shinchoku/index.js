$(document).ready(function() {
	/************** EVENT ***************/
	$("#btnPrev").on("click", function(e){
		if(!confirm("入力したデータを保存せずに前ページに移動してもよろしいですか？")){
			e.preventDefault();
            e.stopPropagation();
            return false;
		}

	});
	$("#btnNext").on("click", function(e){
		if(!confirm("入力したデータを保存せずに次ページに移動してもよろしいですか？")){
			e.preventDefault();
            e.stopPropagation();
            return false;
		}
	});
	/************** INIT ***************/
	var fncInit = function(){

	}
	//画面の初期表示
});