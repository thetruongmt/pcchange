$(document).ready(function() {
	/************** EVENT ***************/
    $('.badge.badge-light').click(function(event) {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url:BASE_URL +  "/search/shiyousha?projectNo=" + $("select[id$='sagyouProjectNo']").val(),
            dataType: 'html',
            success: function(data) {
                // ページにモダルを追加する
                $('#modalContainer').append(data);
                // モダルを表示する
                $('#myModal').modal('toggle');
                $('#myModal').on('hidden.bs.modal', function(event) {
                    $('#modalContainer').empty();
                });

                // テーブルをクリックした
                $('#tblKokyaku tbody tr').click(function(event) {
                    $('#tblKokyaku tbody tr.bg-success').first().removeClass('bg-success');
                    $(event.target).closest('tr').addClass('bg-success')
                });

                // 選択ボタンを押した
                var colData = $(event.target).closest('.row').find('input')
                $('#btnSelect').click(function(event) {
                    $('#myModal').modal('toggle');
                    colData.val($('#tblKokyaku tbody tr.bg-success').children('td').eq(1).data("id"));
                });
            },
            error: function(e) {
                console.log("ERROR: ", e);
            },
            done: function(e) {
                console.log("DONE");
            }
        });

    });


   /************** INIT ***************/

   var fncInit = function(){
	   var table = $('.table-input').DataTable( {
	        scrollX:        true,
	        scrollCollapse: true,
	        searching:		false,
	        ordering:		false,
	        info:			false,
	        paging:         false,
	        fixedColumns:   {
	            leftColumns: 1
	        }
	    } );

	   var $errors = $(".is-invalid");
	   if($errors.length > 0){
		   $errors.first().focus().select();
	   }
   }

   //画面の初期表示
   fncInit();

});