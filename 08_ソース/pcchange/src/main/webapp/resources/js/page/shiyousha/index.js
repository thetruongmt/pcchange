$(document).ready(function() {
	/************** EVENT ***************/
	$("#kokyakuNo").on("change", function(){
		var $combobox = $(this);
		var selectedVal = $combobox.val();
		var url = BASE_URL + "/shiyousha/kokyaku?id=" + selectedVal;
		$.ajax({
			url: url,
			method:'GET',
			dataType: 'json',
			cache: false,
			success: function(data){
				var options = $.map(data, function (item, idx) {
				    $option = $('<option>', { value: item.kokyakuBumonNo, text: item.jigyoubuName })
				    $option.data("buka", item.bukaName);
				    return $option;
				});
				$("#kokyakuBumonNo").empty().append(options);
				var bukaName = "";
				if(data.length > 0){
					bukaName = data[0].bukaName;
				}
				$("#bukaName").val(bukaName);
			},
			error: function(){
				alert('エラーが発生しました。管理者に連絡してください。');
			}
		});
	});

	$("#kokyakuBumonNo").on("change", function(){
		var $combobox = $(this);
		var selected = $combobox.find(":selected");
		$("#bukaName").val(selected.data("buka"));
	});

	$("#shiyoushaPage").on("click", "tr", function(){
		var $tr = $(this);
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});
	$("#shiyoushaPage").on("focus", "input", function(){
		var $tr = $(this).closest("tr");
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});
	$("#shiyoushaPage").on("change", "input", function(){
		var $tr = $(this).closest("tr");
		var $tbody = $($tr).closest("tbody");

		if($tr.hasClass("js-new-row")){
			var rowCnt = $("tr", $tbody).length;
			if(rowCnt > 0){
				rowCnt--;
				var $newRow = $tr.clone(true).insertAfter($tr);
				$tr.removeClass("js-new-row");
				$("[id$='shiyoushaName']", $tr)
				.attr("id","shiyoushas"+rowCnt+".shiyoushaName")
				.attr("name","shiyoushas["+rowCnt+"].shiyoushaName");
				$("[id$='naisenNo']", $tr)
				.attr("id","shiyoushas"+rowCnt+".naisenNo")
				.attr("name","shiyoushas["+rowCnt+"].naisenNo");
				$("[id$='mailAddress']", $tr)
				.attr("id","shiyoushas"+rowCnt+".mailAddress")
				.attr("name","shiyoushas["+rowCnt+"].mailAddress");

				$newRow.removeClass("table-active");
				$("input", $newRow).val("");
			}
		}else{
			var $modified = $("[id$=modified]",$tr);
			if($modified.length > 0){
				$modified.val(1);
			}
		}
	});
	$("#shiyoushaPage").on("click", "#btnDelete", function(){
		var $selectTr = $("tr.table-active");
		if($selectTr.length <= 0){
			alert("削除する行を選択してください。");
			return false;
		}
		if($selectTr.hasClass("js-new-row")){
			alert("新行を削除できません。");
			return false;
		}
		var $shiyouNo = $("[id$='kokyakuShiyoushaNo']", $selectTr);
		if($shiyouNo.length <= 0){
			if(confirm("選択した行を削除してもよろしいですか")){
				alert("選択した行を削除しました。");
				fncRefreshIdForGrid($selectTr);
				$selectTr.remove();
			}
			return;
		}else{
			if(confirm("選択した行を削除してもよろしいですか")){
				var data = {
						kokyakuShiyoushaNo: $("[id$=kokyakuShiyoushaNo]", $selectTr).val(),
						updDate: $("[id$=updDate]", $selectTr).val(),
				};
				$.ajax({
					url: BASE_URL + "/shiyousha/delete",
					dataType: "json",
					cache: false,
					data: data,
					method: "POST",
					success: function(ret){
						if(ret.status == 0){
							alert(ret.message);
							return;
						}else{
							alert(ret.message);
							fncRefreshIdForGrid($selectTr);
							$selectTr.remove();
						}
					},
					error: function(){
						alert("削除が失敗しました。");
					}
				});
			}
		}

	})
	/************** INIT ***************/
	var fncInit = function(){

	}

	var fncRefreshIdForGrid = function($trFrom){
		var $allTrs = $("tr", $trFrom.closest("tbody"));
		var idx = $allTrs.index($trFrom);
		if(idx >= 0){
			idx = idx+1;
			for(idx ; idx < $allTrs.length ; idx++){
				fncRefreshIdForRow($($allTrs[idx]), idx-1);
			}
		}

	}

	var fncRefreshIdForRow = function($tr, idx){
		var $input = $("input", $tr);
		$input.each(function(){
			var $this = $(this);
			var id = $this.attr("id");
			var name=$this.attr("name");
			if(name && name.match(/(\[{1}[0-9]+\]{1})/)){
				var newName = name.replace(/(\[{1}[0-9]+\]{1})/g, "["+idx+"]");
				$this.attr("name", newName);
			}
			if(name && id.match(/([0-9]+\.{1})/)){
				var newId = id.replace(/([0-9]+\.{1})/g, ""+idx+".");
				$this.attr("id", newId);
			}
		});
	}
	//画面の初期表示
});