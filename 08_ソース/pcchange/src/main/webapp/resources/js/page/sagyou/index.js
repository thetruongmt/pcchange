$(document).ready(function() {

    $("#sagyouProjectNo").change(function(event) {
        var selectedProjectNo = $("#sagyouProjectNo").val();
        $('#kokyaku_list option').each(function(index, el) {
            var c = $('#kokyaku_list option').eq(index);
            if (c.data("projectno") == selectedProjectNo) {
                $('#kokyaku_list').val(c.val());
                $('#kokyakuKigyouName').val(c.text());
                return false;
            }
        });
    });

    $("#sagyouProjectNo").trigger("change");


});