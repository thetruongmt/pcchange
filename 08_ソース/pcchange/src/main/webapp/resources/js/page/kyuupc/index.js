$(document).ready(function() {
	$(".js-cell-date-picker").datetimepicker({
    	locale: 'ja',
    	collapse: true,
    	format: "YYYY/MM/DD",
    	widgetPositioning:{
            horizontal: 'auto',
            vertical: 'bottom'
        },
    	icons: {
		  time: 'fa fa-clock-o',
		  date: 'fa fa-calendar',
		  up: 'fa fa-chevron-up',
		  down: 'fa fa-chevron-down',
		  previous: 'fa fa-chevron-left',
		  next: 'fa fa-chevron-right',
		  today: 'fa fa-crosshairs',
		  clear: 'fa fa-trash-o',
		  close: 'fa fa-times'
		},
    	viewMode: 'days'

    });
	/************** EVENT ***************/
	$("#kyuupcPage").on("click", ".js-kokyaku-shiyousha-search", function(event) {
		var projectNo = $("select[id$='sagyouProjectNo']").val();
		var $hdnProject = $("[id$='sagyouProject.sagyouProjectNo']", $(this).closest("tr"));
		if($hdnProject.length > 0){
			projectNo = $hdnProject.val();
		}
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url:BASE_URL +  "/search/shiyousha?projectNo=" + projectNo,
            dataType: 'html',
            success: function(data) {
                // ページにモダルを追加する
                $('#modalContainer').append(data);
                // モダルを表示する
                $('#myModal').modal('toggle');
                $('#myModal').on('hidden.bs.modal', function(event) {
                    $('#modalContainer').empty();
                });

                // テーブルをクリックした
                $('#tblKokyaku tbody tr').click(function(event) {
                    $('#tblKokyaku tbody tr.bg-success').first().removeClass('bg-success');
                    $(event.target).closest('tr').addClass('bg-success')
                });

                // 選択ボタンを押した
                var $colDataNo = $(event.target).closest('.row').find("[id$='kokyakuShiyousha.kokyakuShiyoushaNo']")
                var $colDataName = $(event.target).closest('.row').find("[id$='kokyakuShiyousha.shiyoushaName']")
                var $modified = $(event.target).closest('tr').find("[id$='modified']");
                $('#btnSelect').click(function(event) {
                    $('#myModal').modal('toggle');
                    $colDataNo.val($('#tblKokyaku tbody tr.bg-success').children('td').eq(1).data("id"));
                    $colDataName.val($('#tblKokyaku tbody tr.bg-success').children('td').eq(4).text());
        			if($modified.length > 0){
        				$modified.val(1);
        			}
                });
            },
            error: function(e) {
                console.log("ERROR: ", e);
            },
            done: function(e) {
                console.log("DONE");
            }
        });

    });

    /*ページング*/
    $("#kyuupcPage").on("click", ".page-link", function(){
    	//disable check
    	var $link = $(this);
    	var $li = $link.closest("li");
    	if($li.hasClass("disabled")){
    		return ;
    	}
    	var page = $link.data("page");
    	if(!page){
    		return ;
    	}

    	var result = window.confirm("入力したデータを保存せずに移動してもよろしいですか？");
    	if(!result){
    		return ;
    	}

    	$("#currentPage").val(page);
    	$("#btnSearch").click();

    });

    $("#kyuupcPage").on("click", "tr", function(){
		var $tr = $(this);
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});
	$("#kyuupcPage").on("focus", "input, tr button", function(){
		var $tr = $(this).closest("tr");
		$("tr", $tr.closest("tbody")).removeClass("table-active");
		$tr.addClass("table-active");
	});

    /*行を新規作成*/
    $("#kyuupcPage").on("change dp.change", "input", function(){
    	var $input = $(this);
    	var $tr = $input.closest("tr");
		var $tbody = $($tr).closest("tbody");

		if($tr.hasClass("js-new-row")){
			var rowCnt = $("tr", $tbody).length;
			if(rowCnt > 0){
				rowCnt--;
				var $newRow = $tr.clone(false).off();
				$tr.removeClass("js-new-row");
				$(".js-row-title", $tr).text("").removeClass("js-row-title");
				$("[id$='hikitoriPcNo']", $tr)
				.attr("id","detail"+rowCnt+".hikitoriPcNo")
				.attr("name","detail["+rowCnt+"].hikitoriPcNo");

				$("[id$='hikitoriKanryouDate']", $tr)
				.attr("id","detail"+rowCnt+".hikitoriKanryouDate")
				.attr("name","detail["+rowCnt+"].hikitoriKanryouDate");

				$("[id$='leaseKeiyakuNo']", $tr)
				.attr("id","detail"+rowCnt+".leaseKeiyakuNo")
				.attr("name","detail["+rowCnt+"].leaseKeiyakuNo");

				$("[id$='pcKatashiki']", $tr)
				.attr("id","detail"+rowCnt+".pcKatashiki")
				.attr("name","detail["+rowCnt+"].pcKatashiki");

				$("[id$='pcMeishou']", $tr)
				.attr("id","detail"+rowCnt+".pcMeishou")
				.attr("name","detail["+rowCnt+"].pcMeishou");

				$("[id$='serialNo']", $tr)
				.attr("id","detail"+rowCnt+".serialNo")
				.attr("name","detail["+rowCnt+"].serialNo");

				$("[id$='shiyoushaName']", $tr)
				.attr("id","detail"+rowCnt+".kokyakuShiyousha.shiyoushaName")
				.attr("name","detail["+rowCnt+"].kokyakuShiyousha.shiyoushaName");

				$("[id$='kokyakuShiyoushaNo']", $tr)
				.attr("id","detail"+rowCnt+".kokyakuShiyousha.kokyakuShiyoushaNo")
				.attr("name","detail["+rowCnt+"].kokyakuShiyousha.kokyakuShiyoushaNo");

				$("[id$='pcName']", $tr)
				.attr("id","detail"+rowCnt+".pcName")
				.attr("name","detail["+rowCnt+"].pcName");

				$("[id$='setteiKoumoku1']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku1")
				.attr("name","detail["+rowCnt+"].setteiKoumoku1");

				$("[id$='setteiKoumoku2']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku2")
				.attr("name","detail["+rowCnt+"].setteiKoumoku2");

				$("[id$='setteiKoumoku3']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku3")
				.attr("name","detail["+rowCnt+"].setteiKoumoku3");

				$("[id$='setteiKoumoku4']", $tr)
				.attr("id","detail"+rowCnt+".setteiKoumoku4")
				.attr("name","detail["+rowCnt+"].setteiKoumoku4");

				$("[id$='modified']", $tr)
				.attr("id","detail"+rowCnt+".modified")
				.attr("name","detail["+rowCnt+"].modified");

				$newRow.removeClass("table-active");
				$("input", $newRow).val("");
				if(dt){
					dt.row.add($newRow).draw();
				}
				$(".js-cell-date-picker", $newRow).datetimepicker({
			    	locale: 'ja',
			    	collapse: true,
			    	format: "YYYY/MM/DD",
			    	widgetPositioning:{
			            horizontal: 'auto',
			            vertical: 'bottom'
			        },
			    	icons: {
					  time: 'fa fa-clock-o',
					  date: 'fa fa-calendar',
					  up: 'fa fa-chevron-up',
					  down: 'fa fa-chevron-down',
					  previous: 'fa fa-chevron-left',
					  next: 'fa fa-chevron-right',
					  today: 'fa fa-crosshairs',
					  clear: 'fa fa-trash-o',
					  close: 'fa fa-times'
					},
			    	viewMode: 'days'
			    });
			}
		}else{
			var $modified = $("[id$=modified]",$tr);
			if($modified.length > 0){
				$modified.val(1);
			}
		}
    });

    /*行を削除する*/
    $("#kyuupcPage").on("click", "#btnDelete", function(){
    	var $selectTr = $("tr.table-active");
		if($selectTr.length <= 0){
			alert("削除する行を選択してください。");
			return false;
		}
		if($selectTr.hasClass("js-new-row")){
			alert("新行を削除できません。");
			return false;
		}

		var $shiyouNo = $("[id$='hikitoriPcNo']", $selectTr);
		if($shiyouNo.length <= 0){
			if(confirm("選択した行を削除してもよろしいですか")){
				alert("選択した行を削除しました。");
				fncRefreshIdForGrid($selectTr);
				$selectTr.remove();
			}
			return;
		}else{
			if(confirm("選択した行を削除してもよろしいですか")){
				var data = {
					hikitoriPcNo: $("[id$=hikitoriPcNo]", $selectTr).val(),
					updDate: $("[id$=updDate]", $selectTr).val(),
				};
				$.ajax({
					url: BASE_URL + "/kyuupc/delete",
					dataType: "json",
					cache: false,
					data: data,
					method: "POST",
					success: function(ret){
						if(ret.status == 0){
							alert(ret.message);
							return;
						}else{
							alert(ret.message);
							fncRefreshIdForGrid($selectTr);
							$selectTr.remove();
						}
					},
					error: function(){
						alert("削除が失敗しました。");
					}
				});
			}
		}
    });

    /************** INIT ***************/
    var dt = null;
    var fncInit = function(){

		dt = $('.table-input').DataTable({
		    scrollX:        true,
		    scrollCollapse: true,
		    searching:		false,
		    ordering:		false,
		    info:			false,
		    paging:         false,
		    fixedColumns:   {
		        leftColumns: 1
		    }
		});

		var $errors = $(".is-invalid");
		if($errors.length > 0){
			$errors.first().focus().select();
    	}
	}

    var fncRefreshIdForGrid = function($trFrom){
		var $allTrs = $("tr", $trFrom.closest("tbody"));
		var idx = $allTrs.index($trFrom);
		if(idx >= 0){
			idx = idx+1;
			for(idx ; idx < $allTrs.length ; idx++){
				fncRefreshIdForRow($($allTrs[idx]), idx-1);
			}
		}

	}

	var fncRefreshIdForRow = function($tr, idx){
		var $input = $("input", $tr);
		$input.each(function(){
			var $this = $(this);
			var id = $this.attr("id");
			var name=$this.attr("name");
			if(name && name.match(/(\[{1}[0-9]+\]{1})/)){
				var newName = name.replace(/(\[{1}[0-9]+\]{1})/g, "["+idx+"]");
				$this.attr("name", newName);
			}
			if(name && id.match(/([0-9]+\.{1})/)){
				var newId = id.replace(/([0-9]+\.{1})/g, ""+idx+".");
				$this.attr("id", newId);
			}
		});
	}

   //画面の初期表示
   fncInit();

});