<%@tag import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@tag import="com.mediatrust.pcchange.entity.LoginSagyouin"%>
<%@tag import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken"%>
<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	UsernamePasswordAuthenticationToken authen = (UsernamePasswordAuthenticationToken)(request.getUserPrincipal());
	LoginSagyouin loginUser = null;
	if(authen != null){
		loginUser = (LoginSagyouin)authen.getPrincipal();
	}
	String scheme = request.getScheme();
	String serverName = request.getServerName();
	int serverPort = request.getServerPort();
	String appPath = request.getContextPath();
	String baseUrl=String.format("%s://%s:%d%s", scheme,serverName, serverPort,appPath );
	String pageId = (String)request.getAttribute("pageId");

%>
<html>
<head>
<link rel="stylesheet" href='<c:url value="/resources/css/lib/bootstrap.min.css"></c:url>'>
<link rel="stylesheet" href='<c:url value="/resources/css/lib/jquery-ui.min.css"></c:url>'>
<link rel="stylesheet" href='<c:url value="/resources/css/lib/bootstrap-datetimepicker.min.css"></c:url>'>
<link rel="stylesheet" href='<c:url value="/resources/css/lib/font-awesome-4.7.0/css/font-awesome.min.css"></c:url>' rel="stylesheet">
<link rel="stylesheet" href='<c:url value="/resources/js/lib/DataTables/datatables.min.css"></c:url>' rel="stylesheet">
<link rel="stylesheet" href='<c:url value="/resources/css/style.css"></c:url>'>
<script type="text/javascript" src='<c:url value="/resources/js/lib/jquery-3.3.1.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/bootstrap.bundle.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/jquery-ui.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/moment-with-locales.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/bootstrap-datetimepicker.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/DataTables/datatables.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/common.js"></c:url>'></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
var BASE_URL="<%=baseUrl%>";
</script>

<title>PC入替作業システム</title>
</head>
<body>
	<nav class="navbar navbar-light bg-light">
		<span class="navbar-brand mb-0 h1">PC入替作業システム</span>
	</nav>
	<div class="container-fluid">
		<div class="row content">
			<div class="col-md-2 p-0">
				<div class="just-padding border">
					<div class="list-group list-group-root well">
						<a href="#" class="list-group-item border-secondary text-secondary">
							<i class="fa fa-user" aria-hidden="true"></i>
							<span class="ml-2">
							<%
								out.print(loginUser.getUserEntity().getSagyouinName());
							%>
							</span>
						</a>
						<a href='<c:url value="/sagyouin"></c:url>' class="list-group-item  <%=(SystemConstant.PageID.SAGYOUIN_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">作業員一覧・作成</a>
						<a href="#" class="list-group-item border-secondary text-secondary">顧客情報</a>
						<div class="list-group">
							<a href="#" class="list-group-item text-secondary">顧客一覧・作成</a>
							<a href="<c:url value="/shiyousha"></c:url>" class="list-group-item <%=(SystemConstant.PageID.SHIYOUSHA_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">利用者一覧・作成</a>
						</div>
						<a href="javascript:void()" class="list-group-item border-secondary text-secondary">プロジェクト情報</a>
						<div class="list-group">
							<a href="<c:url value="/project/create"></c:url>" class="list-group-item <%=(SystemConstant.PageID.PROJECT_CREATE.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">プロジェクト作成</a>
							<a href='<c:url value="/kyuupc"></c:url>' class="list-group-item <%=(SystemConstant.PageID.KYUUPC_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">旧（引取）PC登録</a>
							<a href="<c:url value="/sagyou"></c:url>" class="list-group-item <%=(SystemConstant.PageID.SAGYOU_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">作業内容登録</a>
							<a href="<c:url value="/shinpc"></c:url>" class="list-group-item <%=(SystemConstant.PageID.SHINPC_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">新PC登録</a>
							<a href="<c:url value="/schedule"></c:url>" class="list-group-item <%=(SystemConstant.PageID.SCHEDULE_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">スケジュール登録</a>
						</div>
						<a href="<c:url value="/shinchoku"></c:url>" class="list-group-item border-secondary <%=(SystemConstant.PageID.SHINCHOKU_REGISTER.equals(pageId)?"rounded-0 text-white bg-secondary":"text-secondary") %>">進捗登録</a>
					</div>
				</div>
			</div>
			<div class="col-md-10 p-0">
				<jsp:doBody />
			</div>
		</div>
	</div>
</body>
</html>