<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:layout>
	<div id="modalContainer"></div>
	<div id="sagyouinPage" class="container-fluid p-0">
	<c:url value="/sagyouin" var="searchUrl"></c:url>
	<f:form id="frmRegister" modelAttribute="model" action="${searchUrl }" method="POST">
		<div class="card page p-0">
			<div class="card-header"><h4>作業員一覧・作成</h4></div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<c:if test="${not empty success}">
							<div class="alert alert-success" role="alert">${success}</div>
						</c:if>
						<c:if test="${not empty errors}">
							<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="err">
								${err.value}<br>
							</c:forEach>
							</div>
						</c:if>
						<c:set var="rowIdx" value="-1"></c:set>
						<hr />
						<div class="form-group row">
							<div class="col-md-12">
								<div class="table-wrapper">
									<table class="table table-bordered">
										<thead class="thead-light">
											<tr>
												<th scope="col">作業員番号</th>
												<th scope="col">作業員名</th>
												<th scope="col">パスワード</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach items="${model.detail }" var="item" varStatus="status">
										<c:set var="rowIdx" value="${status.index }"></c:set>
										<c:set var="key" value="Row${status.index }_totalCheck"></c:set>
										<c:set var="errorStyle" value=""></c:set>
										<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
										<c:set var="errorStyle" value="bg-danger"></c:set>
										</c:if>
										<tr class="${errorStyle }">
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<fmt:formatNumber value="${item.sagyouinNo}" pattern="00000" var="no"></fmt:formatNumber>
														<label class="">${no }</label>
														<f:hidden path="detail[${status.index }].sagyouinNo"/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].sagyouinName"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-left ${errorStyle }"
																path="detail[${status.index }].sagyouinName" />
														<f:hidden path="detail[${status.index }].updDate"/>
														<f:hidden path="detail[${status.index }].modified"/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].password"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control ${errorStyle }"
															path="detail[${status.index }].password" />
													</div>
												</div>
											</td>
										</tr>
										</c:forEach>
										<tr class="js-new-row">
											<td class="text-center font-weight-bold js-row-title">新規行</td>
											<td>

												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input class="form-control text-left"
																path="" id="new_sagyouinName"/>
														<f:hidden path="" id="new_modified"/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input class="form-control"
															path="" id="new_password" />
													</div>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
			<div class="card-body">
				<div class="float-right">
					<button type="submit" id="btnRegister" name="update" class="btn btn-success pl-5 pr-5">登録</button>
					<button type="button" id="btnDelete" class="btn btn-danger ml-2 pl-5 pr-5">削除</button>
				</div>
			</div>
	</div>
	</f:form>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/page/sagyouin/index.js"></script>
</t:layout>