<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-10">
						<form id="create_form" method="POST" class="">
							<div class="form-group row">
								<div class="col-md-4">
									<label for="name">プロジェクト名</label>
								</div>
								<div class="col-md-8">
									<input type="text" readonly="readonly" class="form-control" value="${project.sagyouProjectName }">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-4">
									<label for="name">企業名</label>
								</div>
								<div class="col-md-8">
									<input type="text" readonly="readonly" class="form-control" value="${project.kigyouName }">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-9"></div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-block" id="regis" type="button">検索</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="tblResult" class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>引取PC番号</th>
									<th>引取完了日</th>
									<th>リース契約番号</th>
									<th>コンピュータ名</th>
									<th>PC型式</th>
									<th>PC名称</th>
									<th>シリアル番号</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${hikitoriPCList}" var="item">
									<tr>
										<td>
											<fmt:formatNumber value="${item.hikitoriPcNo}" pattern="00000" var="no"></fmt:formatNumber>
											<fmt:formatDate value="${item.hikitoriKanryouDate }" var="dateStr"
															pattern="<%=SystemConstant.DATE_YYYYMMDD_FORMAT %>" />
										</td>
										<td class="text-right" data-id="${no}">${no}</td>
										<td class="text-center">${dateStr}</td>
										<td>${item.leaseKeiyakuNo}</td>
										<td>${item.pcName}</td>
										<td>${item.pcKatashiki}</td>
										<td>${item.pcMeishou}</td>
										<td>${item.serialNo}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSelect" class="btn btn-primary">選択</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
			</div>
		</div>
	</div>
</div>