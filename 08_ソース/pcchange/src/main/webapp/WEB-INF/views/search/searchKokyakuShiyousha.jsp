<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-10">
						<form id="create_form" method="POST" class="">
							<div class="form-group row">
								<div class="col-md-4">
									<label for="name">プロジェクト名</label>
								</div>
								<div class="col-md-8">
									<input type="text" readonly="readonly" class="form-control" id="sagyou_project_name" value="${project.sagyouProjectName }">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-9"></div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-block" id="regis" type="button">検索</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="tblKokyaku" class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>顧客使用者番号</th>
									<th>事業部門（拠点）名</th>
									<th>部課名</th>
									<th>使用者名</th>
									<th>内線番号</th>
									<th>メールアドレス</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${kokyakuShiyoushaList}" var="item">
									<tr>
										<td><fmt:formatNumber value="${item.kokyakuShiyoushaNo}" pattern="00000" var="no"></fmt:formatNumber></td>
										<td class="text-right" data-id="${no}">${no}</td>
										<td>${item.kokyaku.kokyakuKigyouName}</td>
										<td>${item.kokyakuBumon.jigyoubuName}</td>
										<td>${item.shiyoushaName}</td>
										<td class="text-right">${item.naisenNo}</td>
										<td>${item.mailAddress}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSelect" class="btn btn-primary">選択</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
			</div>
		</div>
	</div>
</div>