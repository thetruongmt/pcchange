<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<t:layout>
	<div class="container-fluid p-0">
		<div class="card page p-0">
			<div class="card-header"><h4>プロジェクト作成</h4></div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="col-md-4"></div>
						<div class="col-md-8">
							<f:form method="POST" modelAttribute="model">
								<c:if test="${not empty msg}">
									<div class="alert alert-success" role="alert">${msg}</div>
								</c:if>

								<c:if test="${not empty errors}">
									<div class="alert alert-danger" role="alert">
									<c:forEach items="${errors}" var="err">
										${err.getDefaultMessage()}<br>
									</c:forEach>
									</div>
								</c:if>
								<div class="form-group row">
									<label for="sagyouProjectName" class="col-md-4 col-form-label">プロジェクト名</label>
									<div class="col-md-8">
										<f:input type="text" class="form-control" path="sagyouProjectName" />
									</div>
								</div>
								<div class="form-group row">
									<label for="kokyakuNo" class="col-md-4 col-form-label">顧客企業名</label>
									<div class="col-md-8">
										<f:select class="form-control" path="kokyakuNo" items="${kokyakuList }" itemLabel="kokyakuKigyouName" itemValue="kokyakuNo"></f:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4 col-form-label">作業期間</label>
									<div class="col-md-4">
										<div class="input-group date" data-provide="datepicker">
											<f:input path="sagyouKikanFrom" class="form-control js-date-picker" />
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="input-group date" data-provide="datepicker">
											<f:input path="sagyouKikanTo" class="form-control js-date-picker" />
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-9"></div>
									<div class="col-md-3">
										<button class="btn btn-lg btn-primary btn-block" id="regis" type="submit">登録</button>
									</div>
								</div>
							</f:form>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</div>
	</div>
</t:layout>