<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:layout>
	<div id="modalContainer"></div>
	<div id="schedulePage" class="container-fluid p-0">
	<c:url value="/schedule" var="searchUrl"></c:url>
	<f:form id="frmRegister" modelAttribute="model" action="${searchUrl }" method="POST">
		<div class="card page p-0">
			<div class="card-header"><h4>スケジュール登録</h4></div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-1"></div>
					<div class="col-md-10">
							<c:if test="${not empty errors}">
								<div class="alert alert-danger" role="alert">
								<c:forEach items="${errors}" var="err">
									${err.getDefaultMessage()}<br>
								</c:forEach>
								</div>
							</c:if>
							<div class="form-group row">
								<label for="sagyouProjectNo" class="col-md-3 col-form-label">プロジェクト名</label>
								<div class="col-md-8">
									<f:select class="form-control" path="sagyouProjectNo" items="${projects }"
										itemLabel="sagyouProjectName" itemValue="sagyouProjectNo"></f:select>
								</div>
							</div>
							<div class="form-group row">
								<label for="sagyouMonth" class="col-md-3 col-form-label">作業年月</label>
								<div class="col-md-8">
									<f:input path="sagyouMonth" class="form-control js-month-picker" />
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-3"></div>
								<div class="col-md-8">
									<button type="submit" name="search" class="btn btn-primary float-right pl-5 pr-5">検索</button>
								</div>
							</div>
							<c:if test="${model.scheduleMeisai != null && model.scheduleMeisai.size() > 0}">
							<div class="form-group row">
								<div class="table-input-wrapper">
									<table class="table table-bordered first-column-header table-input">
										<thead class="thead-light">
											<tr>
												<th scope="col">#</th>
												<th scope="col">作業対象顧客使用者名１</th>
												<th scope="col">作業対象顧客使用者名２</th>
												<th scope="col">作業対象顧客使用者名３</th>
												<th scope="col">作業対象顧客使用者名４</th>
												<th scope="col">作業対象顧客使用者名５</th>
												<th scope="col">作業対象顧客使用者名６</th>
												<th scope="col">作業対象顧客使用者名７</th>
												<th scope="col">作業対象顧客使用者名８</th>
												<th scope="col">作業対象顧客使用者名９</th>
												<th scope="col">作業対象顧客使用者名１０</th>
												<th scope="col">作業対象顧客使用者名１１</th>
												<th scope="col">作業対象顧客使用者名１２</th>
												<th scope="col">作業対象顧客使用者名１３</th>
												<th scope="col">作業対象顧客使用者名１４</th>
												<th scope="col">作業対象顧客使用者名１５</th>
												<th scope="col">作業対象顧客使用者名１６</th>
												<th scope="col">作業対象顧客使用者名１７</th>
												<th scope="col">作業対象顧客使用者名１８</th>
												<th scope="col">作業対象顧客使用者名１９</th>
												<th scope="col">作業対象顧客使用者名２０</th>
												<th scope="col">作業対象顧客使用者名２１</th>
												<th scope="col">作業対象顧客使用者名２２</th>
												<th scope="col">作業対象顧客使用者名２３</th>
												<th scope="col">作業対象顧客使用者名２４</th>
												<th scope="col">作業対象顧客使用者名２５</th>
												<th scope="col">作業対象顧客使用者名２６</th>
												<th scope="col">作業対象顧客使用者名２７</th>
												<th scope="col">作業対象顧客使用者名２８</th>
												<th scope="col">作業対象顧客使用者名２９</th>
												<th scope="col">作業対象顧客使用者名３０</th>
											</tr>
										</thead>
										<tbody>
												<c:forEach items="${model.scheduleMeisai }" var="item" varStatus="status">
													<tr>
														<fmt:formatDate value="${item.sagyouDate }" var="dateStr"
															pattern="<%=SystemConstant.DATE_MMDD_DAY_FORMAT %>" />
														<td>
															${dateStr}
															<fmt:formatDate value="${item.sagyouDate }" var="dateStr"
															pattern="<%=SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT %>" />
															<input type="hidden" value="${dateStr }" name="scheduleMeisai[${status.index }].sagyouDate" /></td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo1"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right ${errorStyle }"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo1" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo2"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right ${errorStyle }"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo2" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo3"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo3" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo4"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo4" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo5"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo5" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo6"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo6" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo7"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo7" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo8"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo8" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo9"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo9" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo10"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo10" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light text-right">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo11"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo11" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo12"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo12" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo13"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo13" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo14"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo14" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo15"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo15" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo16"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo16" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo17"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo17" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo18"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo18" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo19"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo19" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo20"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo20" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo21"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo21" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo22"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo22" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo23"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo23" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo24"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo24" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo25"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo25" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo26"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo26" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo27"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo27" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo28"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo28" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo29"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo29" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
														<td>
															<div class="row form-group">
																<div class="col-md-9">
																	<c:set var="errorStyle" value=""></c:set>
																	<c:set var="key" value="Row_${status.index }_sagyouKokyakuShiyoshaNo30"></c:set>
																	<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
																	<c:set var="errorStyle" value="is-invalid"></c:set>
																	</c:if>
																	<f:input class="form-control text-right"
																		path="scheduleMeisai[${status.index }].sagyouKokyakuShiyoshaNo30" />
																</div>
																<div class="col-md-3">
																	<a class="badge badge-light">...</a>
																</div>
															</div>
														</td>
													</tr>
												</c:forEach>
										</tbody>
									</table>
								</div>

							</div>
							<div class="form-group row">
								<label class="col-md-4 offset-md-2 col-form-label">スケジュール済み作業対象使用者件数</label>
								<div class="col-md-2">
									<f:hidden path="scheduleSumiCount"/>
									<input class="form-control text-right"  disabled="disabled" value="${model.scheduleSumiCount }"/>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-2 offset-md-2 col-form-label">作業期間</label>
								<div class="col-md-3">
									<div class="input-group ">
										<fmt:formatDate value="${project.sagyouKikanFrom }" pattern="<%=SystemConstant.DATE_YYYYMMDD_FORMAT %>" var="dateObj"/>
										<input class="form-control text-center" disabled="disabled" value="${dateObj }"/>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group ">
										<fmt:formatDate value="${project.sagyouKikanTo }" pattern="<%=SystemConstant.DATE_YYYYMMDD_FORMAT %>" var="dateObj"/>
										<input class="form-control text-center" disabled="disabled" value="${dateObj }">
									</div>
								</div>
							</div>
							<f:hidden path="updDate" />
							</c:if>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>

			<c:if test="${model.scheduleMeisai != null && model.scheduleMeisai.size() > 0}">
			<div class="card-body">
				<button type="submit" id="btnRegister" name="update" class="btn btn-success float-right pl-5 pr-5">登録</button>
			</div>
			</c:if>
	</div>
	</f:form>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/page/schedule/index.js"></script>
</t:layout>