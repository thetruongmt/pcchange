<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<t:layout>
	<div class="container-fluid p-0">
		<div class="card page p-0">
			<div class="card-header">作業内容登録</div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<c:url value="/sagyou/search" var="searchUrl"></c:url>
						<f:form id="frmRegister" class="sagyoform" action="${searchUrl }" modelAttribute="model"
							method="POST">
							<c:if test="${not empty msg}">
									<div class="alert alert-success" role="alert">${msg}</div>
								</c:if>

								<c:if test="${not empty errors}">
									<div class="alert alert-danger" role="alert">
									<c:forEach items="${errors}" var="err">
										${err.getDefaultMessage()}<br>
									</c:forEach>
									</div>
								</c:if>
							<div class="form-group row">
								<label for="sagyoProjectNo" class="col-md-3 col-form-label">プロジェクト名</label>
								<div class="col-md-8">
									<f:select class="form-control" path="sagyouProjectNo" items="${projects }"
										itemLabel="sagyouProjectName" itemValue="sagyouProjectNo"></f:select>
								</div>
							</div>
							<div class="form-group row">
								<label for="kokyaku_name" class="col-md-3 col-form-label">顧客企業名</label>
								<div class="col-md-8">
									<div class="d-none">
										<select class="form-control" id="kokyaku_list" name="kokyakuNo">
											<c:forEach items="${kokyakuList}" var="item" varStatus="count">
												<option data-projectno="${item.sagyouProjectNo}" value="${item.kokyakuNo}">${item.kokyakuKigyouName}</option>
											</c:forEach>
										</select>
									</div>
									<f:input disabled="true" path="kokyakuKigyouName" class="form-control" />
								</div>
							</div>
							<div class="form-group row">
								<label for="kokyaku_bumon_no" class="col-md-3 col-form-label">事業部門(拠点)名</label>
								<div class="col-md-8">
									<f:select class="form-control" path="kokyakuBumonNo" items="${kokyakuBumons }"
										itemLabel="jigyoubuName" itemValue="kokyakuBumonNo"></f:select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-3"></div>
								<div class="col-md-8">
									<button  name="search" id="btnSearch" type="submit" class="btn btn-primary float-right pl-5 pr-5">検索</button>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<label for="sagyou_name" class="col-md-3 col-form-label">作業名</label>
								<div class="col-md-8">
									<f:input path="sagyouName" class="form-control" />
								</div>
							</div>
							<div class="form-group settei-area row border border-primary mt-5 pb-5">
								<div class="col-md-12">
									<div class="row mt-5">
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目１</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku1" rows="3" />
										</div>
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目2</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku2" rows="3" />
										</div>
									</div>
									<div class="row mt-5">
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目3</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku3" rows="3" />
										</div>
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目4</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku4" rows="3" />
										</div>
									</div>
									<div class="row mt-5">
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目5</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku5" rows="3" />
										</div>
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目6</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku6" rows="3" />
										</div>
									</div>
									<div class="row mt-5">
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目7</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku7" rows="3" />
										</div>
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目8</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku8" rows="3" />
										</div>
									</div>
									<div class="row mt-5">
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目9</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku9" rows="3" />
										</div>
										<label for="sagyou_name" class="col-md-2 col-form-label">作業設定項目10</label>
										<div class="col-md-4">
											<f:textarea class="form-control" path="sagyouSetteiKoumoku10" rows="3" />
										</div>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-10"></div>
								<div class="col-md-2">
									<button class="btn btn-success btn-block" name="save" id="btnRegister" type="submit">登録</button>
								</div>
							</div>
						</f:form>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
	</div>

	<script src="${pageContext.request.contextPath}/resources/js/page/sagyou/index.js"></script>
</t:layout>