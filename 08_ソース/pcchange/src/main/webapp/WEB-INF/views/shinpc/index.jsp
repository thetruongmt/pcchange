<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:layout>
	<div id="modalContainer"></div>
	<div id="shinpcPage" class="container-fluid p-0">
	<c:url value="/shinpc" var="searchUrl"></c:url>
	<f:form id="frmRegister" modelAttribute="model" action="${searchUrl }" method="POST">
		<div class="card page p-0">
			<div class="card-header"><h4>新PC登録</h4></div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-12">
						<c:if test="${not empty success}">
							<div class="alert alert-success" role="alert">${success}</div>
						</c:if>
						<c:if test="${not empty errors}">
							<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="err">
								${err.value}<br>
							</c:forEach>
							</div>
						</c:if>
						<div class="form-group row">
							<label for="sagyouProjectNo" class="col-md-3 col-form-label">プロジェクト名</label>
							<div class="col-md-8">
								<f:select class="form-control" path="sagyouProjectNo" items="${projects }"
									itemLabel="sagyouProjectName" itemValue="sagyouProjectNo"></f:select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-3"></div>
							<div class="col-md-8">
								<button type="submit" name="search" id="btnSearch" class="btn btn-primary float-right pl-5 pr-5">検索</button>
							</div>
						</div>
						<c:if test="${searched != null }">
						<hr />
						<f:hidden path="currentPage"/>
						<f:hidden path="totalPage"/>
						<div class="form-group row">
							<div class="col-md-12">
								<c:if test="${model.totalPage > 1 }">
								<div class="mt-1 float-right js-pagging-nav">
									<nav aria-label="navigation">
									  <ul class="pagination justify-content-center">
									  	<c:set var="disabled" value=""></c:set>
									  	<c:if test="${model.currentPage == 1 }">
									  	<c:set var="disabled" value="disabled"></c:set>
									  	</c:if>
									    <li class="page-item ${disabled }">
									    	<a class="page-link" href="javascript:void(0);" tabindex="-1" aria-disabled="true" data-page="${(disabled == 'disabled')?'':model.currentPage-1 }">前へ</a>
									    </li>
									    <c:set var="startIdx" value="${model.currentPage-1 }"></c:set>
									    <c:if test="${startIdx <= 0 }">
									    <c:set var="startIdx" value="1"></c:set>
									    </c:if>
									    <c:set var="endIdx" value="${startIdx + 4 }"></c:set>
									    <c:if test="${endIdx > model.totalPage }">
									    <c:set var="endIdx" value="${model.totalPage }"></c:set>
									    </c:if>
									    <c:if test="${startIdx > 1 }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);">...</a></li>
									    </c:if>
									    <c:forEach begin="${startIdx }" end="${endIdx }" var="page">
									    <c:if test="${page == model.currentPage }">
									    <li class="page-item active" aria-current="page">
									    	<a class="page-link" href="javascript:void(0);">${page }</a>
									    </li>
									    </c:if>
									    <c:if test="${page != model.currentPage }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);" data-page="${page }">${page }</a></li>
									    </c:if>
									    </c:forEach>
									    <c:if test="${endIdx < model.totalPage }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);">...</a></li>
									    </c:if>
									    <c:set var="disabled" value=""></c:set>
									  	<c:if test="${model.currentPage == model.totalPage }">
									  	<c:set var="disabled" value="disabled"></c:set>
									  	</c:if>
									    <li class="page-item ${disabled }">
									    	<a class="page-link" href="javascript:void(0);" data-page="${(disabled == 'disabled')?'':model.currentPage+1 }">次へ</a>
									    </li>
									  </ul>
									</nav>
								</div>
								</c:if>
								<div class="table-input-wrapper">
									<table id="shinpcTb" class="table table-bordered table-input" style="table-layout: fixed">
										<thead class="thead-light">
											<tr>
												<th scope="col" width="10px">#</th>
												<th scope="col" width="180px">作業(引当)PC個別番号</th>
												<th scope="col" width="130px">作業員名</th>
												<th scope="col" width="110px">引取PC番号</th>
												<th scope="col" width="150px">作業名</th>
												<th scope="col" width="240px">シリアル番号</th>
												<th scope="col" width="130px">顧客使用者名</th>
												<th scope="col" width="180px">コンピュータ名</th>
												<th scope="col" width="180px">ネットワーク設定１</th>
												<th scope="col" width="180px">ネットワーク設定２</th>
												<th scope="col" width="180px">設定項目３</th>
												<th scope="col" width="180px">設定項目４</th>
												<th scope="col" width="180px">設定項目５</th>
												<th scope="col" width="180px">設定項目６</th>
												<th scope="col" width="180px">設定項目７</th>
												<th scope="col" width="180px">設定項目８</th>
												<th scope="col" width="180px">設定項目９</th>
												<th scope="col" width="180px">設定項目１０</th>
												<th scope="col" width="180px">作業ステータス</th>
											</tr>
										</thead>
										<tbody>
										<c:set var="rowIdx" value="-1"></c:set>
										<c:forEach items="${model.detail }" var="item" varStatus="status">
										<c:set var="rowIdx" value="${status.index }"></c:set>
										<c:set var="key" value="Row${status.index }_totalCheck"></c:set>
										<c:set var="errorStyle" value=""></c:set>
										<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
										<c:set var="errorStyle" value="bg-danger"></c:set>
										</c:if>
										<tr class="${errorStyle }">
											<td class="text-center">
												<f:hidden path="detail[${status.index }].modified"/>
												<f:hidden path="detail[${status.index }].sagyouHikiatePcKobetsuNo"/>
												<f:hidden path="detail[${status.index }].updDate"/>
											</td>
											<td class="text-right">
												<fmt:formatNumber value="${item.sagyouHikiatePcKobetsuNo}" pattern="00000" var="no"></fmt:formatNumber>
												<label class="">${no }</label>
												<f:hidden path="detail[${status.index }].sagyouHikiatePcKobetsuNo"/>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].sagyouin.sagyouinNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:select class="form-control ${errorStyle }" path="detail[${status.index }].sagyouin.sagyouinNo" >
															<f:option value=""></f:option>
															<f:options items="${sagyouins }" itemLabel="sagyouinName" itemValue="sagyouinNo"/>
														</f:select>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="input-group col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].hikitoriPCNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-right ${errorStyle }" path="detail[${status.index }].hikitoriPCNo"/>
														<div class="input-group-append">
															<button class="btn btn-secondary js-hikitori-pc-search" type="button">…</button>
														</div>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].sagyouNaiyou.sagyouNaiyouNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:select class="form-control ${errorStyle }" path="detail[${status.index }].sagyouNaiyou.sagyouNaiyouNo" >
															<f:option value=""></f:option>
															<f:options items="${sagyouNaiyous }" itemLabel="sagyouName" itemValue="sagyouNaiyouNo"/>
														</f:select>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].serialNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input path="detail[${status.index }].serialNo" class="form-control text-left ${errorStyle }" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="input-group col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].kokyakuShiyousha.kokyakuShiyoushaNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-right ${errorStyle }" readonly="true" path="detail[${status.index }].kokyakuShiyousha.shiyoushaName"/>
														<f:hidden path="detail[${status.index }].kokyakuShiyousha.kokyakuShiyoushaNo"/>
														<div class="input-group-append">
															<button class="btn btn-secondary js-kokyaku-shiyousha-search" type="button">…</button>
														</div>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="detail[${status.index }].pcName"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input path="detail[${status.index }].pcName" class="form-control text-left ${errorStyle }" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].network1" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].network2" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku3" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku4" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku5" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku6" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku7" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku8" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku9" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="detail[${status.index }].setteiKoumoku10" class="form-control text-left " />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:select class="form-control" path="detail[${status.index }].sagyouStatus.sagyouStatusNo" >
															<f:option value=""></f:option>
															<f:options items="${statuses }" itemLabel="status" itemValue="sagyouStatusNo"/>
														</f:select>
													</div>
												</div>
											</td>
										</tr>
										</c:forEach>
										<tr class="js-new-row">
											<td class="text-center">
												<f:hidden path="" id="new_modified" />
											</td>
											<td class="text-center font-weight-bold js-row-title">新規行</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:select class="form-control" path="" id="new_sagyouinNo">
															<f:option value=""></f:option>
															<f:options items="${sagyouins }" itemLabel="sagyouinName" itemValue="sagyouinNo"/>
														</f:select>
													</div>
												</div>
											</td>
											<td>
												<div class="input-group col-md-12 p-0">
													<f:input class="form-control text-right" path="" id="new_hikitoriPCNo"/>
													<div class="input-group-append">
														<button class="btn btn-secondary" type="button">…</button>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:select class="form-control" path="" id="new_sagyouNaiyouNo" >
															<f:option value=""></f:option>
															<f:options items="${sagyouNaiyous }" itemLabel="sagyouName" itemValue="sagyouNaiyouNo"/>
														</f:select>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left "  id="new_serialNo"/>
													</div>
												</div>
											</td>
											<td>
												<div class="input-group col-md-12 p-0">
													<f:input class="form-control text-right" readonly="true"  path="" id="new_shiyoushaName"/>
													<f:hidden path="" id="kokyakuShiyoushaNo"/>
													<div class="input-group-append">
														<button class="btn btn-secondary" type="button">…</button>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_pcName"/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_network1"/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_network2" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku3" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku4" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku5" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku6" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku7" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku8" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku9" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:input path="" class="form-control text-left " id="new_setteiKoumoku10" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<f:select class="form-control" path="" id="new_sagyouStatusNo" >
															<f:option value=""></f:option>
															<f:options items="${statuses }" itemLabel="status" itemValue="sagyouStatusNo"/>
														</f:select>
													</div>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
								<c:if test="${model.totalPage > 1 }">
								<div class="mt-1 float-right js-pagging-nav">
									<nav aria-label="navigation">
									  <ul class="pagination justify-content-center">
									  	<c:set var="disabled" value=""></c:set>
									  	<c:if test="${model.currentPage == 1 }">
									  	<c:set var="disabled" value="disabled"></c:set>
									  	</c:if>
									    <li class="page-item ${disabled }">
									    	<a class="page-link" href="javascript:void(0);" tabindex="-1" aria-disabled="true" data-page="${(disabled == 'disabled')?'':model.currentPage-1 }">前へ</a>
									    </li>
									    <c:set var="startIdx" value="${model.currentPage-1 }"></c:set>
									    <c:if test="${startIdx <= 0 }">
									    <c:set var="startIdx" value="1"></c:set>
									    </c:if>
									    <c:set var="endIdx" value="${startIdx + 4 }"></c:set>
									    <c:if test="${endIdx > model.totalPage }">
									    <c:set var="endIdx" value="${model.totalPage }"></c:set>
									    </c:if>
									    <c:if test="${startIdx > 1 }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);">...</a></li>
									    </c:if>
									    <c:forEach begin="${startIdx }" end="${endIdx }" var="page">
									    <c:if test="${page == model.currentPage }">
									    <li class="page-item active" aria-current="page">
									    	<a class="page-link" href="javascript:void(0);">${page }</a>
									    </li>
									    </c:if>
									    <c:if test="${page != model.currentPage }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);" data-page="${page }">${page }</a></li>
									    </c:if>
									    </c:forEach>
									    <c:if test="${endIdx < model.totalPage }">
									    <li class="page-item"><a class="page-link" href="javascript:void(0);">...</a></li>
									    </c:if>
									    <c:set var="disabled" value=""></c:set>
									  	<c:if test="${model.currentPage == model.totalPage }">
									  	<c:set var="disabled" value="disabled"></c:set>
									  	</c:if>
									    <li class="page-item ${disabled }">
									    	<a class="page-link" href="javascript:void(0);" data-page="${(disabled == 'disabled')?'':model.currentPage+1 }">次へ</a>
									    </li>
									  </ul>
									</nav>
								</div>
								</c:if>
							</div>
						</div>
						</c:if>
					</div>
				</div>
			</div>
			<c:if test="${searched != null }">
			<div class="card-body">
				<div class="float-right">
					<button type="submit" id="btnRegister" name="update" class="btn btn-success pl-5 pr-5">登録</button>
					<button type="button" id="btnDelete" class="btn btn-danger ml-2 pl-5 pr-5">削除</button>
				</div>
			</div>
			</c:if>
	</div>
	</f:form>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/page/shinpc/index.js"></script>
</t:layout>