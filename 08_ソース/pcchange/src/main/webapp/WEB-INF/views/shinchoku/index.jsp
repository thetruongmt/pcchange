<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:layout>
	<div id="modalContainer"></div>
	<div id="shinchokuPage" class="container-fluid p-0">
	<f:form id="frmRegister" modelAttribute="model" action="" method="POST">
		<div class="card page p-0">
			<div class="card-header">
				<h4>進捗登録</h4>
				<h6 class="card-subtitle mb-2 text-muted">新PC登録にて定義した内容に進捗を登録する。</h6>
			</div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-12">
						<c:if test="${not empty errors}">
							<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="err">
								${err.getDefaultMessage()}<br>
							</c:forEach>
							</div>
						</c:if>
						<div class="form-group row">
							<label for="sagyouProjectNo" class="col-md-3 col-form-label">プロジェクト名</label>
							<div class="col-md-4">
								<f:select class="form-control" path="sagyouProjectNo" items="${projects }"
									itemLabel="sagyouProjectName" itemValue="sagyouProjectNo"></f:select>
							</div>
						</div>
						<div class="form-group row">
							<label for="sagyouStatusNo" class="col-md-3 col-form-label">作業ステータス</label>
							<div class="col-md-4">
								<f:select class="form-control" path="sagyouStatusNo">
									<f:option value="" label=""></f:option>
									<f:options items="${statuses }" itemLabel="status" itemValue="sagyouStatusNo"/>
								</f:select>
							</div>
						</div>
						<div class="form-group row">
							<label for="sagyouHikiatePcKobetsuNo" class="col-md-3 col-form-label">作業(引当)PC個別番号</label>
							<div class="col-md-4">
								<f:input class="form-control" path="sagyouHikiatePcKobetsuNo"/>
							</div>
							<div class="col-md-3">
								<button type="submit" name="search" class="btn btn-primary pl-5 pr-5">検索</button>
							</div>
						</div>
						<f:hidden path="totalPageCnt"/>
						<f:hidden path="currentPage"/>
						<c:if test="${searched != null && model.detail == null }">
						<hr />
						<div class="form-group row mt-3 text-center">
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">該当データが存在していません。</div>
							</div>
						</div>
						</c:if>
						<c:if test="${model.detail != null }">
						<div class="form-group row mt-3">
							<div class="col-md-3">
							<f:hidden path="detail.updDate"/>
							</div>
							<div class="col-md-4"></div>
							<div class="col-md-5">
								<label class="paging-item p-1 mb-2 bg-secondary text-white text-center">${model.currentPage }</label>
								<label class="">/</label>
								<label class="paging-item p-1 mb-2 bg-secondary text-white text-center">${model.totalPageCnt }</label>
								<label class="">ページ</label>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
							<c:if test="${model.currentPage > 1 }">
							<button type="submit" id="btnPrev" name="prev" class="btn btn-secondary float-left pl-5 pr-5">前ページ</button>
							</c:if>
							<c:if test="${model.currentPage <= 1 }">
							<button type="button" class="btn btn-secondary float-left pl-5 pr-5" disabled="disabled">前ページ</button>
							</c:if>
							<c:if test="${model.currentPage < model.totalPageCnt }">
							<button type="submit" id="btnNext" name="next" class="btn btn-secondary float-right pl-5 pr-5">次ページ</button>
							</c:if>
							<c:if test="${model.currentPage >= model.totalPageCnt }">
							<button type="button" class="btn btn-secondary float-right pl-5 pr-5" disabled="disabled">次ページ</button>
							</c:if>
							</div>
							<div class="col-md-1"></div>
						</div>
						<div class="form-group row mt-3">
							<div class="col-md-6">
								<div class="form-group row">
									<label for="" class="col-md-5 col-form-label">作業(引当)PC個別番号</label>
									<div class="col-md-7">
										<f:input class="form-control" readonly="true" path="detail.sagyouHikiatePcKobetsuNo" />
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-5 col-form-label">引取PC番号</label>
									<div class="col-md-7">
										<f:input class="form-control" readonly="true" path="detail.hikitoriPCNo" />
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-5 col-form-label">作業名</label>
									<div class="col-md-7">
										<f:input class="form-control" readonly="true"  path="detail.sagyouNaiyou.sagyouName"/>
									</div>
								</div>
								<div class="form-group row">
									<label for="" class="col-md-5 col-form-label">顧客使用者名</label>
									<div class="col-md-7">
										<f:input class="form-control" readonly="true" path="detail.kokyakuShiyousha.shiyoushaName" />
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-body">
										<div class="row">
											<label for="" class="col-md-5 col-form-label">作業ステータス</label>
											<div class="col-md-7">
												<f:select class="form-control" path="detail.sagyouStatus.sagyouStatusNo">
													<f:option value="" label=""></f:option>
													<f:options items="${statuses }" itemLabel="status" itemValue="sagyouStatusNo"/>
												</f:select>
											</div>
										</div>
										<div class="row mt-1">
											<label for="" class="col-md-5 col-form-label">作業員名</label>
											<div class="col-md-7">
												<f:select path="detail.sagyouin.sagyouinNo">
													<f:option value="" label=""></f:option>
													<f:options items="${sagyouins }" itemLabel="sagyouinName" itemValue="sagyouinNo"/>
												</f:select>
											</div>
										</div>
										<div class="row mt-1">
											<label for="" class="col-md-5 col-form-label">シリアル番号</label>
											<div class="col-md-7">
												<f:input class="form-control" path="detail.serialNo"/>
											</div>
										</div>
										<div class="row mt-1">
											<label for="" class="col-md-5 col-form-label">コンピュータ名</label>
											<div class="col-md-7">
												<f:input class="form-control" path="detail.pcName"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-5 col-form-label">ネットワーク設定１</label>
										<div class="col-md-7">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.network1"></f:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-5 col-form-label">引取PC番号</label>
										<div class="col-md-7">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.network2"></f:textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目３</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku3"></f:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目４</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku4"></f:textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目５</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku5"></f:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目６</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku6"></f:textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目７</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku7"></f:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目８</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku8"></f:textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目９</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku9"></f:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-md-3 col-form-label">設定項目１０</label>
										<div class="col-md-9">
											<f:textarea class="form-control" rows="3" readonly="true" path="detail.setteiKoumoku10"></f:textarea>
									</div>
								</div>
							</div>
						</div>
						</c:if>
					</div>
				</div>
			</div>
			<c:if test="${model.detail != null }">
			<div class="card-body">
				<button type="submit" name="update" id="btnRegister" class="btn btn-success float-right pl-5 pr-5">登録</button>
			</div>
			</c:if>
	</div>
	</f:form>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/page/shinchoku/index.js"></script>
</t:layout>