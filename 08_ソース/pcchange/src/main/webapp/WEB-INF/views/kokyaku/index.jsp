<%@page import="com.mediatrust.pcchange.constant.SystemConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:layout>
	<div id="modalContainer"></div>
	<div id="shiyoushaPage" class="container-fluid p-0">
	<c:url value="/kokyaku" var="searchUrl"></c:url>
	<f:form id="frmRegister" modelAttribute="model" action="${searchUrl }" method="POST">
		<div class="card page p-0">
			<div class="card-header"><h4>顧客一覧・作成</h4></div>
			<div class="card-body">
				<div class="row content">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<c:if test="${not empty success}">
							<div class="alert alert-success" role="alert">${success}</div>
						</c:if>
						<c:if test="${not empty errors}">
							<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="err">
								${err.value}<br>
							</c:forEach>
							</div>
						</c:if>
						<c:set var="rowIdx" value="-1"></c:set>
						<hr />
						<div class="form-group row">
							<div class="col-md-12">
								<div class="table-wrapper">
									<table class="table table-bordered">
										<thead class="thead-light">
											<tr>
												<th scope="col">企業名</th>
												<th scope="col">事業部門（拠点）名</th>
												<th scope="col">部課名</th>
												<th scope="col">住所１</th>
												<th scope="col">住所２</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach items="${model }" var="item" varStatus="status">
										<c:set var="rowIdx" value="${status.index }"></c:set>
										<c:set var="key" value="Row${status.index }_totalCheck"></c:set>
										<c:set var="errorStyle" value=""></c:set>
										<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
										<c:set var="errorStyle" value="bg-danger"></c:set>
										</c:if>
										<tr class="${errorStyle }">
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="shiyoushas[${status.index }].shiyoushaName"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-left ${errorStyle }"
																path="shiyoushas[${status.index }].shiyoushaName" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="shiyoushas[${status.index }].naisenNo"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-right ${errorStyle }"
															path="shiyoushas[${status.index }].naisenNo" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<c:set var="errorStyle" value=""></c:set>
														<c:set var="key" value="shiyoushas[${status.index }].mailAddress"></c:set>
														<c:if test="${not empty errorDetails && not empty errorDetails[key] }">
														<c:set var="errorStyle" value="is-invalid"></c:set>
														</c:if>
														<f:input class="form-control text-left ${errorStyle }"
																path="shiyoushas[${status.index }].mailAddress" />
													</div>
												</div>
											</td>
										</tr>
										</c:forEach>
										<tr class="js-new-row">
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<label class=""></label>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<input id="new_shiyoushaName" type="text" class="form-control text-left "/>
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<input id="new_naisenNo" type="text" class="form-control text-right" />
													</div>
												</div>
											</td>
											<td>
												<div class="row form-group m-0">
													<div class="col-md-12 p-0">
														<input id="new_mailAddress" type="text" class="form-control text-left" />
													</div>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
			<c:if test="${searched != null }">
			<div class="card-body">
				<div class="float-right">
					<button type="submit" id="btnRegister" name="update" class="btn btn-success pl-5 pr-5">登録</button>
					<button type="button" id="btnDelete" class="btn btn-danger ml-2 pl-5 pr-5">削除</button>
				</div>
			</div>
			</c:if>
	</div>
	</f:form>
	</div>
	<script src="${pageContext.request.contextPath}/resources/js/page/shiyousha/index.js"></script>
</t:layout>