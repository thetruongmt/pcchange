<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<c:set var="scheme" value="${pageContext.request.scheme}"/>
<c:set var="serverPort" value="${pageContext.request.serverPort}"/>
<c:set var="port" value=":${serverPort}"/>
<c:set var="app" value="${pageContext.request.contextPath }" />
<c:set var="baseUrl" value="${scheme}://${pageContext.request.serverName}${port}${app}" />

<!DOCTYPE html>
<html >
<head>
<link rel="stylesheet" href='<c:url value="/resources/css/lib/bootstrap.min.css"></c:url>'>
<link rel="stylesheet" href='<c:url value="/resources/css/style.css"></c:url>'>
<script type="text/javascript"  src='<c:url value="/resources/js/lib/jquery-3.3.1.min.js"></c:url>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/lib/bootstrap.min.js"></c:url>'></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row content">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div id="login_wrap">
					<div class=" card">
						<div class="card-header"><h4 class="font-weight-bold">ログイン</h4></div>
						<div class="card-body">
							<f:form class="form-signin" modelAttribute="model" action="${baseUrl }/login" >
								<div class="form-group">
									<label for="sagyouinNo">作業員番号</label>
									<f:input type="text" class="form-control" placeholder="" path="sagyouinNo"/>
								</div>
								<div class="form-group">
									<label for="password">パスワード</label>
									<f:password class="form-control" path="password" />
								</div>
								<c:if test="${not empty error}">
									<p style="font-size: 16px; color: red; margin-top: 20px;">${error}</p>
								</c:if>
								<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
							</f:form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

</body>
</html>
