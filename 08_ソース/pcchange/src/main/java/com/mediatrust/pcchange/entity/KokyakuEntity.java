package com.mediatrust.pcchange.entity;

import lombok.Data;

/**
 *
 * @author khang.tran
 *
 */
@Data
public class KokyakuEntity {
	private Integer kokyakuNo;
	private String kokyakuKigyouName;
}
