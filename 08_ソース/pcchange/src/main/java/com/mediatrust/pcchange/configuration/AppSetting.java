package com.mediatrust.pcchange.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import lombok.Data;

/**
 * システムの設定（appsetting.propertiesファイルからの設定）
 * @author the.truong
 *
 */
@Data
@Configuration
@PropertySource("classpath:appsetting.properties")
public class AppSetting {

	/**
	 * ページの件数
	 */
	@Value("${paging.rowPerPage}")
	private String pagingRowPerPage;

}
