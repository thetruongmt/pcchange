package com.mediatrust.pcchange.constant;

public class SystemConstant {

	/*********日付フォーマット*********/
	public static final String DATE_YYYYMMDDHHMMSS_FORMAT="yyyy/MM/dd HH:mm:ss";
	public static final String DATE_YYYYMMDD_FORMAT="yyyy/MM/dd";
	public static final String DATE_YYYYMM_FORMAT="yyyy/MM";
	public static final String DATE_MMDD_DAY_FORMAT="MM月dd日(E)";
	public static final String HHMM_FORMAT="HH:mm";

	/**
	 * ページID
	 * @author the.truong
	 *
	 */
	public class PageID{
		public static final String LOGIN_PAGE="PCC000";
		public static final String PROJECT_CREATE="PCC001";
		public static final String SCHEDULE_REGISTER="PCC002";
		public static final String SAGYOU_REGISTER="PCC003";
		public static final String SHINCHOKU_REGISTER="PCC004";
		public static final String SHIYOUSHA_REGISTER="PCC005";
		public static final String SHINPC_REGISTER="PCC006";
		public static final String KYUUPC_REGISTER="PCC007";
		public static final String SAGYOUIN_REGISTER="PCC009";

	}

}
