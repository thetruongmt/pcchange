package com.mediatrust.pcchange.entity;

import lombok.Data;

/**
 *　作業ステータスマスタ
 * @author the.truong
 *
 */
@Data
public class SagyouStatusEntity {
	/**
	 * 作業ステータス番号
	 */
	private Integer sagyouStatusNo;
	/**
	 * ステータス
	 */
	private String status;
}
