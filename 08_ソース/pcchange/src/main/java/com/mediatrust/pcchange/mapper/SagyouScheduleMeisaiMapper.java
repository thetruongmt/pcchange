package com.mediatrust.pcchange.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.SagyouScheduleEntity;
import com.mediatrust.pcchange.entity.SagyouScheduleMeisaiEntity;

@Mapper
public interface SagyouScheduleMeisaiMapper {

	public List<SagyouScheduleMeisaiEntity> findSagyouScheduleMeisai(@Param("pPjNo") String projectNo, @Param("pDate") Date sagyouDate );
	public void deleteSagyouSheduleMeisai(@Param("pPjNo") String projectNo, @Param("pDate") Date sagyouDate);
	public void insertSagyouScheduleMeisai(@Param("entity") SagyouScheduleEntity entity);

}
