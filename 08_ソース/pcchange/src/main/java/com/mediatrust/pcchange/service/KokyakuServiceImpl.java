package com.mediatrust.pcchange.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.entity.KokyakuEntity;
import com.mediatrust.pcchange.mapper.KokyakuMapper;

@Service("KokyakuSrv")
public class KokyakuServiceImpl implements KokyakuService {

	@Autowired
	private KokyakuMapper _mapper;

	public List<KokyakuEntity> findAll() {
		return _mapper.findAll();
	}
}
