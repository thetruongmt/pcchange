package com.mediatrust.pcchange.entity;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

/**
 * スケジュールエンティティ
 * @author the.truong
 *
 */
@Data
public class SagyouScheduleEntity {

	/**
	 * 作業プロジェクト番号
	 */
	@NotBlank(message = "{"+MessageMap.PCC002_SAGYO_PROJECT_NAME_REQUIRED+"}")
	private String sagyouProjectNo;

	/**
	 * 作業年月
	 */
	@NotNull(message="{"+MessageMap.PCC002_SAGYO_MONTH_REQUIRED+"}")
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMM_FORMAT)
	private Date sagyouMonth;
	/**
	 * スケジュール詳細一覧
	 */
	private List<SagyouScheduleMeisaiEntity> scheduleMeisai;
	private Integer scheduleSumiCount;
	/**
	 * 更新日
	 */
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT)
	private Date updDate;
}
