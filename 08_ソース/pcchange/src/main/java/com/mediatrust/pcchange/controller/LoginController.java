package com.mediatrust.pcchange.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.SagyouinEntity;

@Controller
public class LoginController {

	@Autowired
	MessageSource messageSource;

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.LOGIN_PAGE;
	}

	/**
	 * ページの初期表示
	 * @return
	 */
	private String init(ModelMap model){
		SagyouinEntity sagyoin = new SagyouinEntity();
		model.addAttribute("model", sagyoin);
		return Views.LOGIN;
	}

	/**
	 * ログインページの表示アクション
	 * @return
	 */
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(ModelMap model){

		logger.info("index開始 : 【パラメータ無し】");
		String result =  init(model);
		logger.info("index終了");

		return result;
	}

	/**
	 * ログインページの表示アクション
	 * @return
	 */
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String index1(ModelMap model){
		logger.info("login開始 : 【パラメータ無し】");
		String result =  init(model);
		logger.info("login終了");
		return result;
	}

	/**
	 * ログインする時にエラー発生する場合のアクション
	 * @param loginId
	 * @param msgCd
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/login/error", method=RequestMethod.GET)
	public String error(@RequestParam("sagyoinNo") String sagyoinNo, @RequestParam("msgCd") String msgCd, ModelMap model){
		logger.info(String.format("error開始 : sagyoinNo=%s, msgCd=%s",sagyoinNo ,msgCd));

		SagyouinEntity sagyoin = new SagyouinEntity();
		try{

			sagyoin.setSagyouinNo(Integer.parseInt(sagyoinNo));
		}catch(Exception e){
			sagyoin.setSagyouinNo(null);
		}


		String error = messageSource.getMessage(msgCd, null,null);
		model.addAttribute("model", sagyoin);
		model.addAttribute("error", error);

		logger.info("error終了");
		return Views.LOGIN;

	}

}