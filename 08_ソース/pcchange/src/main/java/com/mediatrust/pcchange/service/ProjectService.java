package com.mediatrust.pcchange.service;

import java.util.List;

import com.mediatrust.pcchange.entity.ProjectEntity;

public interface ProjectService {
	public List<ProjectEntity> getAllProject();
	public ProjectEntity getProjectByNo(String projectNo);
	public void addProject(ProjectEntity p);
}
