package com.mediatrust.pcchange.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.KokyakuBumonEntity;
import com.mediatrust.pcchange.entity.KokyakuEntity;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.entity.ShiyoushaTorokuEntity;
import com.mediatrust.pcchange.service.KokyakuBumonService;
import com.mediatrust.pcchange.service.KokyakuService;
import com.mediatrust.pcchange.service.KokyakuShiyoushaService;

@Controller
@RequestMapping("/shiyousha")
public class ShiyoushaController {

	private static final Logger logger = LoggerFactory.getLogger(ShiyoushaController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	private KokyakuService _kokyakuSrv;
	@Autowired
	private KokyakuBumonService _bumonSrv;
	@Autowired
	private KokyakuShiyoushaService _kokyakuShiyoushaSrv;

	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SHIYOUSHA_REGISTER;
	}
	/**
	 * スケジュールページの表示アクション
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model) {

		logger.info("index開始 : 【パラメータ無し】");

		ShiyoushaTorokuEntity entity = new ShiyoushaTorokuEntity();

		List<KokyakuEntity> kokyakuList = _kokyakuSrv.findAll();
		List<KokyakuBumonEntity> bumonList = null;
		if(!kokyakuList.isEmpty()){
			Integer kokyakuNo = kokyakuList.get(0).getKokyakuNo();
			bumonList =_bumonSrv.findAllByKokyakyId(kokyakuNo);
			if(!bumonList.isEmpty()){
				entity.setKokyakuBumonNo(bumonList.get(0).getKokyakuBumonNo());
				entity.setBukaName(bumonList.get(0).getBukaName());
			}
			entity.setKokyakuNo(kokyakuNo);
		}

		model.addAttribute("model", entity);
		model.addAttribute("kokyakus", kokyakuList);
		model.addAttribute("bumons", bumonList);
		logger.info("index終了");

		return Views.SHIYOUSHA;
	}

	@RequestMapping(value="/kokyaku", produces = MediaType.APPLICATION_JSON_VALUE , method=RequestMethod.GET )
	public @ResponseBody List<KokyakuBumonEntity> kokyakuChange(@RequestParam("id") Integer id){
		return _bumonSrv.findAllByKokyakyId(id);
	}

	/**
	 * 進捗データ検索ボタンのアクション
	 *
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="search", method = RequestMethod.POST)
	public String search(@Valid ShiyoushaTorokuEntity entity, BindingResult result, ModelMap model) {

		logger.info(String.format("search開始 : 【%s】", entity.toString()));
		if(result.hasErrors()){
			List<ObjectError> errorOrgs = result.getAllErrors();

			java.util.Map<String, String> errorDetails=errorOrgs.stream()
						.map(p->(FieldError) p)
						.collect(Collectors.toMap(FieldError::getField	, FieldError::getDefaultMessage));

			Set<String> existing = new HashSet<String>();

			java.util.Map<String, String> errors = errorDetails.entrySet().stream()
					.filter(p -> existing.add(p.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			model.addAttribute("errors", errors);
			model.addAttribute("errorDetails",errorDetails );
			entity.setShiyoushas(null);
		}else{
			List<KokyakuShiyoushaEntity> lst = _kokyakuShiyoushaSrv.findKokyakuShiyoushaByKokyakuAndBumon(entity.getKokyakuNo(), entity.getKokyakuBumonNo());
			entity.setShiyoushas(lst);
		}

		List<KokyakuEntity> kokyakuList = _kokyakuSrv.findAll();
		List<KokyakuBumonEntity> bumonList = null;

		Integer selectKokyaku = entity.getKokyakuNo();
		if(selectKokyaku == null && !kokyakuList.isEmpty()){
			selectKokyaku = kokyakuList.get(0).getKokyakuNo();
		}

		if(selectKokyaku != null){
			bumonList =_bumonSrv.findAllByKokyakyId(selectKokyaku);
		}

		model.addAttribute("model", entity);
		model.addAttribute("kokyakus", kokyakuList);
		model.addAttribute("bumons", bumonList);
		model.addAttribute("searched", true);
		logger.info("search終了");

		return Views.SHIYOUSHA;
	}

	@RequestMapping(value="/delete", produces = MediaType.APPLICATION_JSON_VALUE , method=RequestMethod.POST )
	public @ResponseBody HashMap<String, String> deleteRow(KokyakuShiyoushaEntity entity, ModelMap model){
		logger.info(String.format("deleteRow開始 : 【%s】", entity.toString()));

		HashMap<String, String> retVal = new HashMap<String, String>();
		retVal.put("status", "1");
		retVal.put("message", "");
		HashMap<String, String> deleteRet = _kokyakuShiyoushaSrv.deleteKokyakuShiyousha(entity);
		if(deleteRet.containsKey("_totalCheck")){
			retVal.replace("status", "0");
			retVal.replace("message", deleteRet.get("_totalCheck"));
		}else{
			retVal.replace("message", messageSource.getMessage(MessageMap.PCC005_DELETE_COMPLETED, null, null));
		}

		logger.info("deleteRow終了");
		return retVal;

	}

	/**
	 * 登録ボタンのアクション
	 * @param entity
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(params="update",method = RequestMethod.POST)
	public String update(@Valid ShiyoushaTorokuEntity entity, BindingResult result, ModelMap model){

		logger.debug(String.format("************　update開始【データ：%s】　************", entity.toString()));
		if(result.hasErrors()){
			List<ObjectError> errorOrgs = result.getAllErrors();

			java.util.Map<String, String> errorDetails=errorOrgs.stream()
						.map(p->(FieldError) p)
						.collect(Collectors.toMap(FieldError::getField	, FieldError::getDefaultMessage));

			Set<String> existing = new HashSet<String>();

			java.util.Map<String, String> errors = errorDetails.entrySet().stream()
					.filter(p -> existing.add(p.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			model.addAttribute("errors", errors);
			model.addAttribute("errorDetails",errorDetails );
		}else{
			List<KokyakuShiyoushaEntity> updateList = new ArrayList<>();
			HashMap<String, String> errorDetails = _kokyakuShiyoushaSrv.checkForUpdate(entity.getKokyakuNo(), entity.getKokyakuBumonNo() , entity.getShiyoushas(), updateList);
			if(!errorDetails.isEmpty()){

				HashMap<String, String> errors = new HashMap<String, String>();
				errors.put("ShiyoushaTorokuEntity", messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null, null));

				model.addAttribute("errors", errors );
				model.addAttribute("errorDetails", errorDetails);
			}else{
				_kokyakuShiyoushaSrv.updateShiyoushaInfo(updateList);
				List<KokyakuShiyoushaEntity> lst = _kokyakuShiyoushaSrv.findKokyakuShiyoushaByKokyakuAndBumon(entity.getKokyakuNo(), entity.getKokyakuBumonNo());
				entity.setShiyoushas(lst);
				model.addAttribute("success", messageSource.getMessage(MessageMap.PCC005_UPDATE_COMPLETED, null, null));
			}
		}

		List<KokyakuEntity> kokyakuList = _kokyakuSrv.findAll();
		List<KokyakuBumonEntity> bumonList = null;

		Integer selectKokyaku = entity.getKokyakuNo();
		if(selectKokyaku == null && !kokyakuList.isEmpty()){
			selectKokyaku = kokyakuList.get(0).getKokyakuNo();
		}

		if(selectKokyaku != null){
			bumonList =_bumonSrv.findAllByKokyakyId(selectKokyaku);
		}

		model.addAttribute("model", entity);
		model.addAttribute("kokyakus", kokyakuList);
		model.addAttribute("bumons", bumonList);
		model.addAttribute("searched", true);

		logger.info("update終了");
		return Views.SHIYOUSHA;

	}

}