package com.mediatrust.pcchange.service;

import java.util.HashMap;
import java.util.List;

import com.mediatrust.pcchange.entity.HikitoriPCEntity;
import com.mediatrust.pcchange.entity.KyuuPCTorokuEntity;


public interface HikitoriPCService {
	HikitoriPCEntity findHikitoriPCInProject(Integer pProjectNo, Integer pHikotoriPCNo);
	List<HikitoriPCEntity> findAllHikitoriPCInProject(Integer pProjectNo, Integer pOffset, Integer pRowCnt);
	KyuuPCTorokuEntity getKyuuPCForTouroku(KyuuPCTorokuEntity entity, Integer rowPage);
	HashMap<String, String> deleteSagyouHikiatePC(HikitoriPCEntity entity);
	public HashMap<String, String> checkForUpdate(Integer pProjectNo, List<HikitoriPCEntity> lstEnt, List<HikitoriPCEntity> refUpdateList);
	public void updateListHikiatePC(List<HikitoriPCEntity> pLstEnt);
}
