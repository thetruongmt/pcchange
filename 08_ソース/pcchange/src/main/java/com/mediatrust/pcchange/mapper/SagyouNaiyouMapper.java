package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;

@Mapper
public interface SagyouNaiyouMapper {
    public List<SagyouNaiyouEntity> findAllCustomerWithProject();

    public SagyouNaiyouEntity findFirst(@Param("sagyouProjectNo") Integer sagyouProjectNo, @Param("kokyakuNo") Integer kokyakuNo, @Param("kokyakuBumonNo") Integer kokyakuBumonNo);

    public SagyouNaiyouEntity findSagyouNaiyouInProject(@Param("pProjectNo") Integer pProjectNo, @Param("pSagyouNaiyouNo") Integer pSagyouNaiyouNo);

    public void addSagyouNaiyou(@Param("sagyouNaiyou") SagyouNaiyouEntity sagyouNaiyou);

    public void updateSagyouNaiyou(@Param("sagyouNaiyou") SagyouNaiyouEntity sagyouNaiyou);

    public List<SagyouNaiyouEntity> findAllByProject(@Param("pProjectNo") Integer pProjectNo);
}