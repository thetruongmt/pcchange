package com.mediatrust.pcchange.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.mediatrust.pcchange.security.PCChangeAuthenticationFailureHandler;
import com.mediatrust.pcchange.security.PCChangeAuthenticationSuccessHandler;
import com.mediatrust.pcchange.security.SessionExpiredDetectingLoginUrlAuthenticationEP;
import com.mediatrust.pcchange.service.SagyouinServiceImpl;
/**
 * Spring Security設定クラス.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	PCChangeAuthenticationFailureHandler authenticationFailure;

	@Autowired
	PCChangeAuthenticationSuccessHandler authenticationSuccess;

    @Override
    public void configure(WebSecurity web) throws Exception {
        // 静的リソース(images、css、javascript)に対するアクセスはセキュリティ設定を無視する
    	web.ignoring().antMatchers(
    						"/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 認可の設定
        http
        .authorizeRequests()
        .antMatchers("/", "/login", "/login/error").permitAll()
    	.anyRequest().authenticated()
    	.and()
        .formLogin()
        	// ログインフォームのパス
        	.loginPage("/")
       	   // 認証処理のパス
        	.loginProcessingUrl("/login")
        	// 認証成功時の遷移先
        	.defaultSuccessUrl("/project/create")
        	// ユーザー名、パスワードのパラメータ名
        	.usernameParameter("sagyouinNo").passwordParameter("password")
        	// 認証失敗時に呼ばれるハンドラクラス
        	.failureHandler(authenticationFailure)
        	.successHandler(authenticationSuccess)
        	.and()
        // ログアウト設定
        .logout()
        	// ログアウト処理のパス
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout**"))
            // ログアウト完了時のパス
            .logoutSuccessUrl("/")
            .and()
        .exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint());

        http.csrf().disable();
    }

    @Bean
	AuthenticationEntryPoint authenticationEntryPoint() {
		return new SessionExpiredDetectingLoginUrlAuthenticationEP("/");
	}

    @Configuration
    protected static class AuthenticationConfiguration
    extends GlobalAuthenticationConfigurerAdapter {

    	@Autowired
        SagyouinServiceImpl sagyoinService;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            // 認証するユーザーを設定する
            auth.userDetailsService(sagyoinService).passwordEncoder(NoOpPasswordEncoder.getInstance());

        }
    }
}
