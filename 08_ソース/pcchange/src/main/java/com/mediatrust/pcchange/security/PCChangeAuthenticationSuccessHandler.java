package com.mediatrust.pcchange.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.mediatrust.pcchange.constant.SessionKeyConstant;
import com.mediatrust.pcchange.entity.LoginSagyouin;
import com.mediatrust.pcchange.entity.SagyouinEntity;


@Component
public class PCChangeAuthenticationSuccessHandler  implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication auth)
			throws IOException, ServletException {

		HttpSession session = req.getSession();
		SagyouinEntity user = ((LoginSagyouin)auth.getPrincipal()).getUserEntity();
		session.setAttribute(SessionKeyConstant.LOGIN_SAGYOIN, user);
		String contextPath = req.getContextPath();
		resp.sendRedirect(contextPath+"/schedule");

	}

}
