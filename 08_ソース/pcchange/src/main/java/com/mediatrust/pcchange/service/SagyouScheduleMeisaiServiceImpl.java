package com.mediatrust.pcchange.service;

import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.entity.SagyouScheduleEntity;
import com.mediatrust.pcchange.entity.SagyouScheduleMeisaiEntity;
import com.mediatrust.pcchange.mapper.SagyouScheduleMeisaiMapper;
import com.mediatrust.pcchange.utils.CommonFunc;

@Service("sagyouScheduleMeisaiSrv")
public class SagyouScheduleMeisaiServiceImpl implements SagyouScheduleMeisaiSerivce {

	private static final Logger logger = LoggerFactory.getLogger(SagyouScheduleMeisaiServiceImpl.class);
	@Autowired
	MessageSource _messageSource;

	@Autowired
	private SagyouScheduleMeisaiMapper _mapper;

	@Autowired
	private KokyakuShiyoushaService _kokyakuShiyoshaSrv;

	/**
	 * プロジェクトを元にスケジュール情報を取得する
	 */
	@Override
	public SagyouScheduleEntity getSagyouScheduleByProject(String projectNo, Date sagyouDate) {

		SagyouScheduleEntity entity = new SagyouScheduleEntity();

		entity.setSagyouProjectNo(projectNo);
		entity.setSagyouMonth(sagyouDate);
		logger.info("データベースアクセス【sagyou_schedule_meisai_master】：作業スケジュール詳細データを取得");
		List<SagyouScheduleMeisaiEntity> dbMeisai = _mapper.findSagyouScheduleMeisai(projectNo, sagyouDate);

		Calendar calendarStart = Calendar.getInstance();
		calendarStart.setTime(sagyouDate);
		calendarStart.set(Calendar.DATE, 1);

		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.setTime(calendarStart.getTime());
		calendarEnd.add(Calendar.MONTH, 1);
		calendarEnd.add(Calendar.DATE,-1);

		List<SagyouScheduleMeisaiEntity> meisai = new ArrayList<SagyouScheduleMeisaiEntity>();
		int scheduleSumiCount = 0;
		Date dbDate = null;
		while(!calendarStart.after(calendarEnd)){

			final Date curDate = calendarStart.getTime();

			Optional<SagyouScheduleMeisaiEntity> findObj = dbMeisai.stream()
					.filter(item -> CommonFunc.compareDateWithoutTime(item.getSagyouDate(), curDate) == 0)
					.findFirst();

			SagyouScheduleMeisaiEntity item = new SagyouScheduleMeisaiEntity();
			if(!findObj.isPresent()){
				item = new SagyouScheduleMeisaiEntity();
				item.setSagyouProjectNo(projectNo);
				item.setSagyouDate(curDate);
			}else{
				item = findObj.get();
				scheduleSumiCount+=item.getScheduleSumiCount();
				dbDate = item.getUpdDate();
			}

			meisai.add(item);
			calendarStart.add(Calendar.DATE, 1);
		}
		entity.setUpdDate(dbDate);
		entity.setScheduleSumiCount(scheduleSumiCount);
		entity.setScheduleMeisai(meisai);

		return entity;
	}

	/**
	 * 入力データをエラーチェックする
	 */
	public HashMap<String, String> updateValidate(SagyouScheduleEntity entity){

		HashMap<String, String> retValue = new HashMap<String, String>();

		String itemKey = "";
		int itemIdx = 0;
		List<SagyouScheduleMeisaiEntity> lst = entity.getScheduleMeisai();
		String messageTemplate = _messageSource.getMessage(MessageMap.PCC002_SAGYO_KOKYAKU_SHIYOUSHA_EXISTS, null,null);
		for (SagyouScheduleMeisaiEntity item : lst) {
			if(item.getScheduleSumiCount() == 0){
				continue;
			}
			itemIdx = lst.indexOf(item);
			Integer sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo1();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo1", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo2();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo2", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo2();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo2", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名３", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo3();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo3", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名４", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo4();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo4", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名５", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo5();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo5", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名６", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo6();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo6", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名７", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo7();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo7", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名８", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo8();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo8", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名９", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo9();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo9", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１０", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo11();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo11", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１１", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo12();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo12", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１２", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo13();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo13", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１３", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo14();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo14", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１４", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo15();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo15", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１５", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo16();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo16", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１６", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo17();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo17", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１７", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo18();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo18", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１８", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo19();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo19", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名１９", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo20();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo20", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２０", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo21();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo21", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２１", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo22();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo22", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２２", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo23();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo23", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２３", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo24();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo24", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２４", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo25();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo25", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２５", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo26();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo26", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２６", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo27();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo27", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２７", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo28();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo28", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２８", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo29();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo29", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名２９", "顧客使用者マスタ"));
				}
			}
			sagyouKokyakuShiyoshaNo = item.getSagyouKokyakuShiyoshaNo30();
			if(sagyouKokyakuShiyoshaNo != null){
				KokyakuShiyoushaEntity kokyaku = _kokyakuShiyoshaSrv.findKokyakuShiyoushaByNo(sagyouKokyakuShiyoshaNo);
				if(kokyaku == null){
					itemKey = String.format("Row_%d_sagyouKokyakuShiyoshaNo30", itemIdx);
					retValue.put(itemKey, String.format(messageTemplate, "作業対象顧客使用者名３０", "顧客使用者マスタ"));
				}
			}
		}

		//整合性チェック
		List<SagyouScheduleMeisaiEntity> dbList = _mapper.findSagyouScheduleMeisai(entity.getSagyouProjectNo(), entity.getSagyouMonth());
		Date dbDate = null;
		if(dbList.size() > 0){
			dbDate = dbList.get(0).getUpdDate();
		}
		if((dbDate == null && entity.getUpdDate() != null ) ||
				(dbDate != null && entity.getUpdDate() == null) ||
				(dbDate != null && dbDate.compareTo(entity.getUpdDate()) != 0)){
			retValue.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null) );
		}
		return retValue;

	}

	/**
	 * データベースに更新する
	 */
	@Transactional
	public void update(SagyouScheduleEntity entity){
		List<SagyouScheduleMeisaiEntity> data = entity.getScheduleMeisai();
		for (int i = data.size() -1; i >= 0 ; i-- ) {
			SagyouScheduleMeisaiEntity item = data.get(i);
			if(item.getScheduleSumiCount() == 0){
				data.remove(item);
			}
		}

		//データベースのデータを削除する
		_mapper.deleteSagyouSheduleMeisai(entity.getSagyouProjectNo(), entity.getSagyouMonth());
		if(data.size() > 0){
			_mapper.insertSagyouScheduleMeisai(entity);
		}
	}

}
