package com.mediatrust.pcchange.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.entity.SagyouScheduleEntity;
import com.mediatrust.pcchange.entity.SagyouScheduleMeisaiEntity;
import com.mediatrust.pcchange.service.ProjectService;
import com.mediatrust.pcchange.service.SagyouScheduleMeisaiSerivce;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {

	private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	private SagyouScheduleMeisaiSerivce _sagyoSchSrv;
	@Autowired
	private ProjectService _pjSrv;

	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SCHEDULE_REGISTER;
	}

	@ModelAttribute("projects")
	public List<ProjectEntity> getProjects() {
		return _pjSrv.getAllProject();
	}

	/**
	 * スケジュールページの表示アクション
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model) {

		logger.info("index開始 : 【パラメータ無し】");

		// 画面を初期化
		Date now = new Date();
		SagyouScheduleEntity entity = new SagyouScheduleEntity();
		entity.setSagyouMonth(now);
		entity.setScheduleMeisai(new ArrayList<SagyouScheduleMeisaiEntity>());

		// 画面にデータを渡る
		model.addAttribute("model", entity);

		logger.info("index終了");

		return Views.SCHEDULE;
	}

	/**
	 * スケジュール検索ボタンのアクション
	 *
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="search", method = RequestMethod.POST)
	public String search(@Valid SagyouScheduleEntity entity, BindingResult result, ModelMap model) {

		logger.info(String.format("search開始 : 【%s】", entity.toString()));

		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
		}

		// データを取得
		ProjectEntity pjEnt = null;
		if(Strings.isNotEmpty(entity.getSagyouProjectNo())){
			pjEnt = _pjSrv.getProjectByNo(entity.getSagyouProjectNo());
		}

		if (!result.hasErrors() && pjEnt != null) {
			entity = _sagyoSchSrv.getSagyouScheduleByProject(entity.getSagyouProjectNo(), entity.getSagyouMonth());
		}
		model.addAttribute("project", pjEnt);
		model.addAttribute("model", entity);

		logger.info("search終了");

		return Views.SCHEDULE;
	}

	@RequestMapping(params="update",method = RequestMethod.POST)
	public String update(@Valid SagyouScheduleEntity entity, BindingResult result, ModelMap model){

		logger.debug(String.format("************　update開始【データ：%s】　************", entity.toString()));

		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
		}else{
			HashMap<String, String> errorDetails = _sagyoSchSrv.updateValidate(entity);

			if(errorDetails.size() > 0){
				List<ObjectError> errors = new ArrayList<ObjectError>();
				if(errorDetails.containsKey("_totalCheck")){
					ObjectError error = new ObjectError("SagyoScheduleEntity", errorDetails.get("_totalCheck"));
					errors.add(error);
					errorDetails.remove("_totalCheck");
				}
				if(!errorDetails.isEmpty()){
					ObjectError error = new ObjectError("SagyoScheduleEntity.scheduleMeisai",
							String.format(messageSource.getMessage(MessageMap.PCC000_COMMON_LIST_INPUT_ERROR, null, null),"スケジュールの設定"));


					errors.add(error);

				}
				model.addAttribute("errors", errors);
				model.addAttribute("errorDetails",errorDetails);

			}else{
				//更新する
				_sagyoSchSrv.update(entity);
				entity = _sagyoSchSrv.getSagyouScheduleByProject(entity.getSagyouProjectNo(), entity.getSagyouMonth());
			}

		}

		// データを取得
		ProjectEntity pjEnt = _pjSrv.getProjectByNo(entity.getSagyouProjectNo());
		model.addAttribute("model", entity);
		model.addAttribute("project", pjEnt);

		logger.info("update終了");
		return Views.SCHEDULE;

	}

}