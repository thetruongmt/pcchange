package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;

@Mapper
public interface KokyakuShiyoushaMapper {
	public List<KokyakuShiyoushaEntity> findAll();
	public KokyakuShiyoushaEntity findKokyakuShiyoushaByNo(@Param("pNo") Integer pNo);
	public List<KokyakuShiyoushaEntity> findAllKokyakuShiyoushaByProject(@Param("pPrjNo") Integer pProjectNo);
	public List<KokyakuShiyoushaEntity> findKokyakuShiyoushaByKokyakuAndBumon(@Param("pKokyakuNo") Integer pKokyaku, @Param("pBumonNo") Integer pBumon);
	public KokyakuShiyoushaEntity findKokyakuShiyoushaInProject(@Param("pProjectNo") Integer pProjectNo, @Param("pKokyakuShiyouShano") Integer pKokyakuShiyouShaNo);
	public void deleteKokyakuShiyoushaByNo(@Param("pNo") Integer pNo);
	public void updateListShiyoushaInfo(@Param("lstEnt") List<KokyakuShiyoushaEntity> lstEnt);
}