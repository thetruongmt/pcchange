package com.mediatrust.pcchange.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mediatrust.pcchange.configuration.AppSetting;
import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.entity.SagyouHikiatePCKobetsuEntity;
import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;
import com.mediatrust.pcchange.entity.SagyouStatusEntity;
import com.mediatrust.pcchange.entity.SagyouinEntity;
import com.mediatrust.pcchange.entity.ShinPCTorokuEntity;
import com.mediatrust.pcchange.service.ProjectService;
import com.mediatrust.pcchange.service.SagyouHikiatePCKobetsuService;
import com.mediatrust.pcchange.service.SagyouNaiyouService;
import com.mediatrust.pcchange.service.SagyouStatusService;
import com.mediatrust.pcchange.service.SagyouinService;

@Controller
@RequestMapping("/shinpc")
public class ShinPCController {

	private static final Logger logger = LoggerFactory.getLogger(ShinPCController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	AppSetting _appSetting;
	@Autowired
	ProjectService _prjSrv;
	@Autowired
	SagyouinService _sagyouinSrv;
	@Autowired
	SagyouNaiyouService _sagyouNaiyouSrv;
	@Autowired
	SagyouStatusService _sagyouStatusSrv;
	@Autowired
	private SagyouHikiatePCKobetsuService _sagyouHikiatePCSrv;

	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SHINPC_REGISTER;
	}

	@ModelAttribute("projects")
	public List<ProjectEntity> getProjectList(){
		return _prjSrv.getAllProject();
	}

	@ModelAttribute("sagyouins")
	public List<SagyouinEntity> getSagyouinList(){
		return _sagyouinSrv.findAll();
	}

	@ModelAttribute("statuses")
	public List<SagyouStatusEntity> getListStatus(){
		return _sagyouStatusSrv.findAll();
	}

	/**
	 * スケジュールページの表示アクション
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model) {

		logger.info("index開始 : 【パラメータ無し】");

		ShinPCTorokuEntity entity = new ShinPCTorokuEntity();

		model.addAttribute("model", entity);
		logger.info("index終了");

		return Views.SHINPC_TOROKU;
	}

	/**
	 * 進捗データ検索ボタンのアクション
	 *
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="search", method = RequestMethod.POST)
	public String search(@Valid ShinPCTorokuEntity entity, BindingResult result, ModelMap model) {

		logger.info(String.format("search開始 : 【%s】", entity.toString()));

		if(result.hasErrors()){
			List<ObjectError> errorOrgs = result.getAllErrors();

			java.util.Map<String, String> errorDetails=errorOrgs.stream()
						.map(p->(FieldError) p)
						.collect(Collectors.toMap(FieldError::getField	, FieldError::getDefaultMessage));

			Set<String> existing = new HashSet<String>();

			java.util.Map<String, String> errors = errorDetails.entrySet().stream()
					.filter(p -> existing.add(p.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			model.addAttribute("errors", errors);
			model.addAttribute("errorDetails",errorDetails );
		}else{
			Integer rowPerPage =Integer.valueOf(_appSetting.getPagingRowPerPage());
			entity = _sagyouHikiatePCSrv.getShinPCForToroku(entity, rowPerPage);
		}

		if(entity.getSagyouProjectNo() == null){
			List<SagyouNaiyouEntity> sagyouNaiyous = _sagyouNaiyouSrv.findAllByProject(entity.getSagyouProjectNo());
			model.addAttribute("sagyouNaiyous", sagyouNaiyous);
		}


		model.addAttribute("model", entity);
		model.addAttribute("searched", true);
		logger.info("search終了");

		return Views.SHINPC_TOROKU;
	}

	@RequestMapping(value="/delete", produces = MediaType.APPLICATION_JSON_VALUE , method=RequestMethod.POST )
	public @ResponseBody HashMap<String, String> deleteRow(SagyouHikiatePCKobetsuEntity entity, ModelMap model){
		logger.info(String.format("deleteRow開始 : 【%s】", entity.toString()));

		HashMap<String, String> retVal = new HashMap<String, String>();
		retVal.put("status", "1");
		retVal.put("message", "");
		HashMap<String, String> deleteRet = _sagyouHikiatePCSrv.deleteSagyouHikiatePCKobetsu(entity);
		if(deleteRet.containsKey("_totalCheck")){
			retVal.replace("status", "0");
			retVal.replace("message", deleteRet.get("_totalCheck"));
		}else{
			retVal.replace("message", messageSource.getMessage(MessageMap.PCC006_DELETE_COMPLETED, null, null));
		}

		logger.info("deleteRow終了");
		return retVal;

	}

	/**
	 * 登録ボタンのアクション
	 * @param entity
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(params="update",method = RequestMethod.POST)
	public String update(@Valid ShinPCTorokuEntity entity, BindingResult result, ModelMap model){

		logger.debug(String.format("************　update開始【データ：%s】　************", entity.toString()));
		if(result.hasErrors()){
			List<ObjectError> errorOrgs = result.getAllErrors();

			java.util.Map<String, String> errorDetails=errorOrgs.stream()
						.map(p->(FieldError) p)
						.collect(Collectors.toMap(FieldError::getField	, FieldError::getDefaultMessage));

			Set<String> existing = new HashSet<String>();

			java.util.Map<String, String> errors = errorDetails.entrySet().stream()
					.filter(p -> existing.add(p.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			model.addAttribute("errors", errors);
			model.addAttribute("errorDetails",errorDetails );
		}else{
			List<SagyouHikiatePCKobetsuEntity> updateList = new ArrayList<>();
			HashMap<String, String> errorDetails = _sagyouHikiatePCSrv.checkForUpdate(entity.getSagyouProjectNo(), entity.getDetail(), updateList);
			if(!errorDetails.isEmpty()){

				Set<String> existing = new HashSet<String>();

				java.util.Map<String, String> errors = errorDetails.entrySet().stream()
						.filter(p -> existing.add(p.getValue()))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

				model.addAttribute("errors", errors );
				model.addAttribute("errorDetails", errorDetails);
			}else{
				_sagyouHikiatePCSrv.updateListHikiatePCKobetsu(updateList);
				Integer rowPerPage =Integer.valueOf(_appSetting.getPagingRowPerPage());
				entity = _sagyouHikiatePCSrv.getShinPCForToroku(entity, rowPerPage);
				model.addAttribute("success", messageSource.getMessage(MessageMap.PCC006_UPDATE_COMPLETED, null, null));
			}
		}

		if(entity.getSagyouProjectNo() == null){
			List<SagyouNaiyouEntity> sagyouNaiyous = _sagyouNaiyouSrv.findAllByProject(entity.getSagyouProjectNo());
			model.addAttribute("sagyouNaiyous", sagyouNaiyous);
		}

		model.addAttribute("model", entity);
		model.addAttribute("searched", true);

		logger.info("update終了");
		return Views.SHINPC_TOROKU;

	}

}