package com.mediatrust.pcchange.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.entity.LoginSagyouin;
import com.mediatrust.pcchange.entity.SagyouinEntity;
import com.mediatrust.pcchange.mapper.SagyouinMapper;

@Service("sagyouinSrv")
public class SagyouinServiceImpl implements UserDetailsService, SagyouinService{

	private static final Logger logger = LoggerFactory.getLogger(SagyouinServiceImpl.class);

	@Autowired
	MessageSource _messageSource;
	@Autowired
	private SagyouinMapper _mapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		if(StringUtils.isEmpty(username)){
			throw new UsernameNotFoundException("User not found for login id: " + username);
		}

		List<String> roleList = new ArrayList<String>();
		SagyouinEntity user = null;

		try{
			user = _mapper.findSagyouinById(username);
		} catch(Exception e){
			logger.error("例外【SagyouinServiceImpl.loadUserByUsername】：%s", e.getMessage());
			throw e;
		}

		if(user == null){
			throw new UsernameNotFoundException("User not found for login id: " + username);
		}

		return new LoginSagyouin(user, roleList.toArray(new String[]{}));
	}

	/**
	 * 作業員一覧を取得する
	 */
	@Override
	public List<SagyouinEntity> findAll(){
		logger.info("データベースアクセス【sagyouin_master】：作業員一覧を取得");
		return _mapper.findAll();
	}

	public SagyouinEntity findSagyouinById(Integer pId){
		logger.info("データベースアクセス【sagyouin_master】：作業員情報を取得");
		return _mapper.findSagyouinById(String.valueOf(pId));
	}

	public HashMap<String, String> deleteSagyouin(Integer loginId, SagyouinEntity entity){
		HashMap<String, String> retVal = new HashMap<>();
		if(entity.getSagyouinNo() == loginId){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC009_DELETE_OWNER, null,null));
			return retVal;
		}

		if(!this.checkSeigosei(entity)){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
		}else{
			logger.info("データベースアクセス【sagyouin_master】：作業員マスタを削除する");
			_mapper.deleteSagyouin(entity.getSagyouinNo());
		}
		return retVal;
	}

	public HashMap<String, String> checkForUpdate(List<SagyouinEntity> lstEnt, List<SagyouinEntity> refUpdateList){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(lstEnt == null) return retVal;
		for (SagyouinEntity item : lstEnt) {
			if(item.getSagyouinNo() != null && item.getModified() != 1){
				continue;
			}

			if(item.getSagyouinNo() == null){
				refUpdateList.add(item);
			}else if(item.getModified() == 1){
				if(!checkSeigosei(item)){
					String key = String.format("Row%d_totalCheck", lstEnt.indexOf(item));
					retVal.put(key, _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
				}else{
					refUpdateList.add(item);
				}
			}
		}

		return retVal;
	}
	public void updateSagyouin(List<SagyouinEntity> lstEnt){
		if(lstEnt != null && !lstEnt.isEmpty()){
			logger.info("データベースアクセス【sagyouin_master】：作業員一覧を更新する");
			_mapper.updateSagyouin(lstEnt);
		}
	}

	/**
	 * 整合性チェック
	 * @param entity
	 * @return
	 */
	private boolean checkSeigosei(SagyouinEntity entity){

		SagyouinEntity dbEntity = _mapper.findSagyouinById(String.valueOf(entity.getSagyouinNo()));
		if(dbEntity == null) return false;

		Date dbDate = dbEntity.getUpdDate();
		Date inputDate = entity.getUpdDate();
		if((dbDate == null && inputDate != null ) ||
				(dbDate != null && inputDate == null) ||
				(dbDate != null && dbDate.compareTo(inputDate) != 0)){
			return false;
		}

		return true;
	}


}
