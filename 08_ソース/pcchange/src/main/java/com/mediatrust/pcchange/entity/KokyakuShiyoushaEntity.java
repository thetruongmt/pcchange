package com.mediatrust.pcchange.entity;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

/**
 *
 * @author khang.tran
 *
 */
@Data
public class KokyakuShiyoushaEntity {
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer kokyakuShiyoushaNo;
	private KokyakuEntity kokyaku;
	private KokyakuBumonEntity kokyakuBumon;
	@NotBlank(message = "{"+MessageMap.PCC005_SHIYOUSHA_NAME_REQUIRED+"}")
	@Size(max=128, message="{" + MessageMap.PCC005_SHIYOUSHA_NAME_LENGTH + "}")
	private String shiyoushaName;

	@Size(max=128, message="{" + MessageMap.PCC005_NAISEN_LENGTH + "}")
	@Pattern(regexp="^([0-9]*)$|^\\s*$", message="{" + MessageMap.PCC005_NAISEN_NUMBER + "}")
	private String naisenNo;
	@Size(max=128, message = "{"+MessageMap.PCC005_MAIL_ADDRESS_LENGTH+"}")
	@Pattern(regexp="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$|^\\s*$", flags=Flag.CASE_INSENSITIVE, message="{" + MessageMap.PCC005_MAIL_ADDRESS_FORMAT + "}")
	private String mailAddress;
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT)
	private Date updDate;
	private Integer modified = 0;
}
