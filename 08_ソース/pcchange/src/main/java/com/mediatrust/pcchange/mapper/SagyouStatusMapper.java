package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mediatrust.pcchange.entity.SagyouStatusEntity;

@Mapper
public interface SagyouStatusMapper {
	public List<SagyouStatusEntity> findAllStatus();
}