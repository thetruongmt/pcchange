package com.mediatrust.pcchange.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mediatrust.pcchange.constant.MessageMap;

import lombok.Data;

@Data
public class ShiyoushaTorokuEntity {

	/**
	 * 企業
	 */
	@NotNull(message="{"+MessageMap.PCC005_KIGYOU_NAME_REQUIRED+"}")
	private Integer kokyakuNo;
	/**
	 * 事業部門
	 */
	@NotNull(message="{"+MessageMap.PCC005_BUMON_REQUIRED+"}")
	private Integer kokyakuBumonNo;
	/*
	 * 部課
	 */
	@NotNull(message="{"+MessageMap.PCC005_BUKA_NAME_REQUIRED+"}")
	private String bukaName;
	/**
	 * 使用者一覧
	 */
	@Valid
	private List<KokyakuShiyoushaEntity> shiyoushas;
}
