package com.mediatrust.pcchange.entity;

import lombok.Data;

/**
 *
 * @author khang.tran
 *
 */
@Data
public class KokyakuBumonEntity {
	private Integer kokyakuBumonNo;
	private String jigyoubuName;
	private Integer kokyakuNo;
	private String bukaName;
	private String jyuusho1;
	private String jyuusho2;
}
