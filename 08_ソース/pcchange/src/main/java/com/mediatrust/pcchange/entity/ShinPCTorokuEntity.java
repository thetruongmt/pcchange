package com.mediatrust.pcchange.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mediatrust.pcchange.constant.MessageMap;

import lombok.Data;

@Data
public class ShinPCTorokuEntity {

	@NotNull(message="{"+MessageMap.PCC006_PROJECT_ID_REQUIRED+"}")
	private Integer sagyouProjectNo;

	@Valid
	private List<SagyouHikiatePCKobetsuEntity> detail;
	private Integer currentPage;
	private Integer totalPage;

}
