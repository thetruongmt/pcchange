package com.mediatrust.pcchange.configuration;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.mediatrust.pcchange.constant.SystemConstant;

import org.springframework.http.converter.*;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Autowired
    private MessageSource messageSource;

    @Bean
    public LocalValidatorFactoryBean validator()
    {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(messageSource);
        return localValidatorFactoryBean;
    }

   @Bean
   public UrlBasedViewResolver setupViewResolver() {
       UrlBasedViewResolver resolver = new UrlBasedViewResolver();
       resolver.setPrefix("/WEB-INF/views/");
       resolver.setSuffix(".jsp");
       resolver.setViewClass(JstlView.class);
       return resolver;
   }

   @Override
   public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
       Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
               .timeZone("Asia/Tokyo")
               .dateFormat(new SimpleDateFormat(SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT));
       converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
   }

}
