package com.mediatrust.pcchange.service;

import java.util.HashMap;
import java.util.List;


import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;

public interface KokyakuShiyoushaService {
	public List<KokyakuShiyoushaEntity> findAll();
	public KokyakuShiyoushaEntity findKokyakuShiyoushaByNo(Integer pNo);
	public List<KokyakuShiyoushaEntity> findAllKokyakuShiyoushaByProject(Integer pProjectNo);
	public List<KokyakuShiyoushaEntity> findKokyakuShiyoushaByKokyakuAndBumon(Integer pKokyaku, Integer pBumon);
	public KokyakuShiyoushaEntity findKokyakuShiyoushaInProject(Integer pProjectNo,Integer pKokyakuShiyouShaNo);
	public HashMap<String, String> deleteKokyakuShiyousha(KokyakuShiyoushaEntity entity);
	public HashMap<String, String> checkForUpdate(Integer pKokyakuNo, Integer pBumonNo, List<KokyakuShiyoushaEntity> lstEnt, List<KokyakuShiyoushaEntity> refUpdateList);
	public void updateShiyoushaInfo(List<KokyakuShiyoushaEntity> lstEnt);
}
