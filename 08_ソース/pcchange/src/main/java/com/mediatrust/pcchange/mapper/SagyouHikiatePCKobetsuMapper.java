package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.SagyouHikiatePCKobetsuEntity;

@Mapper
public interface SagyouHikiatePCKobetsuMapper {
	List<SagyouHikiatePCKobetsuEntity> findSagyouHikiatePCKobetsu(@Param("pProjectNo") Integer pProjectNo,
																	@Param("pStatus") Integer pStatus,
																	@Param("pHikiatePCNo") Integer pHikiatePCNo,
																	@Param("pOffset") Integer pOffset,
																	@Param("pRowCnt") Integer pCount);
	int getCountSagyouHikiatePCKobetsu(@Param("pProjectNo") Integer pProjectNo,
											@Param("pStatus") Integer pStatus,
											@Param("pHikiatePCNo") Integer pHikiatePCNo);

	SagyouHikiatePCKobetsuEntity findSagyouHikiatePCKobetsuByNo(@Param("pNo") Integer pNo);
	void updateHikiatePCKobetsuShinchoku(@Param("pEnt") SagyouHikiatePCKobetsuEntity entity);
	void deleteHikiatePCKobetsuByNo(@Param("pNo") Integer pNo);
	void updateListHikiatePCKobetsu(@Param("lstEnt") List<SagyouHikiatePCKobetsuEntity> pLstEnt);
}