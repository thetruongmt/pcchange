package com.mediatrust.pcchange.entity;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

@Data
public class ProjectEntity {

	private Integer sagyouProjectNo;

	@NotBlank(message = "{" + MessageMap.PCC001_SAGYO_PROJECT_NAME_REQUIRED + "}")
	private String sagyouProjectName;

	@NotNull(message = "{" + MessageMap.PCC001_SKOKYAKU_REQUIRED + "}")
	private String kokyakuNo;

	private String kigyouName;

	@NotNull(message =  "{" + MessageMap.PCC001_SAGYO_KIKAN_FROM_REQUIRED + "}")
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDD_FORMAT)
	private Date sagyouKikanFrom;

	@NotNull(message =  "{" + MessageMap.PCC001_SAGYO_KIKAN_TO_REQUIRED + "}")
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDD_FORMAT)
	private Date sagyouKikanTo;

}
