package com.mediatrust.pcchange.constant;

/**
 * i18nメッセージのキーマップ
 *
 * @author the
 * Message code format: "PCC" + 画面コード + index
 */
public class MessageMap {
    public static final String PCC000_LOGIN_ERROR = "PCC00001";
    public static final String PCC000_COMMON_CONFLIT_DATA_ERROR = "PCC00002";
    public static final String PCC000_COMMON_LIST_INPUT_ERROR = "PCC00003";

    // 画面：プロジェット作成
    // コード：PCC01
    public static final String PCC001_SAGYO_PROJECT_NAME_REQUIRED = "PCC01001";
    public static final String PCC001_SKOKYAKU_REQUIRED = "PCC01002";
    public static final String PCC001_SAGYO_KIKAN_FROM_REQUIRED = "PCC01003";
    public static final String PCC001_SAGYO_KIKAN_TO_REQUIRED = "PCC01004";
    public static final String PCC001_PROJECT_CREATED = "PCC01005";

    // 画面：スケジュール画面
    // コード：PCC02
    public static final String PCC002_SAGYO_PROJECT_NAME_REQUIRED = "PCC02001";
    public static final String PCC002_SAGYO_MONTH_REQUIRED = "PCC02002";
    public static final String PCC002_SAGYO_KOKYAKU_SHIYOUSHA_EXISTS = "PCC02003";

    // 画面：作業内容画面
    // コード：PCC03

    public static final String PCC003_SAGYOU_NAME_REQUIRED = "PCC03001";
    public static final String PCC003_SAGYOU_PROJECT_NO_REQUIRED = "PCC03002";
    public static final String PCC003_KOKYAKU_NO_REQUIRED = "PCC03003";
    public static final String PCC003_KOKYAKU_BUMON_NO_REQUIRED = "PCC03004";
    public static final String PCC003_SAGYOU_NAIYOU_CREATED = "PCC03005";
    public static final String PCC003_SAGYOU_NAIYOU_UPDATED = "PCC03006";

    // 画面：進捗画面
    // コード：PCC04
    public static final String PCC004_PROJECT_NAME_REQUIRED="PCC04001";
    public static final String PCC004_HIKIATE_PC_NUMBER_LENGTH="PCC04002";
    public static final String PCC004_COMPUTER_NAME_LENGTH="PCC04003";

    // 画面：使用者一覧・作成
    // コード：PCC05
    public static final String PCC005_KIGYOU_NAME_REQUIRED="PCC05001";
    public static final String PCC005_BUMON_REQUIRED="PCC05002";
    public static final String PCC005_BUKA_NAME_REQUIRED="PCC05003";
    public static final String PCC005_DELETE_COMPLETED="PCC05004";
    public static final String PCC005_SHIYOUSHA_NAME_REQUIRED = "PCC05005";
    public static final String PCC005_SHIYOUSHA_NAME_LENGTH = "PCC05006";
    public static final String PCC005_NAISEN_LENGTH = "PCC05007";
    public static final String PCC005_NAISEN_NUMBER = "PCC05008";
    public static final String PCC005_MAIL_ADDRESS_LENGTH = "PCC05009";
    public static final String PCC005_MAIL_ADDRESS_FORMAT = "PCC05010";
    public static final String PCC005_UPDATE_COMPLETED = "PCC05011";

    // 画面：新PC登録画面
    // コード：PCC06
    public static final String PCC006_PROJECT_ID_REQUIRED="PCC06001";
    public static final String PCC006_DELETE_COMPLETED="PCC06002";
    public static final String PCC006_MASTER_EXISTS = "PCC06003";
    public static final String PCC006_UPDATE_COMPLETED = "PCC06004";

    // 画面：旧PC登録画面
    // コード：PCC07
    public static final String PCC007_PROJECT_REQUIRED="PCC07001";
    public static final String PCC007_DELETE_COMPLETED="PCC07002";
    public static final String PCC007_UPDATE_COMPLETED = "PCC07003";
    public static final String PCC007_MASTER_EXISTS = "PCC07004";
    public static final String PCC007_LEASE_KEIYAKU_NO_LENGTH = "PCC07005";
    public static final String PCC007_PC_KATASHIKI_LENGTH = "PCC07006";
    public static final String PCC007_PC_MEISHOU_LENGTH = "PCC07007";
    public static final String PCC007_SERIAL_LENGTH = "PCC07008";
    public static final String PCC007_COMPUTER_NAME_LENGTH = "PCC07009";

    // 画面：作業員一覧・作成画面
    // コード：PCC09
    public static final String PCC009_DELETE_OWNER="PCC09001";
    public static final String PCC009_DELETE_COMPLETED="PCC09002";
    public static final String PCC009_UPDATE_COMPLETED = "PCC09003";
    public static final String PCC009_SAGYOUIN_NAME_REQUIRED = "PCC09004";
    public static final String PCC009_SAGYOUIN_NAME_LENGTH = "PCC09005";
    public static final String PCC009_PASSWORD_REQUIRED = "PCC09006";
    public static final String PCC009_PASSWORD_LENGTH = "PCC09007";
}
