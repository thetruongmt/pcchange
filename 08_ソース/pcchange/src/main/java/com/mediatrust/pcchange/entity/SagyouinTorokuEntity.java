package com.mediatrust.pcchange.entity;

import java.util.List;

import javax.validation.Valid;

import lombok.Data;

@Data
public class SagyouinTorokuEntity {

	@Valid
	private List<SagyouinEntity> detail;

}
