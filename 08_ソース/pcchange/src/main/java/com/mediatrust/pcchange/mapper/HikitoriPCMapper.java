package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.HikitoriPCEntity;

@Mapper
public interface HikitoriPCMapper {
	HikitoriPCEntity findHikitoriPC(@Param("pHikitoriPcNo") Integer pHikotoriPCNo);
	HikitoriPCEntity findHikitoriPCInProject(@Param("pProjectNo") Integer pProjectNo, @Param("pHikitoriPcNo") Integer pHikotoriPCNo);
	int getCountHikitoriPCInProject(@Param("pProjectNo") Integer pProjectNo);
	List<HikitoriPCEntity> findAllHikitoriPCInProject(@Param("pProjectNo") Integer pProjectNo, @Param("pOffset") Integer pOffset, @Param("pRowCnt") Integer pRowCnt);
	void deleteHikiatePC(@Param("pHikitoriPcNo") Integer pHikitoriPCNo);
	void updateListHikiatePC(@Param("lstEnt") List<HikitoriPCEntity> pLstEnt);
}