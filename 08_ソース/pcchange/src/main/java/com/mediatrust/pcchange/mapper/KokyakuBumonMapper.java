package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.KokyakuBumonEntity;

@Mapper
public interface KokyakuBumonMapper {
	public List<KokyakuBumonEntity> findAll();
	public List<KokyakuBumonEntity> findAllByKokyakyId(@Param("pKokyakuId") Integer pKokyaku);
	public KokyakuBumonEntity findAllById(@Param("pNo") Integer pNo);
}