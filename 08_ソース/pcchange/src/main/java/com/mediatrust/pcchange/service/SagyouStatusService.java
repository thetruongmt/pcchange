package com.mediatrust.pcchange.service;

import java.util.List;

import com.mediatrust.pcchange.entity.SagyouStatusEntity;

public interface SagyouStatusService {
	public List<SagyouStatusEntity> findAll();
}
