package com.mediatrust.pcchange.entity;

import java.util.Date;

import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

@Data
public class HikitoriPCEntity {

	private Integer hikitoriPcNo;
	private ProjectEntity sagyouProject;
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDD_FORMAT)
	private Date hikitoriKanryouDate;
	@Size(max=128, message="{"+MessageMap.PCC007_LEASE_KEIYAKU_NO_LENGTH+"}")
	private String leaseKeiyakuNo;
	@Size(max=128, message="{"+MessageMap.PCC007_PC_KATASHIKI_LENGTH+"}")
	private String pcKatashiki;
	@Size(max=128, message="{"+MessageMap.PCC007_PC_MEISHOU_LENGTH+"}")
	private String pcMeishou;
	@Size(max=128, message="{"+MessageMap.PCC007_SERIAL_LENGTH+"}")
	private String serialNo;
	private KokyakuShiyoushaEntity kokyakuShiyousha;
	@Size(max=128, message="{"+MessageMap.PCC007_COMPUTER_NAME_LENGTH+"}")
	private String pcName;
	private SagyouHikiatePCKobetsuEntity sagyouHikiatePC;
	private String setteiKoumoku1;
	private String setteiKoumoku2;
	private String setteiKoumoku3;
	private String setteiKoumoku4;
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT)
	private Date updDate;
	private Integer modified = 0;
}
