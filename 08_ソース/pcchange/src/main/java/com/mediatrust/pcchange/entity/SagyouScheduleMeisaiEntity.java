package com.mediatrust.pcchange.entity;

import java.util.Date;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import lombok.Data;

/**
 * 作業スケジュール明細マスタ
 * @author the.truong
 *
 */
@Data
public class SagyouScheduleMeisaiEntity {
	/**
	 *
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouScheduleMeisaiNo;
	/**
	 * 作業プロジェクト番号
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private String sagyouProjectNo;
	/**
	 * 作業年月
	 */
	private Date sagyouDate;
	/**
	 * 作業対象顧客使用者番号１
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo1;
	/**
	 * 作業対象顧客使用者番号２
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo2;
	/**
	 * 作業対象顧客使用者番号３
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo3;
	/**
	 * 作業対象顧客使用者番号４
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo4;
	/**
	 * 作業対象顧客使用者番号５
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo5;
	/**
	 * 作業対象顧客使用者番号６
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo6;
	/**
	 * 作業対象顧客使用者番号７
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo7;
	/**
	 * 作業対象顧客使用者番号８
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo8;
	/**
	 * 作業対象顧客使用者番号９
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo9;
	/**
	 * 作業対象顧客使用者番号１０
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo10;
	/**
	 * 作業対象顧客使用者番号１１
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo11;
	/**
	 * 作業対象顧客使用者番号１２
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo12;
	/**
	 * 作業対象顧客使用者番号１３
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo13;
	/**
	 * 作業対象顧客使用者番号１４
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo14;
	/**
	 * 作業対象顧客使用者番号１５
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo15;
	/**
	 * 作業対象顧客使用者番号１６
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo16;
	/**
	 * 作業対象顧客使用者番号１７
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo17;
	/**
	 * 作業対象顧客使用者番号１８
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo18;
	/**
	 * 作業対象顧客使用者番号１９
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo19;
	/**
	 * 作業対象顧客使用者番号２０
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo20;
	/**
	 * 作業対象顧客使用者番号２１
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo21;
	/**
	 * 作業対象顧客使用者番号２２
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo22;
	/**
	 * 作業対象顧客使用者番号２３
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo23;
	/**
	 * 作業対象顧客使用者番号２４
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo24;
	/**
	 * 作業対象顧客使用者番号２５
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo25;
	/**
	 * 作業対象顧客使用者番号２６
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo26;
	/**
	 * 作業対象顧客使用者番号２７
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo27;
	/**
	 * 作業対象顧客使用者番号２８
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo28;
	/**
	 * 作業対象顧客使用者番号２９
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo29;
	/**
	 * 作業対象顧客使用者番号３０
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer sagyouKokyakuShiyoshaNo30;
	/**
	 * 更新日
	 */
	private Date updDate;

	public int getScheduleSumiCount(){
		int retVal = 0;
		if(sagyouKokyakuShiyoshaNo1 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo2 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo3 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo4 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo5 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo6 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo7 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo8 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo9 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo10 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo11 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo12 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo13 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo14 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo15 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo16 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo17 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo18 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo19 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo20 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo21 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo22 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo23 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo24 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo25 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo26 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo27 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo28 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo29 != null){
			retVal++;
		}
		if(sagyouKokyakuShiyoshaNo30 != null){
			retVal++;
		}
		return retVal;
	}
}
