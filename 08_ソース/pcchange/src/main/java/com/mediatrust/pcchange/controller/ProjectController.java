package com.mediatrust.pcchange.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.KokyakuEntity;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.service.KokyakuService;
import com.mediatrust.pcchange.service.ProjectService;

@Controller
@RequestMapping("/project")
public class ProjectController {

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);
	@Autowired
	MessageSource _messageSource;

	@Autowired
	private KokyakuService _kokyakuService;
	@Autowired
	private ProjectService _projectService;

	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.PROJECT_CREATE;
	}

	@ModelAttribute("kokyakuList")
	public List<KokyakuEntity> getListKokyaku(){
		List<KokyakuEntity> kokyakuList = _kokyakuService.findAll();
		return kokyakuList;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create(ModelMap model) {

		logger.info("create開始 : 【パラメータ無し】");
		ProjectEntity entity = new ProjectEntity();

		model.addAttribute("model", entity);

		logger.info("create終了");
		return Views.PROJECT_CREATE;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createSubmit(@Valid ProjectEntity entity, BindingResult result, ModelMap model) {

		logger.info(String.format("createSubmit開始 : 【%s】", entity.toString()));

		// 入力チェック
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());

		} else {
			_projectService.addProject(entity);
			// 成功メッセージを表示する
			model.addAttribute("msg", _messageSource.getMessage(MessageMap.PCC001_PROJECT_CREATED, null, null));
			//データをリセット
			entity = new ProjectEntity();
		}

		model.addAttribute("model", entity);

		logger.info("createSubmit終了");
		return Views.PROJECT_CREATE;
	}

}