package com.mediatrust.pcchange.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.entity.KokyakuBumonEntity;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;
import com.mediatrust.pcchange.service.KokyakuBumonService;
import com.mediatrust.pcchange.service.ProjectService;
import com.mediatrust.pcchange.service.SagyouNaiyouService;

@Controller
@RequestMapping("/sagyou")
public class SagyouController {

	private static final Logger logger = LoggerFactory.getLogger(SagyouController.class);

	@Autowired
	MessageSource _messageSource;
	@Autowired
	private ProjectService _projectService;
	@Autowired
	private SagyouNaiyouService _sagyouNaiyouService;
	@Autowired
	private KokyakuBumonService _kokyakuBumonService;

	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SAGYOU_REGISTER;
	}

	@ModelAttribute("projects")
	public List<ProjectEntity> getProjects() {
		return _projectService.getAllProject();
	}

	@ModelAttribute("kokyakuBumons")
	public List<KokyakuBumonEntity> getKokyakuBumons() {
		return _kokyakuBumonService.findAll();
	}

	@ModelAttribute("kokyakuList")
	public List<SagyouNaiyouEntity> getKokyakuList() {
		return _sagyouNaiyouService.findAllCustomerWithProject();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		logger.info("index開始 : 【パラメータ無し】");

		SagyouNaiyouEntity s = new SagyouNaiyouEntity();
		model.addAttribute("model", s);

		logger.info("index終了");
		return "sagyou/index";
	}

	@RequestMapping(value = "/search", params = "search", method = RequestMethod.POST)
	public String search(SagyouNaiyouEntity sagyouNaiyou, Model model) {
		logger.info("search開始");

		SagyouNaiyouEntity s = _sagyouNaiyouService.findFirst(sagyouNaiyou.getSagyouProjectNo(),
				sagyouNaiyou.getKokyakuNo(), sagyouNaiyou.getKokyakuBumonNo());
		if (s == null) {
			s = sagyouNaiyou;
			s.setSagyouName("");
			s.removeAllSagyoSettei();
		}
		model.addAttribute("model", s);

		logger.info("search終了");

		return "sagyou/index";
	}

	@RequestMapping(value = "/search", params = "save", method = RequestMethod.POST)
	public String save(@Valid SagyouNaiyouEntity sagyouNaiyou, BindingResult result, Model model) {
		logger.info("save開始");

		// 入力チェック
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());

		} else {
			// 作業内容存在チェック
			// プロジェクトごとに、1顧客と1部門があります
			SagyouNaiyouEntity s = _sagyouNaiyouService.findFirst(sagyouNaiyou.getSagyouProjectNo(),
					sagyouNaiyou.getKokyakuNo(), sagyouNaiyou.getKokyakuBumonNo());
			if (s == null) {
				_sagyouNaiyouService.addSagyouNaiyou(sagyouNaiyou);
				// 成功メッセージを表示する
				model.addAttribute("msg",
						_messageSource.getMessage(MessageMap.PCC003_SAGYOU_NAIYOU_CREATED, null, null));
				sagyouNaiyou = new SagyouNaiyouEntity();
			} else {
				sagyouNaiyou.setSagyouNaiyouNo(s.getSagyouNaiyouNo());
				_sagyouNaiyouService.updateSagyouNaiyou(sagyouNaiyou);
				// 成功メッセージを表示する
				model.addAttribute("msg",
						_messageSource.getMessage(MessageMap.PCC003_SAGYOU_NAIYOU_UPDATED, null, null));
			}
		}

		model.addAttribute("model", sagyouNaiyou);

		logger.info("save終了");

		return "sagyou/index";
	}

}