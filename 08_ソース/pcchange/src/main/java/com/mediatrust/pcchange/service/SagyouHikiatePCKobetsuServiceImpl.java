package com.mediatrust.pcchange.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.entity.HikitoriPCEntity;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.entity.SagyouHikiatePCKobetsuEntity;
import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;
import com.mediatrust.pcchange.entity.SagyouinEntity;
import com.mediatrust.pcchange.entity.ShinPCTorokuEntity;
import com.mediatrust.pcchange.entity.ShinchokuEntity;
import com.mediatrust.pcchange.mapper.SagyouHikiatePCKobetsuMapper;

@Service("sagyouHikiatePCKobetsuSrv")
public class SagyouHikiatePCKobetsuServiceImpl implements SagyouHikiatePCKobetsuService {

	private static final Logger logger = LoggerFactory.getLogger(SagyouHikiatePCKobetsuServiceImpl.class);
	@Autowired
	MessageSource _messageSource;
	@Autowired
	private SagyouHikiatePCKobetsuMapper _mapper;
	@Autowired
	private SagyouinService _sagyouinSrv;
	@Autowired
	private HikitoriPCService _hikitoriPCSrv;
	@Autowired
	private SagyouNaiyouService _sagyouNaiyouSrv;
	@Autowired
	private KokyakuShiyoushaService _kokyakuShiyoushaSrv;

	public ShinchokuEntity getShinchoku(ShinchokuEntity entity, boolean isNext){

		ShinchokuEntity retVal= entity;

		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別件数を取得する");
		int cnt = _mapper.getCountSagyouHikiatePCKobetsu(entity.getSagyouProjectNo(),entity.getSagyouStatusNo(), entity.getSagyouHikiatePcKobetsuNo());

		if(cnt == 0 ){
			retVal.setDetail(null);
			retVal.setTotalPageCnt(0);
			retVal.setCurrentPage(1);
			return retVal;
		}
		Integer currentPage = entity.getCurrentPage();
		if(currentPage == null){
			currentPage = 0;
		}
		if(isNext){
			currentPage = currentPage+1;
		}else{
			currentPage=currentPage-1;
		}

		if(currentPage >= cnt){
			currentPage = cnt;
		}else if(currentPage <= 0){
			currentPage = 1;
		}

		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別一覧を取得する");
		List<SagyouHikiatePCKobetsuEntity> hikiateLst = _mapper.findSagyouHikiatePCKobetsu(entity.getSagyouProjectNo(),entity.getSagyouStatusNo(), entity.getSagyouHikiatePcKobetsuNo(),currentPage-1,1);
		SagyouHikiatePCKobetsuEntity detail = null;
		if(!hikiateLst.isEmpty()){
			detail = hikiateLst.get(0);
		}
		retVal.setCurrentPage(currentPage);
		retVal.setTotalPageCnt(cnt);
		retVal.setDetail(detail);

		return retVal;

	}

	@Override
	public SagyouHikiatePCKobetsuEntity getSagyouHikiatePCKobetsuByNo(Integer pNo){

		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：番号を元に作業引当PC個別情報を検索する");
		return _mapper.findSagyouHikiatePCKobetsuByNo(pNo);

	}

	/**
	 * 整合性チェック
	 */
	@Override
	public HashMap<String, String> updateValidate(SagyouHikiatePCKobetsuEntity entity) {
		HashMap<String, String> retVal = new HashMap<String, String>();

		SagyouHikiatePCKobetsuEntity dbEntity = this.getSagyouHikiatePCKobetsuByNo(entity.getSagyouHikiatePcKobetsuNo());

		Date inputDate = entity.getUpdDate();
		Date dbDate = dbEntity.getUpdDate();

		if((dbDate == null && inputDate != null ) ||
				(dbDate != null && inputDate == null) ||
				(dbDate != null && dbDate.compareTo(inputDate) != 0)){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null) );
		}
		return retVal;
	}

	/**
	 * 進捗を登録する
	 */
	public void updateHikiatePCKobetsuShinchoku(SagyouHikiatePCKobetsuEntity entity){

		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：進捗情報を登録する");
		_mapper.updateHikiatePCKobetsuShinchoku(entity);

	}

	/**
	 * 新PC登録データを取得する
	 * @param entity
	 * @param rowPage
	 * @param isNext
	 * @return
	 */
	public ShinPCTorokuEntity getShinPCForToroku(ShinPCTorokuEntity entity, Integer rowPage){

		ShinPCTorokuEntity retVal = entity;

		if(rowPage == null || rowPage == 0){
			rowPage = 20;
		}
		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別件数を取得する");
		int cnt = _mapper.getCountSagyouHikiatePCKobetsu(entity.getSagyouProjectNo(),null, null);

		if(cnt == 0 ){
			retVal.setDetail(null);
			retVal.setTotalPage(1);
			retVal.setCurrentPage(1);
			return retVal;
		}

		Integer pageCount = (int) Math.ceil(cnt / (1.0*rowPage));
		Integer currentPage = entity.getCurrentPage();
		if(currentPage == null){
			currentPage = 1;
		}

		if(currentPage >= pageCount){
			currentPage = pageCount;
		}else if(currentPage <= 0){
			currentPage = 1;
		}

		logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別一覧を取得する");
		List<SagyouHikiatePCKobetsuEntity> hikiateLst = _mapper.findSagyouHikiatePCKobetsu(entity.getSagyouProjectNo(),null, null,(currentPage-1)*rowPage,rowPage);

		retVal.setCurrentPage(currentPage);
		retVal.setTotalPage(pageCount);
		retVal.setDetail(hikiateLst);

		return retVal;

	}

	public HashMap<String, String> deleteSagyouHikiatePCKobetsu(SagyouHikiatePCKobetsuEntity entity){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(!this.checkSeigosei(entity)){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
		}else{
			logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別を削除する");
			_mapper.deleteHikiatePCKobetsuByNo(entity.getSagyouHikiatePcKobetsuNo());
		}
		return retVal;
	}

	/**
	 * 整合性チェック
	 * @param entity
	 * @return
	 */
	private boolean checkSeigosei(SagyouHikiatePCKobetsuEntity entity){

		SagyouHikiatePCKobetsuEntity dbEntity = _mapper.findSagyouHikiatePCKobetsuByNo(entity.getSagyouHikiatePcKobetsuNo());
		if(dbEntity == null) return false;

		Date dbDate = dbEntity.getUpdDate();
		Date inputDate = entity.getUpdDate();
		if((dbDate == null && inputDate != null ) ||
				(dbDate != null && inputDate == null) ||
				(dbDate != null && dbDate.compareTo(inputDate) != 0)){
			return false;
		}

		return true;
	}

	public HashMap<String, String> checkForUpdate(Integer pProjectNo, List<SagyouHikiatePCKobetsuEntity> lstEnt, List<SagyouHikiatePCKobetsuEntity> refUpdateList){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(lstEnt == null) return retVal;
		for (SagyouHikiatePCKobetsuEntity item : lstEnt) {
			Integer itemIdx = lstEnt.indexOf(item);
			String itemKey = "";
			if(item.getSagyouHikiatePcKobetsuNo() != null && item.getModified() != 1){
				continue;
			}

			String messageTemplate = _messageSource.getMessage(MessageMap.PCC006_MASTER_EXISTS, null,null);
			if(item.getSagyouin() != null && item.getSagyouin().getSagyouinNo() != null){
				//作業員マスタに存在チェック
				SagyouinEntity sagyouin = _sagyouinSrv.findSagyouinById(item.getSagyouin().getSagyouinNo());
				if(sagyouin == null){
					itemKey = String.format("detail[%d].sagyouin.sagyouinNo", itemIdx);
					retVal.put(itemKey, String.format(messageTemplate, "作業員", "作業員マスタ"));
				}
			}

			if(item.getHikitoriPCNo() != null){
				//引取PC番号を存在チェック
				HikitoriPCEntity hikitoriPC = _hikitoriPCSrv.findHikitoriPCInProject(pProjectNo, item.getHikitoriPCNo());
				if(hikitoriPC == null){
					itemKey = String.format("detail[%d].hikitoriPCNo", itemIdx);
					retVal.put(itemKey, String.format(messageTemplate, "引取PC番号", "引取PCマスタ"));
				}
			}

			if(item.getSagyouNaiyou() != null && item.getSagyouNaiyou().getSagyouNaiyouNo() != null){
				//作業内容チェック
				SagyouNaiyouEntity sagyouNaiyou = _sagyouNaiyouSrv.findSagyouNaiyouInProject(pProjectNo, item.getSagyouNaiyou().getSagyouNaiyouNo());
				if(sagyouNaiyou == null){
					itemKey = String.format("detail[%d].sagyouNaiyou.sagyouNaiyouNo", itemIdx);
					retVal.put(itemKey, String.format(messageTemplate, "作業名", "作業内容(パターン)マスタ"));
				}
			}

			if(item.getKokyakuShiyousha() != null && item.getKokyakuShiyousha().getKokyakuShiyoushaNo() != null){
				//顧客使用者チェック
				KokyakuShiyoushaEntity kokyakuShiyousha = _kokyakuShiyoushaSrv.findKokyakuShiyoushaInProject(pProjectNo, item.getKokyakuShiyousha().getKokyakuShiyoushaNo());
				if(kokyakuShiyousha == null){
					itemKey = String.format("detail[%d].kokyakuShiyousha.kokyakuShiyoushaNo", itemIdx);
					retVal.put(itemKey, String.format(messageTemplate, "顧客使用者名", "顧客使用者マスタ"));
				}

			}

			if(item.getSagyouHikiatePcKobetsuNo() == null){
				refUpdateList.add(item);
			}else if(item.getModified() == 1){
				if(!checkSeigosei(item)){
					String key = String.format("Row%d_totalCheck", lstEnt.indexOf(item));
					retVal.put(key, _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
				}else{
					refUpdateList.add(item);
				}
			}
		}

		return retVal;
	}
	public void updateListHikiatePCKobetsu(List<SagyouHikiatePCKobetsuEntity> pLstEnt){
		if(pLstEnt != null && !pLstEnt.isEmpty()){
			logger.info("データベースアクセス【sagyou_hikiate_pc_kobetsu_master】：作業引当PC個別一覧を更新する");
			_mapper.updateListHikiatePCKobetsu(pLstEnt);
		}
	}
}
