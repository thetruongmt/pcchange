package com.mediatrust.pcchange.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.entity.HikitoriPCEntity;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.entity.KyuuPCTorokuEntity;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.mapper.HikitoriPCMapper;

@Service("hikitoriPCService")
public class HikitoriPCServiceImpl implements HikitoriPCService {

	private static final Logger logger = LoggerFactory.getLogger(HikitoriPCServiceImpl.class);

	@Autowired
	MessageSource _messageSource;
	@Autowired
	private HikitoriPCMapper _mapper;
	@Autowired
	private KokyakuShiyoushaService _kokyakuShiyoushaSrv;

	public HikitoriPCEntity findHikitoriPCInProject(Integer pProjectNo, Integer pHikotoriPCNo){
		logger.info("データベースアクセス【hikitori_pc_master】：引取PCマスタ情報を取得");
		return _mapper.findHikitoriPCInProject(pProjectNo, pHikotoriPCNo);
	}

	public List<HikitoriPCEntity> findAllHikitoriPCInProject(Integer pProjectNo, Integer pOffset, Integer pRowCnt){
		logger.info("データベースアクセス【hikitori_pc_master】：引取PCマスタ一覧を取得");
		return _mapper.findAllHikitoriPCInProject(pProjectNo, pOffset, pRowCnt);
	}

	public KyuuPCTorokuEntity getKyuuPCForTouroku(KyuuPCTorokuEntity entity, Integer rowPage){

		KyuuPCTorokuEntity retVal = entity;

		if(rowPage == null || rowPage == 0){
			rowPage = 20;
		}
		logger.info("データベースアクセス【hikitori_pc_master】：引取PC件数を取得する");
		int cnt = _mapper.getCountHikitoriPCInProject(entity.getSagyouProjectNo());

		if(cnt == 0 ){
			retVal.setDetail(null);
			retVal.setTotalPage(1);
			retVal.setCurrentPage(1);
			return retVal;
		}

		Integer pageCount = (int) Math.ceil(cnt / (1.0*rowPage));
		Integer currentPage = entity.getCurrentPage();
		if(currentPage == null){
			currentPage = 1;
		}

		if(currentPage >= pageCount){
			currentPage = pageCount;
		}else if(currentPage <= 0){
			currentPage = 1;
		}

		logger.info("データベースアクセス【hikitori_pc_master】：引取PCマスタを取得する");
		List<HikitoriPCEntity> hikiateLst = _mapper.findAllHikitoriPCInProject(entity.getSagyouProjectNo(), (currentPage-1)*rowPage, rowPage);

		retVal.setCurrentPage(currentPage);
		retVal.setTotalPage(pageCount);
		retVal.setDetail(hikiateLst);

		return retVal;
	}

	public HashMap<String, String> deleteSagyouHikiatePC(HikitoriPCEntity entity){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(!this.checkSeigosei(entity)){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
		}else{
			logger.info("データベースアクセス【hikitori_pc_master】：引取PCマスタを削除する");
			_mapper.deleteHikiatePC(entity.getHikitoriPcNo());
		}
		return retVal;
	}

	/**
	 * 整合性チェック
	 * @param entity
	 * @return
	 */
	private boolean checkSeigosei(HikitoriPCEntity entity){

		HikitoriPCEntity dbEntity = _mapper.findHikitoriPC(entity.getHikitoriPcNo());
		if(dbEntity == null) return false;

		Date dbDate = dbEntity.getUpdDate();
		Date inputDate = entity.getUpdDate();
		if((dbDate == null && inputDate != null ) ||
				(dbDate != null && inputDate == null) ||
				(dbDate != null && dbDate.compareTo(inputDate) != 0)){
			return false;
		}

		return true;
	}

	public HashMap<String, String> checkForUpdate(Integer pProjectNo, List<HikitoriPCEntity> lstEnt, List<HikitoriPCEntity> refUpdateList){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(lstEnt == null) return retVal;
		for (HikitoriPCEntity item : lstEnt) {
			Integer itemIdx = lstEnt.indexOf(item);
			String itemKey = "";
			if(item.getHikitoriPcNo() != null && item.getModified() != 1){
				continue;
			}

			String messageTemplate = _messageSource.getMessage(MessageMap.PCC007_MASTER_EXISTS, null,null);

			if(item.getKokyakuShiyousha() != null && item.getKokyakuShiyousha().getKokyakuShiyoushaNo() != null){
				//顧客使用者チェック
				Integer rowProjectNo = pProjectNo;
				if(item.getSagyouProject() != null && item.getSagyouProject().getSagyouProjectNo() != null){
					rowProjectNo = item.getSagyouProject().getSagyouProjectNo();
				}
				KokyakuShiyoushaEntity kokyakuShiyousha = _kokyakuShiyoushaSrv.findKokyakuShiyoushaInProject(rowProjectNo, item.getKokyakuShiyousha().getKokyakuShiyoushaNo());
				if(kokyakuShiyousha == null){
					itemKey = String.format("detail[%d].kokyakuShiyousha.kokyakuShiyoushaNo", itemIdx);
					retVal.put(itemKey, String.format(messageTemplate, "顧客使用者名", "顧客使用者マスタ"));
				}

			}

			if(item.getHikitoriPcNo() == null){
				ProjectEntity project = new ProjectEntity();
				project.setSagyouProjectNo(pProjectNo);
				item.setSagyouProject(project);
				refUpdateList.add(item);
			}else if(item.getModified() == 1){
				if(!checkSeigosei(item)){
					String key = String.format("Row%d_totalCheck", lstEnt.indexOf(item));
					retVal.put(key, _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
				}else{
					refUpdateList.add(item);
				}
			}
		}

		return retVal;
	}
	public void updateListHikiatePC(List<HikitoriPCEntity> pLstEnt){
		if(pLstEnt != null && !pLstEnt.isEmpty()){
			logger.info("データベースアクセス【hikitori_pc_master】：引取PCマスタに更新する");
			_mapper.updateListHikiatePC(pLstEnt);
		}
	}
}
