package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.SagyouinEntity;

@Mapper
public interface SagyouinMapper {

	public List<SagyouinEntity> findAll();
	public SagyouinEntity findSagyouinById(@Param("no") String sagyouinNo);
	public void deleteSagyouin(@Param("no") Integer sagyouinNo);
	public void updateSagyouin(@Param("lstEnt") List<SagyouinEntity> lstEnt);

}
