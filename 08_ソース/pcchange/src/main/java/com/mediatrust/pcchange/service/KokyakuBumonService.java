package com.mediatrust.pcchange.service;

import java.util.List;

import com.mediatrust.pcchange.entity.KokyakuBumonEntity;

public interface KokyakuBumonService {
	public List<KokyakuBumonEntity> findAll();
	public List<KokyakuBumonEntity> findAllByKokyakyId(Integer pKokyaku);
	public KokyakuBumonEntity findAllById(Integer pNo);
}
