package com.mediatrust.pcchange.entity;

import java.util.Date;

import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

/**
 * 作業(引当)PC個別マスタ
 * @author the.truong
 *
 */
@Data
public class SagyouHikiatePCKobetsuEntity {

	/**
	 * 作業(引当)PC個別番号
	 */
	private Integer sagyouHikiatePcKobetsuNo;
	/**
	 * 作業員
	 */
	private SagyouinEntity sagyouin;
	/**
	 * 引取PC番号
	 */
	@NumberFormat(pattern="00000", style=Style.NUMBER)
	private Integer hikitoriPCNo;
	/**
	 * 作業内容(パターン)番号
	 */
	private SagyouNaiyouEntity sagyouNaiyou;
	/**
	 * シリアル番号
	 */

	@Size(max=128, message="{"+MessageMap.PCC004_HIKIATE_PC_NUMBER_LENGTH+"}")
	private String serialNo;
	/**
	 * 顧客使用者
	 */
	private KokyakuShiyoushaEntity kokyakuShiyousha;
	/**
	 * コンピュータ名
	 */
	@Size(max=128, message="{"+MessageMap.PCC004_COMPUTER_NAME_LENGTH+"}")
	private String pcName;
	/**
	 * ネットワーク設定１
	 */
	private String network1;
	/**
	 * ネットワーク設定２
	 */
	private String network2;
	/**
	 * 設定項目３
	 */
	private String setteiKoumoku3;
	/**
	 * 設定項目４
	 */
	private String setteiKoumoku4;
	/**
	 * 設定項目５
	 */
	private String setteiKoumoku5;
	/**
	 * 設定項目６
	 */
	private String setteiKoumoku6;
	/**
	 * 設定項目７
	 */
	private String setteiKoumoku7;
	/**
	 * 設定項目８
	 */
	private String setteiKoumoku8;
	/**
	 * 設定項目９
	 */
	private String setteiKoumoku9;
	/**
	 * 設定項目１０
	 */
	private String setteiKoumoku10;
	/**
	 * 作業ステータス番号
	 */
	private SagyouStatusEntity sagyouStatus;
	/**
	 * 更新日
	 */
	@DateTimeFormat(pattern = SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT)
	private Date updDate;

	private Integer modified = 0;;

}
