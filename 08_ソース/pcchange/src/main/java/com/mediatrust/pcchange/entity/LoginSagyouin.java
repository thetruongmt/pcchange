package com.mediatrust.pcchange.entity;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class LoginSagyouin extends User{

	/**
	 * シリアル
	 */
	private static final long serialVersionUID = 1L;

	private SagyouinEntity userEntity;

	public LoginSagyouin(SagyouinEntity user, String[] roles) {
		super(String.valueOf(user.getSagyouinNo()), user.getPassword(), AuthorityUtils.createAuthorityList(roles));
		this.userEntity = user;
	}

	public SagyouinEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(SagyouinEntity userEntity) {
		this.userEntity = userEntity;
	}

}