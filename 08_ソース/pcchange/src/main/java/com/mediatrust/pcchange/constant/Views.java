package com.mediatrust.pcchange.constant;

public class Views {

	/*ログイン*/
	public static final String LOGIN = "login";

	public static final String PROJECT_CREATE = "project/create";

	/**
	 *スケジュール画面
	 */
	public static final String SCHEDULE="schedule/index";

	/**
	 * 進捗登録画面
	 */
	public static final String SHINCHOKU="shinchoku/index";

	/**
	 * 使用者一覧画面
	 */
	public static final String SHIYOUSHA="shiyousha/index";

	/**
	 * 新PC登録画面
	 */
	public static final String SHINPC_TOROKU="shinpc/index";

	/**
	 * 旧PC登録画面
	 */
	public static final String KYUUPC_TOROKU="kyuupc/index";

	/**
	 * 作業員一覧画面
	 */
	public static final String SAGYOUIN="sagyouin/index";

	/**
	 *
	 */
	public static final String SEARCH_KOKYAKU_SHIYOUSHA="search/searchKokyakuShiyousha";
	/**
	 *
	 */
	public static final String SEARCH_HIKITORI_PC="search/searchHikitoriPC";
}
