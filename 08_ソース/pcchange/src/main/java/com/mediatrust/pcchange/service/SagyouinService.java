package com.mediatrust.pcchange.service;

import java.util.HashMap;
import java.util.List;

import com.mediatrust.pcchange.entity.SagyouinEntity;

public interface SagyouinService {

	public List<SagyouinEntity> findAll();
	public SagyouinEntity findSagyouinById(Integer pId);
	public HashMap<String, String> deleteSagyouin(Integer loginId, SagyouinEntity entity);
	public HashMap<String, String> checkForUpdate(List<SagyouinEntity> lstEnt, List<SagyouinEntity> refUpdateList);
	public void updateSagyouin(List<SagyouinEntity> lstEnt);

}
