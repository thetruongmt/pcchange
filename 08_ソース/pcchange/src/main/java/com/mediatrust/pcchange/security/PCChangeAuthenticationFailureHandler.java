package com.mediatrust.pcchange.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.mediatrust.pcchange.constant.MessageMap;

/**
 * ログイン失敗のハンドル
 * @author the
 *
 */
@Component
public class PCChangeAuthenticationFailureHandler implements AuthenticationFailureHandler{


	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authenticationException)
			throws IOException, ServletException {

		String contextPath = request.getContextPath();
		String loginId = request.getParameter("sagyouinNo");
		String error = "";
		// ExceptionからエラーIDをセットする
		if(authenticationException instanceof BadCredentialsException){
			error = MessageMap.PCC000_LOGIN_ERROR;
		}

		// ログイン画面にリダイレクトする
		response.sendRedirect(contextPath+"/login/error?msgCd=" + error+"&sagyoinNo="+loginId);

	}

}
