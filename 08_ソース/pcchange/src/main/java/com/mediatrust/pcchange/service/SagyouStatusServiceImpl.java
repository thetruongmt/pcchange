package com.mediatrust.pcchange.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.entity.SagyouStatusEntity;
import com.mediatrust.pcchange.mapper.SagyouStatusMapper;

@Service("sagyouStatusSrv")
public class SagyouStatusServiceImpl implements SagyouStatusService {

	private static final Logger logger = LoggerFactory.getLogger(SagyouStatusServiceImpl.class);
	@Autowired
	private SagyouStatusMapper _mapper;

	public List<SagyouStatusEntity> findAll() {

		logger.info("データベースアクセス【sagyou_status_master】：作業ステータス一覧を取得");
		return _mapper.findAllStatus();
	}
}
