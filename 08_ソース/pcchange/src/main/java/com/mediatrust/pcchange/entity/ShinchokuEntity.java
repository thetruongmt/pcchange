package com.mediatrust.pcchange.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mediatrust.pcchange.constant.MessageMap;

import lombok.Data;

/**
 * 進捗登録画面のエンティティ
 * @author the.truong
 *
 */
@Data
public class ShinchokuEntity {

	/**
	 * プロジェクト番号
	 */

	@NotNull(message="{"+MessageMap.PCC004_PROJECT_NAME_REQUIRED+"}")
	private Integer sagyouProjectNo;
	/**
	 * 作業ステータス
	 */
	private Integer sagyouStatusNo;
	/**
	 * 作業（引当）PC個別番号
	 */
	private Integer sagyouHikiatePcKobetsuNo;
	/**
	 * 作業引当PC個別情報
	 */
	@Valid
	private SagyouHikiatePCKobetsuEntity detail;
	/**
	 * 現在ページ
	 */
	private Integer currentPage;
	/**
	 * 件数
	 */
	private Integer totalPageCnt;

}
