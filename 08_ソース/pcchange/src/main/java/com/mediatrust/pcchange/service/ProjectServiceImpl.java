package com.mediatrust.pcchange.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.mapper.ProjectMapper;

@Service("ProjectService")
public class ProjectServiceImpl implements ProjectService {

	private static final Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);
	@Autowired
	private ProjectMapper _mapper;

	/**
	 * プロジェクト一覧を取得する
	 */
	public List<ProjectEntity> getAllProject() {

		logger.info("データベースアクセス【project_master】：プロジェクト一覧データを取得");
		return _mapper.findAllProject();
	}

	/**
	 * プロジェクト情報を取得
	 */
	public ProjectEntity getProjectByNo(String projectNo) {
		logger.info("データベースアクセス【project_master】：プロジェクト情報を取得");
		return _mapper.findProjectByNo(projectNo);
	}

	public void addProject(ProjectEntity project) {

		logger.info("データベースアクセス【project_master】：プロジェクト情報を追加する");
		_mapper.addProject(project);
	}
}
