package com.mediatrust.pcchange.service;

import java.util.Date;
import java.util.HashMap;

import com.mediatrust.pcchange.entity.SagyouScheduleEntity;


public interface SagyouScheduleMeisaiSerivce {
	public SagyouScheduleEntity getSagyouScheduleByProject(String projectNo, Date sagyouDate);
	public HashMap<String, String> updateValidate(SagyouScheduleEntity entity);
	public void update(SagyouScheduleEntity entity);
}
