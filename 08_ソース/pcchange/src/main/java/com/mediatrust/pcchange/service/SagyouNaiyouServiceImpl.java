package com.mediatrust.pcchange.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;
import com.mediatrust.pcchange.mapper.SagyouNaiyouMapper;

@Service("sagyouNaiyouSrv")
public class SagyouNaiyouServiceImpl implements SagyouNaiyouService {

    private static final Logger logger = LoggerFactory.getLogger(SagyouNaiyouServiceImpl.class);
    @Autowired
    private SagyouNaiyouMapper _mapper;

    /**
     *
     * 顧客とプロジェクトを取得
     */
    public List<SagyouNaiyouEntity> findAllCustomerWithProject() {
		logger.info("データベースアクセス【sagyou_naiyou_master】：作業内容データを取得");
		return _mapper.findAllCustomerWithProject();
    }

    /**
     * プロジェクトごとに、1顧客と1部門があります
     */
    public SagyouNaiyouEntity findFirst(Integer sagyouProjectNo, Integer kokyakuNo, Integer kokyakuBumonNo) {
		logger.info("データベースアクセス【sagyou_naiyou_master】：作業内容データを取得");
		return _mapper.findFirst(sagyouProjectNo, kokyakuNo, kokyakuBumonNo);
    }

    public SagyouNaiyouEntity findSagyouNaiyouInProject(Integer pProjectNo, Integer pSagyouNaiyouNo){
    	logger.info("データベースアクセス【sagyou_naiyou_master】：作業内容データを取得");
		return _mapper.findSagyouNaiyouInProject(pProjectNo, pSagyouNaiyouNo);
    }

    public void addSagyouNaiyou(SagyouNaiyouEntity sagyouNaiyou) {
    	_mapper.addSagyouNaiyou(sagyouNaiyou);
    }

    public void updateSagyouNaiyou(SagyouNaiyouEntity sagyouNaiyou) {
    	_mapper.updateSagyouNaiyou(sagyouNaiyou);
    }

    public List<SagyouNaiyouEntity> findAllByProject(Integer projectNo){
    	logger.info("データベースアクセス【sagyou_naiyou_master】：プロジェクト番号で作業内容データを取得");
    	return _mapper.findAllByProject(projectNo);
    }
}
