package com.mediatrust.pcchange.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.LoginSagyouin;
import com.mediatrust.pcchange.entity.SagyouinEntity;
import com.mediatrust.pcchange.entity.SagyouinTorokuEntity;
import com.mediatrust.pcchange.service.SagyouinService;

@Controller
@RequestMapping("/sagyouin")
public class SagyouinController {

	private static final Logger logger = LoggerFactory.getLogger(SagyouinController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	private SagyouinService _sagyouinSrv;

	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SAGYOUIN_REGISTER;
	}
	/**
	 * 作業員一覧の表示アクション
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model, HttpServletRequest request) {

		logger.info("index開始 : 【パラメータ無し】");

				SagyouinTorokuEntity entity = new SagyouinTorokuEntity();
		List<SagyouinEntity> sagyouins = _sagyouinSrv.findAll();
		entity.setDetail(sagyouins);

		model.addAttribute("model", entity);
		logger.info("index終了");

		return Views.SAGYOUIN;
	}

	@RequestMapping(value="/delete", produces = MediaType.APPLICATION_JSON_VALUE , method=RequestMethod.POST )
	public @ResponseBody HashMap<String, String> deleteRow(SagyouinEntity entity, ModelMap model, UsernamePasswordAuthenticationToken principal){
		logger.info(String.format("deleteRow開始 : 【%s】", entity.toString()));

		HashMap<String, String> retVal = new HashMap<String, String>();
		retVal.put("status", "1");
		retVal.put("message", "");
		//ログインユーザーとる
		SagyouinEntity loginUser = ((LoginSagyouin)principal.getPrincipal()).getUserEntity();
		HashMap<String, String> deleteRet = _sagyouinSrv.deleteSagyouin(loginUser.getSagyouinNo(), entity);
		if(deleteRet.containsKey("_totalCheck")){
			retVal.replace("status", "0");
			retVal.replace("message", deleteRet.get("_totalCheck"));
		}else{
			retVal.replace("message", messageSource.getMessage(MessageMap.PCC009_DELETE_COMPLETED, null, null));
		}

		logger.info("deleteRow終了");
		return retVal;

	}

	/**
	 * 登録ボタンのアクション
	 * @param entity
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(params="update",method = RequestMethod.POST)
	public String update(@Valid SagyouinTorokuEntity entity, BindingResult result, ModelMap model,RedirectAttributes attributes){

		logger.debug(String.format("************　update開始【データ：%s】　************", entity.toString()));
		if(result.hasErrors()){
			List<ObjectError> errorOrgs = result.getAllErrors();

			java.util.Map<String, String> errorDetails=errorOrgs.stream()
						.map(p->(FieldError) p)
						.collect(Collectors.toMap(FieldError::getField	, FieldError::getDefaultMessage));

			Set<String> existing = new HashSet<String>();

			java.util.Map<String, String> errors = errorDetails.entrySet().stream()
					.filter(p -> existing.add(p.getValue()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			model.addAttribute("errors", errors);
			model.addAttribute("errorDetails",errorDetails );
		}else{
			List<SagyouinEntity> updateList = new ArrayList<>();
			HashMap<String, String> errorDetails = _sagyouinSrv.checkForUpdate(entity.getDetail(), updateList);
			if(!errorDetails.isEmpty()){

				HashMap<String, String> errors = new HashMap<String, String>();
				errors.put("SagyouinTorokuEntity", messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null, null));

				model.addAttribute("errors", errors );
				model.addAttribute("errorDetails", errorDetails);
			}else{
				_sagyouinSrv.updateSagyouin(updateList);
				attributes.addFlashAttribute("success", messageSource.getMessage(MessageMap.PCC009_UPDATE_COMPLETED, null, null));
				return "redirect:/sagyouin";
			}
		}

		model.addAttribute("model", entity);

		logger.info("update終了");
		return Views.SAGYOUIN;

	}

}