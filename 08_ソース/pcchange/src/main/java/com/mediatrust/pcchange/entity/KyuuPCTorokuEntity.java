package com.mediatrust.pcchange.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mediatrust.pcchange.constant.MessageMap;

import lombok.Data;

@Data
public class KyuuPCTorokuEntity {

	@NotNull(message="{"+MessageMap.PCC007_PROJECT_REQUIRED+"}")
	private Integer sagyouProjectNo;

	@Valid
	private List<HikitoriPCEntity> detail;
	private Integer currentPage;
	private Integer totalPage;

}
