package com.mediatrust.pcchange.entity;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.constant.SystemConstant;

import lombok.Data;

/**
 * 作業員エンティティ
 * @author the
 *
 */
@Data
public class SagyouinEntity {

	/**
	 * 作業員番号
	 */
	private Integer sagyouinNo;

	/**
	 * 作業員名
	 */
	@NotBlank(message = "{"+MessageMap.PCC009_SAGYOUIN_NAME_REQUIRED+"}")
	@Size(max=128, message = "{"+MessageMap.PCC009_SAGYOUIN_NAME_LENGTH+"}")
	private String sagyouinName;

	/**
	 * パスワード
	 */
	@NotBlank(message = "{"+MessageMap.PCC009_PASSWORD_REQUIRED+"}")
	@Size(max=128, message = "{"+MessageMap.PCC009_PASSWORD_LENGTH+"}")
	private String password;

	@DateTimeFormat(pattern=SystemConstant.DATE_YYYYMMDDHHMMSS_FORMAT)
	private Date updDate;

	private Integer modified = 0;
}
