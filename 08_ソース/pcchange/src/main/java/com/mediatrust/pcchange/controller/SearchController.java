package com.mediatrust.pcchange.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.HikitoriPCEntity;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.service.HikitoriPCService;
import com.mediatrust.pcchange.service.KokyakuShiyoushaService;
import com.mediatrust.pcchange.service.ProjectService;

@Controller
@RequestMapping("/search")
public class SearchController {

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	private ProjectService _prjSrv;
	@Autowired
	private KokyakuShiyoushaService _kokyakuShiyoushaService;
	@Autowired
	private HikitoriPCService _hikitoriPCSrv;

	/*********** モデル要素 ***********/


	@RequestMapping(value = "/shiyousha", method = RequestMethod.GET)
	public String searchShiyousha(@RequestParam("projectNo") Integer projectNo, ModelMap model) {

		logger.info(String.format("searchShiyousha開始 : 【プロジェクト番号=%d】", projectNo));

		List<KokyakuShiyoushaEntity> lstEnt = null;
		if(projectNo != null){
			ProjectEntity project = _prjSrv.getProjectByNo(String.valueOf(projectNo));
			model.addAttribute("project", project);
			lstEnt = _kokyakuShiyoushaService.findAllKokyakuShiyoushaByProject(projectNo);
		}else{
			lstEnt = _kokyakuShiyoushaService.findAll();
		}

		model.addAttribute("kokyakuShiyoushaList", lstEnt);

		logger.info("searchShiyousha終了");
		return Views.SEARCH_KOKYAKU_SHIYOUSHA;
	}

	/**
	 * スケジュールページの表示アクション
	 *
	 * @return
	 */
	@RequestMapping(value = "/hikitoripc", method = RequestMethod.GET)
	public String searchHikitoriPC(@RequestParam("projectNo") Integer projectNo, ModelMap model) {

		logger.info(String.format("searchHikitoriPC開始 : 【プロジェクト番号=%d】", projectNo));

		List<HikitoriPCEntity> lstEnt = null;
		if(projectNo != null){
			ProjectEntity project = _prjSrv.getProjectByNo(String.valueOf(projectNo));
			model.addAttribute("project", project);
		}

		lstEnt = _hikitoriPCSrv.findAllHikitoriPCInProject(projectNo, null, null);

		model.addAttribute("hikitoriPCList", lstEnt);

		logger.info("searchHikitoriPC終了");

		return Views.SEARCH_HIKITORI_PC;
	}

}