package com.mediatrust.pcchange.service;

import java.util.List;

import com.mediatrust.pcchange.entity.KokyakuEntity;

public interface KokyakuService {
	public List<KokyakuEntity> findAll();
}
