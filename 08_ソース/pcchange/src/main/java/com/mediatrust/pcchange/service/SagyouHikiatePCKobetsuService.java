package com.mediatrust.pcchange.service;

import java.util.HashMap;
import java.util.List;

import com.mediatrust.pcchange.entity.SagyouHikiatePCKobetsuEntity;
import com.mediatrust.pcchange.entity.ShinPCTorokuEntity;
import com.mediatrust.pcchange.entity.ShinchokuEntity;

public interface SagyouHikiatePCKobetsuService {

	public ShinchokuEntity getShinchoku(ShinchokuEntity entity, boolean isNext);
	public HashMap<String, String> updateValidate(SagyouHikiatePCKobetsuEntity entity);
	public SagyouHikiatePCKobetsuEntity getSagyouHikiatePCKobetsuByNo(Integer pNo);
	public void updateHikiatePCKobetsuShinchoku(SagyouHikiatePCKobetsuEntity entity);

	public ShinPCTorokuEntity getShinPCForToroku(ShinPCTorokuEntity entity, Integer rowPage);
	public HashMap<String, String> deleteSagyouHikiatePCKobetsu(SagyouHikiatePCKobetsuEntity entity);
	public HashMap<String, String> checkForUpdate(Integer pProjectNo, List<SagyouHikiatePCKobetsuEntity> lstEnt, List<SagyouHikiatePCKobetsuEntity> refUpdateList);
	public void updateListHikiatePCKobetsu(List<SagyouHikiatePCKobetsuEntity> pLstEnt);

}
