package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.mediatrust.pcchange.entity.KokyakuEntity;

@Mapper
public interface KokyakuMapper {
	public List<KokyakuEntity> findAll();
}