package com.mediatrust.pcchange.service;

import java.util.List;

import com.mediatrust.pcchange.entity.SagyouNaiyouEntity;

public interface SagyouNaiyouService {

    public List<SagyouNaiyouEntity> findAllCustomerWithProject();

    /**
     * プロジェクトごとに、1顧客と1部門があります
     *
     * @return
     */
    public SagyouNaiyouEntity findFirst(Integer sagyouProjectNo, Integer kokyakuNo, Integer kokyakuBumonNo);

    public SagyouNaiyouEntity findSagyouNaiyouInProject(Integer pProjectNo, Integer pSagyouNaiyouNo);

    public void addSagyouNaiyou(SagyouNaiyouEntity sagyouNaiyou);
    public void updateSagyouNaiyou(SagyouNaiyouEntity sagyouNaiyou);
    public List<SagyouNaiyouEntity> findAllByProject(Integer projectNo);
}
