package com.mediatrust.pcchange.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.constant.MessageMap;
import com.mediatrust.pcchange.entity.KokyakuBumonEntity;
import com.mediatrust.pcchange.entity.KokyakuEntity;
import com.mediatrust.pcchange.entity.KokyakuShiyoushaEntity;
import com.mediatrust.pcchange.mapper.KokyakuShiyoushaMapper;

@Service("kokyakuShiyoushaSrv")
public class KokyakuShiyoushaServiceImpl implements KokyakuShiyoushaService {

	private static final Logger logger = LoggerFactory.getLogger(KokyakuShiyoushaServiceImpl.class);
	@Autowired
	MessageSource _messageSource;
	@Autowired
	private KokyakuShiyoushaMapper _mapper;

	public List<KokyakuShiyoushaEntity> findAll() {
		logger.info("データベースアクセス【kokyaku_shiyousha_master】：顧客利用者一覧をすべて検索する");
		return _mapper.findAll();
	}

	public KokyakuShiyoushaEntity findKokyakuShiyoushaByNo(Integer pNo){

		logger.info("データベースアクセス【kokyaku_shiyousha_master】：顧客番号を元に顧客利用者一覧を検索する");
		return _mapper.findKokyakuShiyoushaByNo(pNo);
	}

	public List<KokyakuShiyoushaEntity> findAllKokyakuShiyoushaByProject(Integer pProjectNo){
		logger.info("データベースアクセス【kokyaku_shiyousha_master】：プロジェクト番号を元に顧客利用者一覧を検索する");
		return _mapper.findAllKokyakuShiyoushaByProject(pProjectNo);
	}

	public List<KokyakuShiyoushaEntity> findKokyakuShiyoushaByKokyakuAndBumon(Integer pKokyaku, Integer pBumon){
		logger.info("データベースアクセス【kokyaku_shiyousha_master】：画面からの条件で顧客利用者一覧を検索する");
		return _mapper.findKokyakuShiyoushaByKokyakuAndBumon(pKokyaku, pBumon);
	}

	public KokyakuShiyoushaEntity findKokyakuShiyoushaInProject(Integer pProjectNo,Integer pKokyakuShiyouShaNo){
		logger.info("データベースアクセス【kokyaku_shiyousha_master】：顧客利用者を検索する");
		return _mapper.findKokyakuShiyoushaInProject(pProjectNo,pKokyakuShiyouShaNo);
	}

	/**
	 *　顧客使用者情報を削除する
	 * @param entity
	 */
	public HashMap<String, String> deleteKokyakuShiyousha(KokyakuShiyoushaEntity entity){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(!this.checkSeigosei(entity)){
			retVal.put("_totalCheck", _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
		}else{
			_mapper.deleteKokyakuShiyoushaByNo(entity.getKokyakuShiyoushaNo());
		}
		return retVal;

	}

	public HashMap<String, String> checkForUpdate(Integer pKokyakuNo, Integer pBumonNo, List<KokyakuShiyoushaEntity> lstEnt, List<KokyakuShiyoushaEntity> refUpdateList){

		HashMap<String, String> retVal = new HashMap<String, String>();
		if(lstEnt == null) return retVal;
		for (KokyakuShiyoushaEntity item : lstEnt) {

			if(item.getKokyakuShiyoushaNo() == null){
				KokyakuEntity kokyaku = new KokyakuEntity();
				kokyaku.setKokyakuNo(pKokyakuNo);
				item.setKokyaku(kokyaku);
				KokyakuBumonEntity kokyakuBumon = new KokyakuBumonEntity();
				kokyakuBumon.setKokyakuBumonNo(pBumonNo);
				item.setKokyakuBumon(kokyakuBumon);
				refUpdateList.add(item);
			}else if(item.getModified() == 1){
				if(!checkSeigosei(item)){
					String key = String.format("Row%d_totalCheck", lstEnt.indexOf(item));
					retVal.put(key, _messageSource.getMessage(MessageMap.PCC000_COMMON_CONFLIT_DATA_ERROR, null,null));
				}else{
					refUpdateList.add(item);
				}
			}
		}

		return retVal;
	}

	/**
	 * 顧客使用者情報を更新する
	 */
	public void updateShiyoushaInfo(List<KokyakuShiyoushaEntity> lstEnt){
		if(lstEnt != null && !lstEnt.isEmpty()){
			_mapper.updateListShiyoushaInfo(lstEnt);
		}
	}

	/**
	 * 整合性チェック
	 * @param entity
	 * @return
	 */
	private boolean checkSeigosei(KokyakuShiyoushaEntity entity){

		KokyakuShiyoushaEntity dbEntity = _mapper.findKokyakuShiyoushaByNo(entity.getKokyakuShiyoushaNo());
		if(dbEntity == null) return false;

		Date dbDate = dbEntity.getUpdDate();
		Date inputDate = entity.getUpdDate();
		if((dbDate == null && inputDate != null ) ||
				(dbDate != null && inputDate == null) ||
				(dbDate != null && dbDate.compareTo(inputDate) != 0)){
			return false;
		}

		return true;
	}
}
