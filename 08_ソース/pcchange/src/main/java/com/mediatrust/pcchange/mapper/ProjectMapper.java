package com.mediatrust.pcchange.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mediatrust.pcchange.entity.ProjectEntity;

@Mapper
public interface ProjectMapper {
	public List<ProjectEntity> findAllProject();
	public ProjectEntity findProjectByNo(@Param("pPjNo") String projectNo);
	public void addProject(@Param("project") ProjectEntity project);
}