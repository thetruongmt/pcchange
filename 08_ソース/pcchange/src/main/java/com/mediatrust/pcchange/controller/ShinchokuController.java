package com.mediatrust.pcchange.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mediatrust.pcchange.constant.SystemConstant;
import com.mediatrust.pcchange.constant.Views;
import com.mediatrust.pcchange.entity.ProjectEntity;
import com.mediatrust.pcchange.entity.SagyouStatusEntity;
import com.mediatrust.pcchange.entity.SagyouinEntity;
import com.mediatrust.pcchange.entity.ShinchokuEntity;
import com.mediatrust.pcchange.service.ProjectService;
import com.mediatrust.pcchange.service.SagyouHikiatePCKobetsuService;
import com.mediatrust.pcchange.service.SagyouStatusService;
import com.mediatrust.pcchange.service.SagyouinService;

@Controller
@RequestMapping("/shinchoku")
public class ShinchokuController {

	private static final Logger logger = LoggerFactory.getLogger(ShinchokuController.class);

	@Autowired
	MessageSource messageSource;
	@Autowired
	private ProjectService _pjSrv;
	@Autowired
	private SagyouStatusService _statusSrv;
	@Autowired
	private SagyouinService _sagyouinSrv;
	@Autowired
	private SagyouHikiatePCKobetsuService _sagyouHikiatePCSrv;


	/*********** モデル要素 ***********/
	@ModelAttribute("pageId")
	public String getPageId(){
		return SystemConstant.PageID.SHINCHOKU_REGISTER;
	}

	@ModelAttribute("projects")
	public List<ProjectEntity> getProjects() {
		return _pjSrv.getAllProject();
	}

	@ModelAttribute("statuses")
	public List<SagyouStatusEntity> getListStatus(){
		return _statusSrv.findAll();
	}

	@ModelAttribute("sagyouins")
	public List<SagyouinEntity> getListSagyouin(){
		return _sagyouinSrv.findAll();
	}

	/**
	 * スケジュールページの表示アクション
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model) {

		logger.info("index開始 : 【パラメータ無し】");

		ShinchokuEntity entity = new ShinchokuEntity();

		model.addAttribute("model", entity);

		logger.info("index終了");

		return Views.SHINCHOKU;
	}

	/**
	 * 進捗データ検索ボタンのアクション
	 *
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="search", method = RequestMethod.POST)
	public String search(@Valid ShinchokuEntity entity, BindingResult result, ModelMap model) {

		logger.info(String.format("search開始 : 【%s】", entity.toString()));

		entity.setCurrentPage(0);
		entity =_sagyouHikiatePCSrv.getShinchoku(entity, true);

		model.addAttribute("model", entity);
		model.addAttribute("searched", true);
		logger.info("search終了");

		return Views.SHINCHOKU;
	}


	/**
	 * 次ページのボタンのアクション
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="next", method=RequestMethod.POST)
	public String next(ShinchokuEntity entity, ModelMap model){

		logger.info(String.format("next開始：【%s】", entity.toString()));

		entity = _sagyouHikiatePCSrv.getShinchoku(entity, true);

		model.addAttribute("model", entity);
		model.addAttribute("searched", true);
		logger.info("next終了");
		return Views.SHINCHOKU;
	}

	/**
	 * 前ページのボタンのアクション
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(params="prev", method=RequestMethod.POST)
	public String prev(ShinchokuEntity entity, ModelMap model){

		logger.info(String.format("prev開始：【%s】", entity.toString()));

		entity = _sagyouHikiatePCSrv.getShinchoku(entity, false);

		model.addAttribute("model", entity);
		model.addAttribute("searched", true);
		logger.info("prev終了");
		return Views.SHINCHOKU;
	}

	@RequestMapping(params="update",method = RequestMethod.POST)
	public String update(@Valid ShinchokuEntity entity, BindingResult result, ModelMap model){

		logger.debug(String.format("************　update開始【データ：%s】　************", entity.toString()));

		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
		}else{
			HashMap<String, String> errorDetails = _sagyouHikiatePCSrv.updateValidate(entity.getDetail());

			if(errorDetails.size() > 0){
				List<ObjectError> errors = new ArrayList<ObjectError>();
				if(errorDetails.containsKey("_totalCheck")){
					ObjectError error = new ObjectError("ShinchokuEntity", errorDetails.get("_totalCheck"));
					errors.add(error);
					errorDetails.remove("_totalCheck");
				}

				model.addAttribute("errors", errors);

			}else{
				//更新する
				_sagyouHikiatePCSrv.updateHikiatePCKobetsuShinchoku(entity.getDetail());
				next(entity, model);
			}
		}

		model.addAttribute("model", entity);

		logger.info("update終了");
		return Views.SHINCHOKU;

	}

}