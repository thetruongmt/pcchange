package com.mediatrust.pcchange.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediatrust.pcchange.entity.KokyakuBumonEntity;
import com.mediatrust.pcchange.mapper.KokyakuBumonMapper;

@Service("kokyakuBumonSrv")
public class KokyakuBumonServiceImpl implements KokyakuBumonService {

	private static final Logger logger = LoggerFactory.getLogger(KokyakuBumonServiceImpl.class);

	@Autowired
	private KokyakuBumonMapper _mapper;

	public List<KokyakuBumonEntity> findAll() {

		logger.info("データベースアクセス【kokyaku_bumon_master】：顧客部門一覧を取得する");
		return _mapper.findAll();
	}

	/**
	 * 顧客番号を元に顧客部門一覧を取得する
	 */
	public List<KokyakuBumonEntity> findAllByKokyakyId(Integer pKokyaku){

		logger.info("データベースアクセス【kokyaku_bumon_master】：顧客部門一覧を取得する");
		return _mapper.findAllByKokyakyId(pKokyaku);
	}

	public KokyakuBumonEntity findAllById(Integer pNo){
		logger.info("データベースアクセス【kokyaku_bumon_master】：顧客部門情報を取得する");
		return _mapper.findAllById(pNo);
	}
}
