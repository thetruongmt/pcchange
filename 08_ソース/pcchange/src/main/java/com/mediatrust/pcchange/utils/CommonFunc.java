package com.mediatrust.pcchange.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.mediatrust.pcchange.constant.SystemConstant;

public class CommonFunc {

	public static int compareDateWithoutTime(Date d1, Date d2){
		SimpleDateFormat fmt = new SimpleDateFormat(SystemConstant.DATE_YYYYMMDD_FORMAT);
		return fmt.format(d1).compareTo(fmt.format(d2));
	}
}
