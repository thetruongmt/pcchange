package com.mediatrust.pcchange.entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.mediatrust.pcchange.constant.MessageMap;
import lombok.Data;

@Data
public class SagyouNaiyouEntity {

    private Integer sagyouNaiyouNo;

    @NotBlank(message = "{" + MessageMap.PCC003_SAGYOU_NAME_REQUIRED + "}")
    private String sagyouName;

    @NotNull(message = "{" + MessageMap.PCC003_SAGYOU_PROJECT_NO_REQUIRED + "}")
    private Integer sagyouProjectNo;

    @NotNull(message = "{" + MessageMap.PCC003_KOKYAKU_NO_REQUIRED + "}")
    private Integer kokyakuNo;

    @NotNull(message = "{" + MessageMap.PCC003_KOKYAKU_BUMON_NO_REQUIRED + "}")
    private Integer kokyakuBumonNo;

    private String sagyouSetteiKoumoku1;
    private String sagyouSetteiKoumoku2;
    private String sagyouSetteiKoumoku3;
    private String sagyouSetteiKoumoku4;
    private String sagyouSetteiKoumoku5;
    private String sagyouSetteiKoumoku6;
    private String sagyouSetteiKoumoku7;
    private String sagyouSetteiKoumoku8;
    private String sagyouSetteiKoumoku9;
    private String sagyouSetteiKoumoku10;

    // 外部のプロパティ
    private String kokyakuKigyouName;

    public void removeAllSagyoSettei() {
	this.sagyouSetteiKoumoku1 = "";
	this.sagyouSetteiKoumoku2 = "";
	this.sagyouSetteiKoumoku3 = "";
	this.sagyouSetteiKoumoku4 = "";
	this.sagyouSetteiKoumoku5 = "";
	this.sagyouSetteiKoumoku6 = "";
	this.sagyouSetteiKoumoku7 = "";
	this.sagyouSetteiKoumoku8 = "";
	this.sagyouSetteiKoumoku9 = "";
	this.sagyouSetteiKoumoku10 = "";
    }
}
