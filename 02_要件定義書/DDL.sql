-- Project Name : pcchange_db_ER図
-- Date/Time    : 2019/01/21 17:25:01
-- Author       : m.ohba
-- RDBMS Type   : MySQL
-- Application  : A5:SQL Mk-2

-- 作業スケジュール明細マスタ
drop table if exists sagyou_schedule_meisai_master cascade;

create table sagyou_schedule_meisai_master (
  sagyou_schedule_meisai_no SMALLINT ZEROFILL not null comment '作業スケジュール詳細番号'
  , sagyou_project_no SMALLINT ZEROFILL comment '作業プロジェクト番号'
  , sagyou_date DATE comment '作業年月'
  , sagyou_taishou_kokyaku_shiyousha_no1 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１'
  , sagyou_taishou_kokyaku_shiyousha_no2 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２'
  , sagyou_taishou_kokyaku_shiyousha_no3 SMALLINT ZEROFILL comment '作業対象顧客使用者番号３'
  , sagyou_taishou_kokyaku_shiyousha_no4 SMALLINT ZEROFILL comment '作業対象顧客使用者番号４'
  , sagyou_taishou_kokyaku_shiyousha_no5 SMALLINT ZEROFILL comment '作業対象顧客使用者番号５'
  , sagyou_taishou_kokyaku_shiyousha_no6 SMALLINT ZEROFILL comment '作業対象顧客使用者番号６'
  , sagyou_taishou_kokyaku_shiyousha_no7 SMALLINT ZEROFILL comment '作業対象顧客使用者番号７'
  , sagyou_taishou_kokyaku_shiyousha_no8 SMALLINT ZEROFILL comment '作業対象顧客使用者番号８'
  , sagyou_taishou_kokyaku_shiyousha_no9 SMALLINT ZEROFILL comment '作業対象顧客使用者番号９'
  , sagyou_taishou_kokyaku_shiyousha_no10 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１０'
  , sagyou_taishou_kokyaku_shiyousha_no11 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１１'
  , sagyou_taishou_kokyaku_shiyousha_no12 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１２'
  , sagyou_taishou_kokyaku_shiyousha_no13 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１３'
  , sagyou_taishou_kokyaku_shiyousha_no14 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１４'
  , sagyou_taishou_kokyaku_shiyousha_no15 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１５'
  , sagyou_taishou_kokyaku_shiyousha_no16 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１６'
  , sagyou_taishou_kokyaku_shiyousha_no17 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１７'
  , sagyou_taishou_kokyaku_shiyousha_no18 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１８'
  , sagyou_taishou_kokyaku_shiyousha_no19 SMALLINT ZEROFILL comment '作業対象顧客使用者番号１９'
  , sagyou_taishou_kokyaku_shiyousha_no20 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２０'
  , sagyou_taishou_kokyaku_shiyousha_no21 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２１'
  , sagyou_taishou_kokyaku_shiyousha_no22 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２２'
  , sagyou_taishou_kokyaku_shiyousha_no23 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２３'
  , sagyou_taishou_kokyaku_shiyousha_no24 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２４'
  , sagyou_taishou_kokyaku_shiyousha_no25 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２５'
  , sagyou_taishou_kokyaku_shiyousha_no26 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２６'
  , sagyou_taishou_kokyaku_shiyousha_no27 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２７'
  , sagyou_taishou_kokyaku_shiyousha_no28 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２８'
  , sagyou_taishou_kokyaku_shiyousha_no29 SMALLINT ZEROFILL comment '作業対象顧客使用者番号２９'
  , sagyou_taishou_kokyaku_shiyousha_no30 SMALLINT ZEROFILL comment '作業対象顧客使用者番号３０'
  , constraint sagyou_schedule_meisai_master_PKC primary key (sagyou_schedule_meisai_no)
) comment '作業スケジュール明細マスタ' ;

-- 引取PCマスタ
drop table if exists hikitori_pc_master cascade;

create table hikitori_pc_master (
  hikitori_pc_no SMALLINT ZEROFILL not null comment '引取PC番号'
  , sagyou_project_no SMALLINT ZEROFILL comment '作業プロジェクト番号'
  , hikitori_kanryou_date DATE comment '引取完了日'
  , lease_keiyaku_no VARCHAR(128) comment 'リース契約番号'
  , pc_katashiki VARCHAR(128) comment 'PC型式'
  , pc_meishou VARCHAR(128) comment 'PC名称'
  , serial_no VARCHAR(128) comment 'シリアル番号'
  , kokyaku_shiyousha_no SMALLINT ZEROFILL comment '顧客使用者番号'
  , pc_name VARCHAR(128) comment 'コンピュータ名'
  , sagyou_hikiate_pc_kobetsu_no SMALLINT ZEROFILL comment '作業(引当)PC個別番号'
  , settei_koumoku1 TEXT(1024) comment '設定項目１'
  , settei_koumoku2 TEXT(1024) comment '設定項目２'
  , settei_koumoku3 TEXT(1024) comment '設定項目３'
  , settei_koumoku4 TEXT(1024) comment '設定項目４'
  , constraint hikitori_pc_master_PKC primary key (hikitori_pc_no)
) comment '引取PCマスタ' ;

-- 作業ステータスマスタ
drop table if exists sagyou_status_master cascade;

create table sagyou_status_master (
  sagyou_status_no SMALLINT ZEROFILL not null comment '作業ステータス番号'
  , status VARCHAR(128) not null comment 'ステータス'
  , constraint sagyou_status_master_PKC primary key (sagyou_status_no)
) comment '作業ステータスマスタ' ;

-- 作業(引当)PC個別マスタ
drop table if exists sagyou_hikiate_pc_kobetsu_master cascade;

create table sagyou_hikiate_pc_kobetsu_master (
  sagyou_hikiate_pc_kobetsu_no SMALLINT ZEROFILL not null comment '作業(引当)PC個別番号'
  , sagyouin_no SMALLINT ZEROFILL comment '作業員番号'
  , hikitori_pc_no SMALLINT ZEROFILL comment '引取PC番号'
  , sagyou_naiyou_no SMALLINT ZEROFILL comment '作業内容(パターン)番号'
  , serial_no VARCHAR(128) comment 'シリアル番号'
  , kokyaku_shiyousha_no SMALLINT ZEROFILL comment '顧客使用者番号'
  , pc_name VARCHAR(128) comment 'コンピュータ名'
  , network1 TEXT(1024) comment 'ネットワーク設定１'
  , network2 TEXT(1024) comment 'ネットワーク設定２'
  , settei_koumoku3 TEXT(1024) comment '設定項目３'
  , settei_koumoku4 TEXT(1024) comment '設定項目４'
  , settei_koumoku5 TEXT(1024) comment '設定項目５'
  , settei_koumoku6 TEXT(1024) comment '設定項目６'
  , settei_koumoku7 TEXT(1024) comment '設定項目７'
  , settei_koumoku8 TEXT(1024) comment '設定項目８'
  , settei_koumoku9 TEXT(1024) comment '設定項目９'
  , settei_koumoku10 TEXT(1024) comment '設定項目１０'
  , sagyou_status_no SMALLINT ZEROFILL comment '作業ステータス番号'
  , constraint sagyou_hikiate_pc_kobetsu_master_PKC primary key (sagyou_hikiate_pc_kobetsu_no)
) comment '作業(引当)PC個別マスタ' ;

-- 作業内容(パターン)マスタ
drop table if exists sagyou_naiyou_master cascade;

create table sagyou_naiyou_master (
  sagyou_naiyou_no SMALLINT ZEROFILL not null comment '作業内容(パターン)番号'
  , sagyou_name VARCHAR(128) comment '作業名'
  , sagyou_project_no SMALLINT ZEROFILL comment '作業プロジェクト番号'
  , kokyaku_no SMALLINT ZEROFILL comment '顧客番号'
  , kokyaku_bumon_no SMALLINT ZEROFILL comment '顧客部門番号'
  , sagyou_settei_koumoku1 TEXT(1024) comment '作業設定項目１'
  , sagyou_settei_koumoku2 TEXT(1024) comment '作業設定項目２'
  , sagyou_settei_koumoku3 TEXT(1024) comment '作業設定項目３'
  , sagyou_settei_koumoku4 TEXT(1024) comment '作業設定項目４'
  , sagyou_settei_koumoku5 TEXT(1024) comment '作業設定項目５'
  , sagyou_settei_koumoku6 TEXT(1024) comment '作業設定項目６'
  , sagyou_settei_koumoku7 TEXT(1024) comment '作業設定項目７'
  , sagyou_settei_koumoku8 TEXT(1024) comment '作業設定項目８'
  , sagyou_settei_koumoku9 TEXT(1024) comment '作業設定項目９'
  , sagyou_settei_koumoku10 TEXT(1024) comment '作業設定項目１０'
  , constraint sagyou_naiyou_master_PKC primary key (sagyou_naiyou_no)
) comment '作業内容(パターン)マスタ' ;

-- プロジェクトマスタ
drop table if exists project_master cascade;

create table project_master (
  sagyou_project_no SMALLINT ZEROFILL not null comment '作業プロジェクト番号'
  , sagyou_project_name VARCHAR(128) comment '作業プロジェクト名'
  , kokyaku_no SMALLINT ZEROFILL comment '顧客番号'
  , sagyou_kikan_from DATE comment '作業期間FROM'
  , sagyou_kikan_to DATE comment '作業期間TO'
  , constraint project_master_PKC primary key (sagyou_project_no)
) comment 'プロジェクトマスタ' ;

-- 顧客使用者マスタ
drop table if exists kokyaku_shiyousha_master cascade;

create table kokyaku_shiyousha_master (
  kokyaku_shiyousha_no SMALLINT ZEROFILL not null comment '顧客使用者番号'
  , kokyaku_no SMALLINT ZEROFILL comment '顧客番号'
  , kokyaku_bumon_no SMALLINT ZEROFILL comment '顧客部門番号'
  , shiyousha_name VARCHAR(128) comment '使用者名前'
  , naisen_no VARCHAR(128) comment '内線番号'
  , mail_address VARCHAR(128) comment 'メールアドレス'
  , constraint kokyaku_shiyousha_master_PKC primary key (kokyaku_shiyousha_no)
) comment '顧客使用者マスタ' ;

-- 顧客部門マスタ
drop table if exists kokyaku_bumon_master cascade;

create table kokyaku_bumon_master (
  kokyaku_bumon_no SMALLINT ZEROFILL not null comment '顧客部門番号'
  , jigyoubu_name VARCHAR(128) comment '事業部門(拠点)名'
  , kokyaku_no SMALLINT ZEROFILL comment '顧客番号'
  , buka_name VARCHAR(128) comment '部課名'
  , jyuusho1 VARCHAR(128) comment '住所１'
  , jyuusho2 VARCHAR(128) comment '住所２'
  , constraint kokyaku_bumon_master_PKC primary key (kokyaku_bumon_no)
) comment '顧客部門マスタ' ;

-- 顧客マスタ
drop table if exists kokyaku_master cascade;

create table kokyaku_master (
  kokyaku_no SMALLINT ZEROFILL not null comment '顧客番号'
  , kokyaku_kigyou_name VARCHAR(128) comment '顧客企業名'
  , constraint kokyaku_master_PKC primary key (kokyaku_no)
) comment '顧客マスタ' ;

-- 作業員マスタ
drop table if exists sagyouin_master cascade;

create table sagyouin_master (
  sagyouin_no SMALLINT ZEROFILL not null comment '作業員番号'
  , sagyouin_name VARCHAR(128) comment '作業員名'
  , password VARCHAR(128) comment 'パスワード'
  , constraint sagyouin_master_PKC primary key (sagyouin_no)
) comment '作業員マスタ' ;

